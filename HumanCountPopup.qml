import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.2

Item {
    x:0
    y:0
    width: parent.width
    height: parent.height

    signal sigClosed() // cancel signal
    //signal sigClickItem()//각 화면의 2버튼 팝업을 출력했을때 각 화면마다 버튼의 동작을 다르게 하도록 시그널을 설정해줌 (button2의 동작을 설저하기 위한 signal)

    // Popup object
    Popup {
        id:messagePopup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        modal: true
        focus: true
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closeFindAddressPopup();
                //sigClosed();
            }
        }

        Rectangle
        {
            id:popup_rect
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*1/2
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            Text
            {
                id:messageText
                x:popup_rect.width * 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                y:popup_rect.height* 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                width:popup_rect.width*0.8
                height:popup_rect.height*0.5
                color:"black"
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                wrapMode: Text.WordWrap
            }

            Button
            {
                id:ok_btn
                x: popup_rect.width * 0.3
                y: popup_rect.height* 0.7
                width: popup_rect.width*0.4
                height: popup_rect.height*0.2
                contentItem: Text {
                    fontSizeMode: Text.Fit
                    text: qsTr("확인")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onClicked: {
                    closeFindAddressPopup();
                }
            }
        }
    }



    function openMessagePopup(msg){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수
        //console.log("findAddrPopup info:" + info + "button1text:" + button1text + "button2text:" + button2text)
        //popupTitle.text = info
        //button1.text = button1text
        //button2.text = button2text
        console.log("open messagePopup")
        messageText.text=msg;
        messagePopup.open();
    }
    function closeFindAddressPopup()
    {
        console.log("close messagePopup")
        messagePopup.close();
        sigClosed();
    }

}
