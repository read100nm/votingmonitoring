package org.peoplecount.votingmonitoring;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.app.Service;
import android.os.IBinder;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Map;

public class QtAndroidService extends FirebaseMessagingService
{
    //private static native void sendToQt(String message);
    private static native void sendTokenToQt(String token);
    private static native void sendNotificationToQt(String title, String body);
    private static native void sendNotificationToQtTest(String title, String body);
    
    private static final String TAG = "QtAndroidService";

    public static void getTokenFromJava()
    {
        // [START log_reg_token]
        FirebaseMessaging.getInstance().getToken()
            .addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                   return;
                }

                // Get new FCM registration token
                String token = task.getResult();

                // Log and toast
                //String msg = getString(R.string.msg_token_fmt, token);
                Log.d(TAG, token);
                sendTokenToQt(token);
            }
        });
        // [END log_reg_token]
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Creating Service");
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("FirebaseMessagingService", "Refreshed token: " + s);
	//sendToQt(s);
        Log.i(TAG, "Service sent token to Qt:" + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> data = remoteMessage.getData();
        String title = data.get("title");
        String body = data.get("body");

        sendNotificationToQtTest(title,body);
        
        for (Map.Entry<String, String> entry : data.entrySet()) {
                String k = entry.getKey();
                String v = entry.getValue();
                Log.d("FirebaseMessagingService:","Key: " + k + ", Value: " + v);
            }
        
            Log.d("FirebaseMessagingService", "onMessageReceived : " + "title " + title + " body " + body);

        RemoteMessage.Notification nf = remoteMessage.getNotification();
        if(nf != null)
        {
            Log.d("FirebaseMessagingService", "onMessageReceived : " + "title " + nf.getTitle() + " body " + nf.getBody());
            sendNotificationToQt(nf.getTitle(),nf.getBody());
        }
        
    }

    private void sendNotification(String messageTitle,String messageBody) {
        
    }
    /*@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int ret = super.onStartCommand(intent, flags, startId);
        String name = new String(intent.getByteArrayExtra("name"));
        Log.i(TAG, "Service received name: " + name);
        String message = "Hello " + name;
        

        return ret;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }*/
}

