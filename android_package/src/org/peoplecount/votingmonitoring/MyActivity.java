package org.peoplecount.votingmonitoring;

import org.qtproject.qt5.android.bindings.QtActivity;
import android.view.WindowManager;
import android.os.Bundle;
import java.lang.System;

public class MyActivity extends QtActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        System.out.println("start my MainActivity...");
	    System.out.println(android.os.Build.VERSION.SDK_INT);

        //android.content.SharedPreferences sharedPref 
        //= getPreferences(android.content.Context.MODE_PRIVATE);
        //sharedPref.edit().putString("aaa","bbb").apply();
        //System.out.println(sharedPref.getString("aaa","nonono"));

        //android.content.SharedPreferences.Editor editor = sharedPref.edit();
        //System.out.println(android.os.Environment.getExternalStoragePublicDirectory(
        //    android.os.Environment.DIRECTORY_PICTURES)
        //);
        activityVisible = true;
    }

    public String setKeyValue(String key_str, String value_str)
    {
        android.content.SharedPreferences sharedPref 
        = getPreferences(android.content.Context.MODE_PRIVATE);
        sharedPref.edit().putString(key_str,value_str).apply();

        return "ok";
    }

    public String getKeyValue(String key_str, String def_str)
    {
        android.content.SharedPreferences sharedPref 
        = getPreferences(android.content.Context.MODE_PRIVATE);
        return sharedPref.getString(key_str,def_str);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityVisible = true;
        System.out.println("Java: Activity Resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityVisible = false;
        System.out.println("Java: Activity Paused");
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }  

    private static boolean activityVisible;
}
