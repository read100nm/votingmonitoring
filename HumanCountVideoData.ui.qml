import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.12
import QtMultimedia 5.12
import QtQuick.LocalStorage 2.15
import HumanCountLib 1.0

Page {
    id: managevideo_page
    width: stackView.width
    height: stackView.height

    title: qsTr("영상데이터관리")

    HumanCountVideoPopup{
        id: videoPlayPopup
    }

    HumanCountFtpAllUploadPopup{
        id: ftpAllUploadPopup
        onSigCanceled:{
            allUploader.stop();
            console.log("FTP All Upload Thread Stop!!");
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigCurrentFileProgress(result) {
                ftpAllUploadPopup.setProgressValue(result[0]/100.0);
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigCurrentFileProgressText(result) {
                ftpAllUploadPopup.setProgressText(result);
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigTotalProgress(result) {
                ftpAllUploadPopup.setTotalProgressValue(result[0]/100.0);
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigTotalProgressText(result) {
                ftpAllUploadPopup.setTotalProgressText(result);
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigCurrentFinishUpload(result) {
                // 성공으로 레이블 교체 
                videoList.model.get(result[0]).status="OK";
                videoList.model.get(result[0]).color="#008BEF";
                updateVideoStatus(videoList.model.get(result[0]).name,"upload ok");
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigCurrentErrorUpload(result) {
                // 실패로 레이블 교체 
                videoList.model.get(result[0]).status="실패";
                videoList.model.get(result[0]).color="#EF5230";
                updateVideoStatus(videoList.model.get(result[0]).name,"upload fail");
            }
        }

        Connections {
            target: humanCountFtpAllUploader
            function onSigTotalFinishUpload(result) {
                ftpAllUploadPopup.closePopup();
            }
        }
    }

    Rectangle {
        id: allfile_upload
        x: managevideo_page.width * 0.65
        y: managevideo_page.height * 0.015
        width: managevideo_page.width * 0.3
        height: managevideo_page.height * 0.04
        radius: height * 28 / 16
        color: allfile_upload_ma.pressed==true?"#FFFFFF":"#16B2E0"
        border.color: "#16B2E0"
        Text{
            x: 0
            y: 0
            width: parent.width 
            height: parent.height 
            text: qsTr("모든 파일 업로드")
            color: "#FFFFFF"
            minimumPixelSize: 2
            font.pixelSize: height * 14 / 28
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        MouseArea {
            id: allfile_upload_ma
            anchors.fill: parent
            onClicked: {
                if(humanAuditorData.isUploading()){
                    main_window.openMessagePopup("개별 파일 업로드가 진행중입니다. 개별 파일 업로드 진행중에는 모든 파일 업로드 사용이 불가능합니다.");
                    return;
                }
                humanCountFtpAllUploader.clearDownloadList();
                for(var i=0; i<videoList.count; i++)
                {
                    if(videoListModel.get(i).status != "OK")
                    {
                        //console.log("idx: "+i);
                        //console.log(videoList.model.get(i).name);
                        //console.log(videoList.model.get(i).path);
                        //console.log(videoList.model.get(i).nas_id);

                        var idx = i;
                        var name = videoListModel.get(i).name;
                        var path = "file://" +MoiveLocationPath + "/" + videoListModel.get(i).name;
                        if(RunPlatform == "ANDROID")
                            path = videoListModel.get(i).path;
                        //var nas_id = videoList.model.get(i).nas_id;

                        humanCountFtpAllUploader.addUploadList(idx,path,name,videoListModel.get(i).nas_id);
                    }
                    
                }
                ftpAllUploadPopup.openPopup("모든 파일을 업로드 합니다.");
                humanCountFtpAllUploader.start();
            }
        }
    }

    ListView {
        id: videoList
        x: 0
        y: managevideo_page.height * 0.07
        width: managevideo_page.width
        height: managevideo_page.height * 0.93
        clip:true
        model: HumanCountVideoListModel {
            id: videoListModel
        }
        delegate: HumanCountVideoListDelegate {
            id: videoListDelegate
            onClicked:{
                ListView.view.currentIndex = index;
                var path = "file://" +MoiveLocationPath + "/" + model.name;
                if(RunPlatform == "ANDROID")
                    path = model.path;
                console.log(path);
                videoPlayPopup.openVideoPlayPopup(path);
            }
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
    }

    function checkSameFileNameInDB(check_name){
        var check_status = "Not Found";
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
            db.transaction(
                function(tx) {
                    tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)')

                    // update
                    var rs = tx.executeSql('SELECT name, status FROM video_history2 WHERE name=?',
                            [check_name])

                    
                    console.log("Check Size:"+ rs.rows.length);
                    for (var i = 0; i < rs.rows.length; i++) {
                        check_status = rs.rows.item(i).status;
                    }
                }
            )
        }
        return check_status;
    }

    function loadVideoInfo(){
        if(RunPlatform == "ANDROID")
        {
            var fileList = humanCountUserInfoData.getFileListFromAppRoot();
            for(var i=0; i< fileList.length; i=i+4)
            {
                var fileName       =fileList[i];
                var fileNamePath   =fileList[i+1];
                var fileDate       =fileList[i+2];
                var nas_id         =fileList[i+3];

                var check_status   = checkSameFileNameInDB(fileName);
                if(check_status == "Not Found")
                {
                    insertVideoInfo( fileName, fileNamePath,fileDate,nas_id,"start record");
                }

                console.log(fileName + " is added in DB");
            }

            videoList.model.clear();
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
            db.transaction(
                function(tx) {
                    tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)')

                    var rs = tx.executeSql('SELECT * FROM video_history2 order by datetime DESC');

                    var r = ""
                    for (var i = 0; i < rs.rows.length; i++) {
                        if(rs.rows.item(i).status == "finish record")
                        {
                            videoList.model.append({
                                "name":rs.rows.item(i).name,
                                "date":rs.rows.item(i).datetime,
                                "status":"업로드",
                                "color":"#1CB10F",
                                "path":rs.rows.item(i).path,
                                "nas_id":rs.rows.item(i).nas_id
                            })
                        }

                        if(rs.rows.item(i).status == "start record")
                        {
                            videoList.model.append({
                                "name":rs.rows.item(i).name,
                                "date":rs.rows.item(i).datetime,
                                "status":"업로드",
                                "color":"#1CB10F",
                                "path":rs.rows.item(i).path,
                                "nas_id":rs.rows.item(i).nas_id
                            })
                        }

                        if(rs.rows.item(i).status == "upload fail")
                        {
                            videoList.model.append({
                                "name":rs.rows.item(i).name,
                                "date":rs.rows.item(i).datetime,
                                "status":"실패",
                                "color":"#EF5230",
                                "path":rs.rows.item(i).path,
                                "nas_id":rs.rows.item(i).nas_id
                            })
                        }

                        if(rs.rows.item(i).status == "upload ok")
                        {
                            videoList.model.append({
                                "name":rs.rows.item(i).name,
                                "date":rs.rows.item(i).datetime,
                                "status":"OK",
                                "color":"#008BEF",
                                "path":rs.rows.item(i).path,
                                "nas_id":rs.rows.item(i).nas_id
                            })
                        }

                        r = rs.rows.item(i).name + ", " + rs.rows.item(i).path + ", " + rs.rows.item(i).datetime 
                        + ", " + rs.rows.item(i).nas_id + ", " + rs.rows.item(i).status
                        console.log(r)
                    }
                }
            )


        }
        else
        {
            humanCountUserInfoData.loadUserVideoList();
            videoList.model.clear();
            var v_name = humanCountUserInfoData.v_name;//.push(videoName);
            var v_path = humanCountUserInfoData.v_path;
            var v_datetime =  humanCountUserInfoData.v_datetime;
            var v_nas_id   = humanCountUserInfoData.v_nas_id;
            var v_status   = humanCountUserInfoData.v_status;

            for(var i=(v_name.length-1); i>=0; i--)
            {
                var name = v_name[i];
                var datetime = v_datetime[i];
                var path = v_path[i];
                var nas_id = v_nas_id[i];
                var status = v_status[i];
                console.log(typeof nas_id);

                var r = "";
                r = name + ", " + path + ", " + datetime 
                        + ", " + parseInt(nas_id) + ", " + status;

                if(nas_id != null)
                    console.log(r)

                if(status == "finish record")
                {
                    videoList.model.append({
                        "name":name,
                        "date":datetime,
                        "status":"업로드",
                        "color":"#1CB10F",
                        "path":path,
                        "nas_id":nas_id
                    })
                }

                if(status == "upload fail")
                {
                    videoList.model.append({
                        "name":name,
                        "date":datetime,
                        "status":"실패",
                        "color":"#EF5230",
                        "path":path,
                        "nas_id":nas_id
                    })
                }

                if(status == "upload ok")
                {
                    videoList.model.append({
                        "name":name,
                        "date":datetime,
                        "status":"OK",
                        "color":"#008BEF",
                        "path":path,
                        "nas_id":nas_id
                    })
                }

            }
        }
    }

    function deleteVideoInfo(name){
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
            db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT path FROM video_history2 WHERE name=?',[name]);
                    var del_file_path= null;
                    if(rs.rows.length == 1)
                    {
                        del_file_path = rs.rows.item(0).path;
                    }

                    rs = tx.executeSql('DELETE FROM video_history2 WHERE name=?',[name]);

                    if(del_file_path != null)
                    {
                        humanCountUserInfoData.deleteFileByURL(del_file_path);
                    }
                }
            )
        }
        else
        {
            humanCountUserInfoData.loadUserVideoList();

            var v_name = humanCountUserInfoData.v_name;//.push(videoName);
            var v_path = humanCountUserInfoData.v_path;
            var v_datetime =  humanCountUserInfoData.v_datetime;
            var v_nas_id   = humanCountUserInfoData.v_nas_id;
            var v_status   = humanCountUserInfoData.v_status;

            var idx = v_name.indexOf(name);
            var del_file_path = "file://" +MoiveLocationPath + "/" + name;
            if(idx != -1)
            {
                v_name.splice(idx, 1);
                v_path.splice(idx, 1);
                v_datetime.splice(idx, 1);
                v_nas_id.splice(idx, 1);
                v_status.splice(idx, 1);
            }

            humanCountUserInfoData.v_name = v_name;
            humanCountUserInfoData.v_path = v_path;
            humanCountUserInfoData.v_datetime = v_datetime;
            humanCountUserInfoData.v_nas_id = v_nas_id;
            humanCountUserInfoData.v_status = v_status;

            humanCountUserInfoData.saveUserVideoList();
            humanCountUserInfoData.deleteFileByURL(del_file_path);
        }

        
        //videoList.model.remove(index);
    }

    function updateVideoStatus(name,status){
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
            db.transaction(
                function(tx) {
                    // Create the database if it doesn't already exist
                    tx.executeSql('CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)');

                    // update
                    tx.executeSql('UPDATE video_history2 SET status=? WHERE name=?',
                            [status, name])
                }
            )
        }
        else
        {
            humanCountUserInfoData.loadUserVideoList();
            console.log("called updateVideoInfo!!!!");
            console.log(videoName);
            var v_name     = humanCountUserInfoData.v_name;
            var v_status   = humanCountUserInfoData.v_status;
            for(var i=0; i< v_name.length; i++)
            {
                if(v_name[i] == videoName)
                {
                    v_status[i]=status;
                    console.log("updateDB: "+videoName);
                }
                console.log("VideoName not found!!: "+videoName);
            }

            humanCountUserInfoData.v_status = v_status;
            humanCountUserInfoData.saveUserVideoList();
        }
        
    }

    Component.onCompleted:{
        loadVideoInfo();
        //videoList.videoListDelegate.window_size=stackView.height;
        console.log(stackView.height);
    }

    Connections{

    }

    Connections {
        target: humanAuditorData
        function onSigFinishUpload(result) {
            videoList.model.get(result[0]).status="OK";
            videoList.model.get(result[0]).color="#008BEF";
            updateVideoStatus(videoList.model.get(result[0]).name,"upload ok");
        }
    }

    Connections {
        target: humanAuditorData
        function onSigErrorUpload(result){
            videoList.model.get(result[0]).status="실패";
            videoList.model.get(result[0]).color="#EF5230";
            updateVideoStatus(videoList.model.get(result[0]).name,"upload fail");
        }
    }

    Connections {
        target: humanAuditorData
        function onSigProgress(result){
            videoList.model.get(result[0]).status=result[1]+"%";
        }
    }

    function insertVideoInfo(videoName, videoPath, startTime, nas_id, status) {
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3", "1.0",
                                               "Video Recording DB in QML",
                                               100000)
            db.transaction(function (tx) {
                // Create the database if it doesn't already exist
                tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)')

                // Add (another) greeting row
                tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                            [videoName, videoPath, startTime, nas_id,status])

                // Show all added greetings
                var rs = tx.executeSql('SELECT * FROM video_history2')

                var r = ""
                for (var i = 0; i < rs.rows.length; i++) {
                    r = rs.rows.item(i).name + ", " + rs.rows.item(
                                i).path + ", " + rs.rows.item(
                                i).datetime + ", " + rs.rows.item(i).nas_id + ", " + rs.rows.item(i).status
                    console.log(r)
                }
            })
        }
    }
}
