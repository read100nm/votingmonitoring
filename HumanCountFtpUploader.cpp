#include "HumanCountFtpUploader.h"
#include <Poco/Net/FTPClientSession.h>
#include <Poco/FileStream.h>

HumanCountFtpUploader::HumanCountFtpUploader()
{
    mRun = true;
    mListIndex = -1;
    mFilePath = "";
    mFileName = "";
    mHostName = "59.15.239.1";
}


HumanCountFtpUploader::~HumanCountFtpUploader()
{


}

void HumanCountFtpUploader::run()
{

    try{
        // connection info
        std::string id= "YDP";
        std::string pw= "Ab123456";

        // connect
        Poco::Net::FTPClientSession session(mHostName.toStdString(), 21);
        session.login(id,pw);
        
        // upload path
        std::string upload_path="Videos/"+mFileName.toStdString();

        // open the local target file
        Poco::FileStream upfile_stream(mFilePath.toStdString()
                                        ,std::ios::in);

        // open the ftp target file
        std::ostream& ftp_stream = session.beginUpload(upload_path);

        // make ftp buffer 
        int buffer_size = 8192;
        std::vector<char> buffer;
        buffer.resize(buffer_size);
        
        // calculate file size
        upfile_stream.seekg (0, upfile_stream.end);
        int length = upfile_stream.tellg();
        upfile_stream.seekg (0, upfile_stream.beg);

        // upload start
        int iteration_count = length / buffer_size + 1;
        for(int i=0; i< iteration_count; i++)
        {
            if(!mRun)
            {
                QVariantList value;
                value.append(mListIndex);
                value.append("사용자 중지");
                emit sigErrorUpload(value);
                return;
            }

            if(i != iteration_count)
            {
                upfile_stream.read(&buffer[0],buffer_size);
                ftp_stream.write(&buffer[0],buffer_size);
                if(i%100 == 0)
                {
                    float current_percent = (float)i/(float)iteration_count*100.0f;
                    QVariantList value;
                    value.append(mListIndex);
                    value.append((int)(current_percent+0.5));
                    
                    emit sigProgress(value);
                }    
            }
            else
            {
                upfile_stream.read(&buffer[0],buffer_size);
                ftp_stream.write(&buffer[0],upfile_stream.gcount());
            }
        }

        {
            QVariantList value;
            value.append(mListIndex);
            value.append(100);
            QString progress_str = QString::number(100);
            emit sigProgress(value);
        }
        
        QVariantList value;
        value.append(mListIndex);
        emit sigFinishUpload(value);
        

        session.endUpload();
        session.close();
    }
    catch(Poco::Exception& e)
    {
        QVariantList value;
        value.append(mListIndex);
        value.append(e.message().c_str());
        sigErrorUpload(value);
    }
}
