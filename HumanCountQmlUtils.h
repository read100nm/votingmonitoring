#ifndef HUMAN_COUNT_QML_UTILS_H_
#define HUMAN_COUNT_QML_UTILS_H_


#include <QObject>
#include <QString>
#include <qqml.h>


class HumanCountQmlUtils : public QObject
{
   Q_OBJECT
   QML_ELEMENT

public:
   Q_INVOKABLE QString readKakaoMapHtml(QString office_lat, QString office_lng, QString my_lat, QString my_lng);
   
   HumanCountQmlUtils(QObject *parent = nullptr);
   ~HumanCountQmlUtils();
protected:
};


#endif // DEI_CHAIN_QML_UTILS_H_
