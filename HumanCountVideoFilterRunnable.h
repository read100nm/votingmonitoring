#ifndef HUMAN_COUNT_VIDEO_FILTER_H_
#define HUMAN_COUNT_VIDEO_FILTER_H_

#include "HumanCountVideoFilter.h"
#include <QMutex>
//#include <opencv2/tracking.hpp>
#include <opencv2/opencv.hpp>

class HumanCountVideoFilterRunnable : public QVideoFilterRunnable
{
public:
    HumanCountVideoFilterRunnable( HumanCountVideoFilter* filter );
    virtual ~HumanCountVideoFilterRunnable();

    QVideoFrame run( QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags ) Q_DECL_OVERRIDE;

    bool QVideoFrameToQImage(QVideoFrame* input,cv::Mat& output,bool &isBGR);
    void initCountLine(cv::Point& line_pt1, cv::Point& line_pt2,int img_width,int img_height);
    void changeLineLeft();
    void changeLineRight();
    void changeLineRotateLeft();
    void changeLineRotateRight();
    void changeLineRotate90Right();
    void setAutoCountMode(bool on_off);
    void setLineSetMode(bool on_off);

    //void setMosaic(cv::Mat& inputMat, cv::Rect rect);

    void loadHumanDetModel();
protected:
    HumanCountVideoFilter* m_Filter;

    //DeepSort* DS;
#ifdef ANDROID_
    HumanCountDetector detector {nullptr};
#else
    HumanCountDetectorCV detector {nullptr};
#endif

    bool             mCountLineInit;
    CentroidTracker* mCentroidTracker;

    cv::Point mCountLinePt1;
    cv::Point mCountLinePt2;

    cv::Point mCountLineClipPt1;
    cv::Point mCountLineClipPt2;

    cv::Point mCountNormalLinePt1;
    cv::Point mCountNormalLinePt2;

    float     mCountNormalLineSize;

    std::vector<cv::Point> mCountPolygon[2];
    int                    mInsidePolygonID;

    bool      mAutoCountEnabled;
    bool      mLineSetModeEnabled;

    QMutex    mLineSetMutex;

    int       mCurrentImgWidth;
    int       mCurrentImgHeight;
    int       mLineSetImgWidth;
    int       mLineSetImgHeight;

    std::map<int,int> mPreviousAreaTrack;

    cv::Rect          mCropRect;
    cv::Ptr<cv::Tracker> mCSRTTracker;

    bool      mTrackingInited;

    
    int       mLoadedHumanDetMode;
    //cv::Mat   mMosaicImage;
    //bool      mMosaicImageInit;
    //std::map<int,int> mCurrentAreaTrack;

};

#endif
