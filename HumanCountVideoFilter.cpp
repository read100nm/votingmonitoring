#include "HumanCountVideoFilter.h"
#include "HumanCountVideoFilterRunnable.h"
#include <QPainter>
#include <QDebug>
#include <QDateTime>
#include <QGuiApplication>
#include <QScreen>

#ifdef ANDROID_
#include <onnxruntime_cxx_api.h>
#endif

#include <qthread.h>
#include <qdebug.h>
#include <QFile>
#include <QDir>

#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>

template <typename T>
static T vectorProduct(const std::vector<T> &v)
{
    return accumulate(v.begin(), v.end(), 1, std::multiplies<T>());
}

HumanCountVideoFilter::HumanCountVideoFilter(QObject *parent)
    : QAbstractVideoFilter(parent)
{
    mDisplayZoomValue = 1.0;
    mRunnable =nullptr;

    lineX1 = 319;
    lineX2 = 319;
    
    lineY1 = 0;
    lineY2 = 479;

    aiRotate = 0;

    mCurrentHumanDetMode = 0;
    mSetMosaic = true;
}

void HumanCountVideoFilter::changeLineLeft()
{
    if(mRunnable == nullptr)
        return;

    mRunnable->changeLineLeft();
}

void HumanCountVideoFilter::changeLineRight()
{
    if(mRunnable == nullptr)
        return;

    mRunnable->changeLineRight();
}

void HumanCountVideoFilter::changeLineRotateLeft()
{
    if(mRunnable == nullptr)
        return;

    mRunnable->changeLineRotateLeft();
}

void HumanCountVideoFilter::changeLineRotateRight()
{
    if(mRunnable == nullptr)
        return;

    mRunnable->changeLineRotateRight();
}

void HumanCountVideoFilter::changeLineRotate90Right()
{
    if(mRunnable == nullptr)
        return;

    mRunnable->changeLineRotate90Right();
}

void HumanCountVideoFilter::setAutoCountMode(bool on_off)
{
    if(mRunnable == nullptr)
        return;

    mRunnable->setAutoCountMode(on_off);
}

void HumanCountVideoFilter::setLineSetMode(bool on_off)
{
    if(mRunnable == nullptr)
        return;

    mRunnable->setLineSetMode(on_off);
}

void HumanCountVideoFilter::invokeSigAutoCount(int n)
{
    emit sigAutoCount(n);
}

QVideoFilterRunnable *HumanCountVideoFilter::createFilterRunnable()
{
    mRunnable = new HumanCountVideoFilterRunnable(this);
    return mRunnable;
}

void HumanCountVideoFilter::setHumanDetModel(int model_id)
{
   mCurrentHumanDetMode = model_id;
}

void HumanCountVideoFilter::setMosaicOnOff(bool mosaic_on_off)
{
    mSetMosaic = mosaic_on_off;
}

