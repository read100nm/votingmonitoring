#include "HumanCountUserInfoData.h"
#include <qdebug.h>
#include <qstandardpaths.h>
#include <qsettings.h>
#include <qdir.h>
#include <qdatetime.h>
#include <qurl.h>

#include <Poco/URI.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>

#include <sstream>

#include <jnimessenger.h>

#include <HumanCountAuditorData.h>


HumanCountUserInfoData *HumanCountUserInfoData::m_instance = nullptr;

HumanCountUserInfoData::HumanCountUserInfoData(QObject *parent) : QObject(parent)
{
    m_instance     = this;
    lastInsertedVtype ="no";
}

void HumanCountUserInfoData::loadUserInfoV1()
{
#ifdef ANDROID_
    electioncode1= JniMessenger::instance()->getKeyValueInPre("electioncode1","2022001");
    provincecode1= JniMessenger::instance()->getKeyValueInPre("provincecode1","");
    provincename1= JniMessenger::instance()->getKeyValueInPre("provincename1","");
    commitee1    = JniMessenger::instance()->getKeyValueInPre("commitee1","");
    placename1   = JniMessenger::instance()->getKeyValueInPre("placename1","");
    vtype1       = JniMessenger::instance()->getKeyValueInPre("vtype1","");
    username1    = JniMessenger::instance()->getKeyValueInPre("username1","");
    userphone1   = JniMessenger::instance()->getKeyValueInPre("userphone1","");
    inout1       = JniMessenger::instance()->getKeyValueInPre("inout1","");
    ampm1        = JniMessenger::instance()->getKeyValueInPre("ampm1",0).toInt();
    userid1      = JniMessenger::instance()->getKeyValueInPre("userid1","");
    lastInsertedVtype      = JniMessenger::instance()->getKeyValueInPre("lastInsertedVtype","no");
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";

    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO1");
    electioncode1= settings.value("electioncode1","2022001").toString();
    provincecode1= settings.value("provincecode1","").toString();
    provincename1= settings.value("provincename1","").toString();
    commitee1    = settings.value("commitee1","").toString();
    placename1   = settings.value("placename1","").toString();
    vtype1       = settings.value("vtype1","").toString();
    username1    = settings.value("username1","").toString();
    userphone1   = settings.value("userphone1","").toString();
    inout1       = settings.value("inout1","").toString();
    ampm1        = settings.value("ampm1",0).toInt();
    userid1      = settings.value("userid1","").toString();
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    lastInsertedVtype = settings.value("lastInsertedVtype","no").toString();
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::loadUserInfoV2()
{
#ifdef ANDROID_
    electioncode2= JniMessenger::instance()->getKeyValueInPre("electioncode2","2022001");
    provincecode2= JniMessenger::instance()->getKeyValueInPre("provincecode2","");
    provincename2= JniMessenger::instance()->getKeyValueInPre("provincename2","");
    commitee2    = JniMessenger::instance()->getKeyValueInPre("commitee2","");
    placename2   = JniMessenger::instance()->getKeyValueInPre("placename2","");
    vtype2       = JniMessenger::instance()->getKeyValueInPre("vtype2","");
    username2    = JniMessenger::instance()->getKeyValueInPre("username2","");
    userphone2   = JniMessenger::instance()->getKeyValueInPre("userphone2","");
    inout2       = JniMessenger::instance()->getKeyValueInPre("inout2","");
    ampm2        = JniMessenger::instance()->getKeyValueInPre("ampm2",0).toInt();
    userid2      = JniMessenger::instance()->getKeyValueInPre("userid2","");
    lastInsertedVtype      = JniMessenger::instance()->getKeyValueInPre("lastInsertedVtype","no");
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";

    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO2");
    electioncode2= settings.value("electioncode2","2022001").toString();
    provincecode2= settings.value("provincecode2","").toString();
    provincename2= settings.value("provincename2","").toString();
    commitee2    = settings.value("commitee2","").toString();
    placename2   = settings.value("placename2","").toString();
    vtype2       = settings.value("vtype2","").toString();
    username2    = settings.value("username2","").toString();
    userphone2   = settings.value("userphone2","").toString();
    inout2       = settings.value("inout2","").toString();
    ampm2        = settings.value("ampm2",0).toInt();
    userid2      = settings.value("userid2","").toString();
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    lastInsertedVtype = settings.value("lastInsertedVtype","no").toString();
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::loadUserInfoV3()
{
#ifdef ANDROID_
    electioncode3= JniMessenger::instance()->getKeyValueInPre("electioncode3","2022001");
    provincecode3= JniMessenger::instance()->getKeyValueInPre("provincecode3","");
    provincename3= JniMessenger::instance()->getKeyValueInPre("provincename3","");
    commitee3    = JniMessenger::instance()->getKeyValueInPre("commitee3","");
    placename3   = JniMessenger::instance()->getKeyValueInPre("placename3","");
    vtype3       = JniMessenger::instance()->getKeyValueInPre("vtype3","");
    username3    = JniMessenger::instance()->getKeyValueInPre("username3","");
    userphone3   = JniMessenger::instance()->getKeyValueInPre("userphone3","");
    inout3       = JniMessenger::instance()->getKeyValueInPre("inout3","");
    ampm3        = JniMessenger::instance()->getKeyValueInPre("ampm3",0).toInt();
    userid3      = JniMessenger::instance()->getKeyValueInPre("userid3","");
    lastInsertedVtype      = JniMessenger::instance()->getKeyValueInPre("lastInsertedVtype","no");
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";

    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO3");
    electioncode3= settings.value("electioncode3","2022001").toString();
    provincecode3= settings.value("provincecode3","").toString();
    provincename3= settings.value("provincename3","").toString();
    commitee3    = settings.value("commitee3","").toString();
    placename3   = settings.value("placename3","").toString();
    vtype3       = settings.value("vtype3","").toString();
    username3    = settings.value("username3","").toString();
    userphone3   = settings.value("userphone3","").toString();
    inout3       = settings.value("inout3","").toString();
    ampm3        = settings.value("ampm3",0).toInt();
    userid3      = settings.value("userid3","").toString();
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    lastInsertedVtype = settings.value("lastInsertedVtype","no").toString();
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::saveUserInfoV1()
{
#ifdef ANDROID_
    JniMessenger::instance()->setKeyValueInPre("electioncode1",electioncode1);
    JniMessenger::instance()->setKeyValueInPre("provincecode1",provincecode1);
    JniMessenger::instance()->setKeyValueInPre("provincename1",provincename1);
    JniMessenger::instance()->setKeyValueInPre("commitee1",commitee1);
    JniMessenger::instance()->setKeyValueInPre("placename1",placename1);
    JniMessenger::instance()->setKeyValueInPre("vtype1",vtype1);
    JniMessenger::instance()->setKeyValueInPre("username1",username1);
    JniMessenger::instance()->setKeyValueInPre("userphone1",userphone1);
    JniMessenger::instance()->setKeyValueInPre("inout1",inout1);
    JniMessenger::instance()->setKeyValueInPre("ampm1",QString::number(ampm1));
    JniMessenger::instance()->setKeyValueInPre("userid1",userid1);
    JniMessenger::instance()->setKeyValueInPre("lastInsertedVtype",lastInsertedVtype);
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO1");
    settings.setValue("electioncode1",electioncode1);
    settings.setValue("provincecode1",provincecode1);
    settings.setValue("provincename1",provincename1);
    settings.setValue("commitee1",commitee1);
    settings.setValue("placename1",placename1);
    settings.setValue("vtype1",vtype1);
    settings.setValue("username1",username1);
    settings.setValue("userphone1",userphone1);
    settings.setValue("inout1",inout1);
    settings.setValue("ampm1",ampm1);
    settings.setValue("userid1",userid1);
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    settings.setValue("lastInsertedVtype",lastInsertedVtype);
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::saveUserInfoV2()
{
#ifdef ANDROID_
    JniMessenger::instance()->setKeyValueInPre("electioncode2",electioncode2);
    JniMessenger::instance()->setKeyValueInPre("provincecode2",provincecode2);
    JniMessenger::instance()->setKeyValueInPre("provincename2",provincename2);
    JniMessenger::instance()->setKeyValueInPre("commitee2",commitee2);
    JniMessenger::instance()->setKeyValueInPre("placename2",placename2);
    JniMessenger::instance()->setKeyValueInPre("vtype2",vtype2);
    JniMessenger::instance()->setKeyValueInPre("username2",username2);
    JniMessenger::instance()->setKeyValueInPre("userphone2",userphone2);
    JniMessenger::instance()->setKeyValueInPre("inout2",inout2);
    JniMessenger::instance()->setKeyValueInPre("ampm2",QString::number(ampm2));
    JniMessenger::instance()->setKeyValueInPre("userid2",userid2);
    JniMessenger::instance()->setKeyValueInPre("lastInsertedVtype",lastInsertedVtype);
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO2");
    settings.setValue("electioncode2",electioncode2);
    settings.setValue("provincecode2",provincecode2);
    settings.setValue("provincename2",provincename2);
    settings.setValue("commitee2",commitee2);
    settings.setValue("placename2",placename2);
    settings.setValue("vtype2",vtype2);
    settings.setValue("username2",username2);
    settings.setValue("userphone2",userphone2);
    settings.setValue("inout2",inout2);
    settings.setValue("ampm2",ampm2);
    settings.setValue("userid2",userid2);
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    settings.setValue("lastInsertedVtype",lastInsertedVtype);
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::saveUserInfoV3()
{
#ifdef ANDROID_
    JniMessenger::instance()->setKeyValueInPre("electioncode3",electioncode3);
    JniMessenger::instance()->setKeyValueInPre("provincecode3",provincecode3);
    JniMessenger::instance()->setKeyValueInPre("provincename3",provincename3);
    JniMessenger::instance()->setKeyValueInPre("commitee3",commitee3);
    JniMessenger::instance()->setKeyValueInPre("placename3",placename3);
    JniMessenger::instance()->setKeyValueInPre("vtype3",vtype3);
    JniMessenger::instance()->setKeyValueInPre("username3",username3);
    JniMessenger::instance()->setKeyValueInPre("userphone3",userphone3);
    JniMessenger::instance()->setKeyValueInPre("inout3",inout3);
    JniMessenger::instance()->setKeyValueInPre("ampm3",QString::number(ampm3));
    JniMessenger::instance()->setKeyValueInPre("userid3",userid3);
    JniMessenger::instance()->setKeyValueInPre("lastInsertedVtype",lastInsertedVtype);
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO3");
    settings.setValue("electioncode3",electioncode3);
    settings.setValue("provincecode3",provincecode3);
    settings.setValue("provincename3",provincename3);
    settings.setValue("commitee3",commitee3);
    settings.setValue("placename3",placename3);
    settings.setValue("vtype3",vtype3);
    settings.setValue("username3",username3);
    settings.setValue("userphone3",userphone3);
    settings.setValue("inout3",inout3);
    settings.setValue("ampm3",ampm3);
    settings.setValue("userid3",userid3);
    settings.endGroup();

    settings.beginGroup("USER_INFOG");
    settings.setValue("lastInsertedVtype",lastInsertedVtype);
    settings.endGroup();
#endif
}


void HumanCountUserInfoData::loadUserVideoList()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFOV");
    
    v_name.clear();
    QVariant temp_variant = settings.value("v_name","");
    if(temp_variant.canConvert<QVariantList>())
    {
        QSequentialIterable iterable = temp_variant.value<QSequentialIterable>();

        for (const QVariant &v : iterable) {
            v_name.push_back(v);
        }
    }
        
    v_path.clear();
    temp_variant = settings.value("v_path","");
    if(temp_variant.canConvert<QVariantList>())
    {
         QSequentialIterable iterable = temp_variant.value<QSequentialIterable>();

        for (const QVariant &v : iterable) {
            v_path.push_back(v);
        }
    }
            
    v_datetime.clear();
    temp_variant = settings.value("v_datetime","");
    if(temp_variant.canConvert<QVariantList>())
    {
        QSequentialIterable iterable = temp_variant.value<QSequentialIterable>();

        for (const QVariant &v : iterable) {
            v_datetime.push_back(v);
        }
    }
                
                
    v_nas_id.clear();
    temp_variant = settings.value("v_nas_id","");
    if(temp_variant.canConvert<QVariantList>())
    {
        QSequentialIterable iterable = temp_variant.value<QSequentialIterable>();

        for (const QVariant &v : iterable) {
            v_nas_id.push_back(v);
        }
    }
                    
    v_status.clear();
    temp_variant = settings.value("v_status","");
    if(temp_variant.canConvert<QVariantList>())
    {
        QSequentialIterable iterable = temp_variant.value<QSequentialIterable>();

        for (const QVariant &v : iterable) {
            v_status.push_back(v);
        }
    }
    
    settings.endGroup();
}

void HumanCountUserInfoData::saveUserVideoList()
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFOV");
    settings.setValue("v_name",QVariant::fromValue(v_name));
    settings.setValue("v_path",QVariant::fromValue(v_path));
    settings.setValue("v_datetime",QVariant::fromValue(v_datetime));
    settings.setValue("v_nas_id",QVariant::fromValue(v_nas_id));
    settings.setValue("v_status",QVariant::fromValue(v_status));

    settings.endGroup();
}

void HumanCountUserInfoData::saveCountInfoDay()
{
    
#ifdef ANDROID_
    QDateTime datetime = QDateTime::currentDateTime();
    QString key = datetime.toString("yyyyMMdd");
    qDebug()<<key;
    QString key1_1 = "daym1_"+key;
    QString key1_2 = "daya1_"+key;
    QString key2_1 = "daym2_"+key;
    QString key2_2 = "daya2_"+key;
    QString key3_1 = "daym3_"+key;
    QString key3_2 = "daya3_"+key;
    JniMessenger::instance()->setKeyValueInPre(key1_1,manualCountDay1);
    JniMessenger::instance()->setKeyValueInPre(key1_2,autoCountDay1);
    JniMessenger::instance()->setKeyValueInPre(key2_1,manualCountDay2);
    JniMessenger::instance()->setKeyValueInPre(key2_2,autoCountDay2);
    JniMessenger::instance()->setKeyValueInPre(key3_1,manualCountDay3);
    JniMessenger::instance()->setKeyValueInPre(key3_2,autoCountDay3);
#else
    QDateTime datetime = QDateTime::currentDateTime();
    QString key = datetime.toString("yyyyMMdd");
    qDebug()<<key;
    QString key1_1 = "daym1_"+key;
    QString key1_2 = "daya1_"+key;
    QString key2_1 = "daym2_"+key;
    QString key2_2 = "daya2_"+key;
    QString key3_1 = "daym3_"+key;
    QString key3_2 = "daya3_"+key;
    
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO_COUNT");
    settings.setValue(key1_1,manualCountDay1);
    settings.setValue(key1_2,autoCountDay1);
    settings.setValue(key2_1,manualCountDay2);
    settings.setValue(key2_2,autoCountDay2);
    settings.setValue(key3_1,manualCountDay3);
    settings.setValue(key3_2,autoCountDay3);
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::loadCountInfoDay()
{
#ifdef ANDROID_
    QDateTime datetime = QDateTime::currentDateTime();
    QString key = datetime.toString("yyyyMMdd");
    qDebug()<<key;
    QString key1_1 = "daym1_"+key;
    QString key1_2 = "daya1_"+key;
    QString key2_1 = "daym2_"+key;
    QString key2_2 = "daya2_"+key;
    QString key3_1 = "daym3_"+key;
    QString key3_2 = "daya3_"+key;
    manualCountDay1      = JniMessenger::instance()->getKeyValueInPre(key1_1,"0");
    autoCountDay1        = JniMessenger::instance()->getKeyValueInPre(key1_2,"0");
    manualCountDay2      = JniMessenger::instance()->getKeyValueInPre(key2_1,"0");
    autoCountDay2        = JniMessenger::instance()->getKeyValueInPre(key2_2,"0");
    manualCountDay3      = JniMessenger::instance()->getKeyValueInPre(key3_1,"0");
    autoCountDay3        = JniMessenger::instance()->getKeyValueInPre(key3_2,"0");
#else
    QDateTime datetime = QDateTime::currentDateTime();
    QString key = datetime.toString("yyyyMMdd");
    qDebug()<<key;
    QString key1_1 = "daym1_"+key;
    QString key1_2 = "daya1_"+key;
    QString key2_1 = "daym2_"+key;
    QString key2_2 = "daya2_"+key;
    QString key3_1 = "daym3_"+key;
    QString key3_2 = "daya3_"+key;

    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USER_INFO_COUNT");
    manualCountDay1 = settings.value(key1_1,"0").toString();
    autoCountDay1   = settings.value(key1_2,"0").toString();
    manualCountDay2 = settings.value(key2_1,"0").toString();
    autoCountDay2   = settings.value(key2_2,"0").toString();
    manualCountDay3 = settings.value(key3_1,"0").toString();
    autoCountDay3   = settings.value(key3_2,"0").toString();
    settings.endGroup();

#endif
}

void HumanCountUserInfoData::sendAuditorData(QVariantList infos)
{
    try
    {
        QString url = "/vote/api/auditordata.php?electioncode=" + infos[0].toString().trimmed() +
                      "&placecode="+infos[1].toString().trimmed() +
                      "&auditorid="+infos[2].toString().trimmed() +
                      "&url="+infos[3].toString().trimmed() + 
                      "&gpsx="+infos[4].toString().trimmed() + 
                      "&gpsy="+infos[5].toString().trimmed() +
                      "&aicount="+infos[6].toString().trimmed() +
                      "&mancount="+infos[7].toString().trimmed() +
                      "&totalcount="+infos[8].toString().trimmed() +
                      "&x1="+infos[9].toString().trimmed()+
                      "&y1="+infos[10].toString().trimmed()+
                      "&x2="+infos[11].toString().trimmed()+
                      "&y2="+infos[12].toString().trimmed()+
                      "&appkey=1111&devicecd=1111";

        qDebug()<<url;
        Poco::URI uri("http://175.118.126.139"); 		
        Poco::Net::HTTPClientSession cs(uri.getHost(), uri.getPort()); 
        Poco::Net::HTTPRequest http_request(Poco::Net::HTTPRequest::HTTP_POST, url.toStdString(), Poco::Net::HTTPMessage::HTTP_1_1);
        http_request.setContentLength(0);

        Poco::Net::HTTPResponse http_response;
        cs.sendRequest(http_request);

        std::istream& rs = cs.receiveResponse(http_response); 	
        if (http_response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK)
		{
			std::stringstream ss;
            Poco::StreamCopier::copyStream64(rs,ss);
            qDebug()<< QString(ss.str().c_str());
		} 			
        else
        {
            qDebug()<<"Http request error!! code:"<<http_response.getStatus();
        }
    }
    catch(const Poco::Exception& e)
    {
        qDebug() << e.message().c_str();
    }
    catch(const std::exception& e)
    {
        qDebug() << e.what();
    }
}


QVariantList HumanCountUserInfoData::getFileListFromAppRoot()
{
    //QString currentDir = QString(QDir::currentPath());
    QVariantList returnList;
    QDir currentDir = QDir(QDir::currentPath());

    QFileInfoList list = currentDir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        //std::cout << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10)
        //                                        .arg(fileInfo.fileName()));
        //std::cout << std::endl;
        //20220601114338
        if( (fileInfo.fileName().size() > 18) && (fileInfo.fileName().right(4) == ".mp4"))
        {
            returnList.append(fileInfo.fileName());          // name
            returnList.append(fileInfo.filePath());          // path
            returnList.append(fileInfo.fileName().left(14)); // date   
            returnList.append(HumanCountAuditorData::instance()->mCurrentNasID); // nasid
            qDebug()<<"Nas ID:"<<HumanCountAuditorData::instance()->mCurrentNasID;
        }
    }

    return returnList;
}

void HumanCountUserInfoData::deleteFileByURL(QString path_url)
{
    if( QUrl::fromUserInput(path_url).isLocalFile())
    {
        QUrl temp_url(path_url);
        QString local_path = temp_url.toLocalFile();
        qDebug()<<"deleteFileByURL LocalPath:"<<local_path;
        QFile::remove(local_path);
    }
}

void HumanCountUserInfoData::loadAiSettings()
{
#ifdef ANDROID_
    ai_engine             = JniMessenger::instance()->getKeyValueInPre("ai_engine","0");
    ai_rec_fps            = JniMessenger::instance()->getKeyValueInPre("ai_rec_fps","0");
    ai_rec_resolutions    = JniMessenger::instance()->getKeyValueInPre("ai_rec_resolutions","0");
    ai_data_transfer_time = JniMessenger::instance()->getKeyValueInPre("ai_data_transfer_time","0");
    ai_human_masking      = JniMessenger::instance()->getKeyValueInPre("ai_human_masking","0");
    ai_rec_save_pos       = JniMessenger::instance()->getKeyValueInPre("ai_rec_save_pos","0");
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";

    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("AI_SETTINGS");
    ai_engine             = settings.value("ai_engine","0").toString();
    ai_rec_fps            = settings.value("ai_rec_fps","0").toString();
    ai_rec_resolutions    = settings.value("ai_rec_resolutions","0").toString();
    ai_data_transfer_time = settings.value("ai_data_transfer_time","0").toString();
    ai_human_masking      = settings.value("ai_human_masking","0").toString();
    ai_rec_save_pos       = settings.value("ai_rec_save_pos","0").toString();
    settings.endGroup();
#endif
}

void HumanCountUserInfoData::saveAiSettings()
{
#ifdef ANDROID_
    JniMessenger::instance()->setKeyValueInPre("ai_engine",ai_engine);
    JniMessenger::instance()->setKeyValueInPre("ai_rec_fps",ai_rec_fps);
    JniMessenger::instance()->setKeyValueInPre("ai_rec_resolutions",ai_rec_resolutions);
    JniMessenger::instance()->setKeyValueInPre("ai_data_transfer_time",ai_data_transfer_time);
    JniMessenger::instance()->setKeyValueInPre("ai_human_masking",ai_human_masking);
    JniMessenger::instance()->setKeyValueInPre("ai_rec_save_pos",ai_rec_save_pos);
#else
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString filename = "vmuserinfo";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("AI_SETTINGS");
    settings.setValue("ai_engine",ai_engine);
    settings.setValue("ai_rec_fps",ai_rec_fps);
    settings.setValue("ai_rec_resolutions",ai_rec_resolutions);
    settings.setValue("ai_data_transfer_time",ai_data_transfer_time);
    settings.setValue("ai_human_masking",ai_human_masking);
    settings.setValue("ai_rec_save_pos",ai_rec_save_pos);
    settings.endGroup();
#endif
}
