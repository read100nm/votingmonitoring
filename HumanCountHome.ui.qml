import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.12
import QtMultimedia 5.12
import HumanCountLib 1.0
import QtQuick.LocalStorage 2.15

Page {
    id: mainhome_page
    width: stackView.width
    height: stackView.height
    title: qsTr("Home")

    //property int manualCount_file: 0
    //property int autoCount_file:0
    property int manualCount_day:0
    property int autoCount_day:0
    property int airotate:0
    property bool isAutoDetecting: false
    property bool isVideoRecording: false
    property bool isLineset: false
    property real cameraDigitalZoomValue: 1.0
    property real cameraOpticalZoomValue: 1.0
    property var recordStartTime
    property var recordFileName
    property var recordLocation
    property var externalDir: null
    //property bool isServerSend: false
    //property string userid: "a0001"


    // 0을 채워주는 pad함수
    function pad2(n) {
        return n < 10 ? '0' + n : n
    }

    // 000,000을 채워주는 함수
    function commaWithZeroPad(n) {
        
        if(n >=0)
        {
            var str_n = n.toString()
            var padsize = 6 - str_n.length
            var pad_str = "0"
            pad_str = pad_str.repeat(padsize)
            var padded_n = pad_str + str_n
            return padded_n;
        }
        else
        {
            var str_n = n.toString()
            var padsize = 5 - str_n.length
            var pad_str = "0"
            pad_str = pad_str.repeat(padsize)
            var padded_n = pad_str + str_n
            return padded_n;
        }
    }

    function delayRun(delayTime, cb) {
        timer = new Timer();
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    function startRecord(currentTime){
        //make file name
        var timeString = currentTime.getFullYear().toString() 
                         + pad2(currentTime.getMonth() + 1) 
                         + pad2(currentTime.getDate()) 
                         + pad2(currentTime.getHours()) 
                         + pad2(currentTime.getMinutes()) 
                         + pad2(currentTime.getSeconds());
        
        var fileName = timeString + "-" + main_window.userId + ".mp4"
        var filePath;
        
        if(RunPlatform == "ANDROID")
        {
            if(recordLocation == 0)
            {
                filePath = "file://" +AppDirPath + "/" + fileName;
            }
            else if(recordLocation == 1)
            {
                filePath = "" +MoiveLocationPath2 + "/" + fileName;
                console.log(filePath);
            }    
            
            //else if(recordLocation == 1 && externalDir != null)
            //{
            //    filePath = "file://" +externalDir[0] + "/" + fileName;
            //}
            //else if(recordLocation == 1 && externalDir == null)
            //{
            //    filePath = "file://" +MoiveLocationPath2 + "/" + fileName;
            //}    
        }
        else
        {
            filePath = "file://" +MoiveLocationPath + "/" + fileName;
        }    

        // recording 시작
        camera.videoRecorder.outputLocation = filePath
        camera.videoRecorder.record();

        delayRunTimer.setTimeout(1000, function(){
            console.log("Rec State:" +camera.videoRecorder.recorderState);
            console.log("Rec Status:" +camera.videoRecorder.recorderStatus);
            if((camera.videoRecorder.recorderStatus == 5) && (camera.videoRecorder.recorderState==1))
            {
                recordStartTime = currentTime;
                recordFileName = fileName;
                insertVideoInfo(fileName, filePath, timeString,humanAuditorData.nasID,
                                    "start record");
                //manualCount_file = 0;
                //autoCount_file   = 0;
                isVideoRecording = true;
            }
            else
            {
                
                main_window.openMessagePopup("녹화를 시작할 수 없습니다. 저장소, 해상도, FPS를 변경 후 다시 시작해보세요.");
            }
        });
    }

    function stopRecord(){
        updateVideoInfo(recordFileName, "finish record");
        camera.videoRecorder.stop();
    }

    HumanCountHomeSettingPopup{
        id: settingPopup
    }

    Timer {
        id: delayRunTimer
        function setTimeout(delayTime,cb) {
            delayRunTimer.interval = delayTime;
            delayRunTimer.repeat = false;
            delayRunTimer.triggered.connect(cb);
            delayRunTimer.triggered.connect(function release () {
                delayRunTimer.triggered.disconnect(cb); // This is important
                delayRunTimer.triggered.disconnect(release); // This is important as well
            });
            delayRunTimer.start();
        }
    }

    // 서버에 데이터를 전송하는 타이머
    Timer {
        id: sendDataTimer
        running: false
        repeat: true
        interval: 60000

        onTriggered:{
            //if(isServerSend)
            {
                sendCurrentVotingInfoToServer();
            }
        }
    }

    // 날짜 표시를 위한 Time 객체
    Timer {
        id: recordTimer
        running: true
        repeat: true
        interval: 1000
        onTriggered: {
            // 1. 30분 단위 파일 분리 
            // 2. 녹화 시간 표시 및 컬러 변경 
            // 3. 현재 시간 표시
            // 4. 녹화가 중지되어 있음 다시시작!!

            var currentTime = new Date()

            var elapsed = 0
            var min_value = 0
            var sec_value = 0

            // 1. 30분 단위 파일 분리
            if (isVideoRecording) {
                //elapsed = (currentTime.getTime(
                //               ) - recordStartTime.getTime()) / 1000
                elapsed = camera.videoRecorder.duration / 1000; //parseInt(elapsed)
                min_value = parseInt(elapsed / 60);
                sec_value = parseInt(elapsed - (min_value * 60));

                if ((currentTime.getMinutes() == 0 || currentTime.getMinutes(
                         ) == 30) && (currentTime.getSeconds() == 0) && (elapsed >= 120)) {
                    camera.videoRecorder.stop();
                    updateVideoInfo(recordFileName, "finish record");

                    //make file name
                    startRecord(currentTime);
                }

                if (recTimeText.color == "#ff0000") {
                    recTimeText.color = "#000000";
                } else {
                    recTimeText.color = "#ff0000";
                }


            } else {
                recTimeText.color = "#000000";
            }

            // 2. 녹화 시간 표시 및 컬러 변경 
            var rec_time_string = "(REC time %1:%2)";
            recTimeText.text = rec_time_string.arg(pad2(min_value)).arg(pad2(sec_value));

            // 3. 현재 시간 표시
            var currentTimeStr = currentTime.getFullYear().toString() 
                                 + "/" + pad2(currentTime.getMonth() + 1) 
                                 + "/" + pad2(currentTime.getDate()) + " "
                                 + pad2(currentTime.getHours()) 
                                 + ":" + pad2(currentTime.getMinutes())

            var current_time_string = "%1";
            currentTimeText.text = current_time_string.arg(currentTimeStr);

            // 4. 녹화가 중지되어 있음 다시시작!!
            console.log("active:"+main_window.active)
            if (isVideoRecording && (camera.videoRecorder.recorderState == 0) )
            {
                //camera.videoRecorder.stop()
                updateVideoInfo(recordFileName, "finish record")

                
                if(JniMessenger.getCurrentActivityVisible() && RunPlatform == "ANDROID")
                {
                    //make file name
                    console.log("try to start record");
                 
                    startRecord(currentTime);
                }
                
                //camera.videoRecorder.record();
            }
        }
    }

    // 카메라 객체
    Camera {
        id: camera
        //digitalZoom: 2.0
        captureMode: Camera.CaptureVideo
        deviceId: QtMultimedia.availableCameras[0] ? QtMultimedia.availableCameras[0].deviceId : ""

        focus.focusMode: CameraFocus.FocusContinuous
        focus.focusPointMode: CameraFocus.FocusPointAuto

        onDeviceIdChanged: {
            focus.focusMode = CameraFocus.FocusContinuous
            focus.focusPointMode = CameraFocus.FocusPointAuto
        }

        //viewfinder.resolution: "480x640"
        //videoRecorder.audioEncodingMode: CameraRecorder.ConstantBitRateEncoding
        //videoRecorder.videoEncodingMode: CameraRecorder.ConstantBitRateEncoding
        videoRecorder.audioBitRate: 64000
        videoRecorder.videoBitRate: 2000000
        videoRecorder.mediaContainer: "mp4"
        videoRecorder.frameRate: 30
        videoRecorder.resolution: "640x480"
        videoRecorder.videoCodec: "avc1"
        videoRecorder.audioCodec: "audio/x-ac3"
        //videoRecorder.outputLocation: "/home/smith/video.mp4"
        onError: console.log("camera error:" + errorString)
    }

    HumanCountVideoFilter {
        id: humanCountFilter
        videoOutputOrientation: videoOutput.orientation
        onSigAutoCount: {
            addAutoCount(n);
        }
        videoOutputWidth: videoOutput.width
        videoOutputHeight: videoOutput.height
    }

    MediaPlayer {
        id: testMediaplayer
        //autoPlay: true
        //source: "file:///home/john/video4.mp4"
    }

    Rectangle {
        x: 0
        y: 0
        width: mainhome_page.width
        height: mainhome_page.height * 0.438
        border.color: "#000000"
        border.width: 1
        color: "#000000"
    }

    VideoOutput {
        id: videoOutput
        filters: [humanCountFilter]
        x: 0
        y: 0
        width: mainhome_page.width
        height: mainhome_page.height * 0.438
        fillMode: VideoOutput.PreserveAspectCrop
        autoOrientation: true

        Text {
            x: parent.width * 0.85
            y: 0
            width: parent.width * 0.1
            height: parent.height * 0.1
            text: qsTr("REC")
            color: "#EE0000"
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            visible: isVideoRecording
        }
        //scale : width/height
        //transformOrigin: Item.Center
        source: camera

        Rectangle {
            x: parent.width * 0.82
            y: parent.height * 0.8
            width: parent.width * 0.16
            height: parent.height * 0.18
            color: "transparent"
            border.color: "#CFE9F0"
            border.width: 2
            Image{
                x:2
                y:2
                width: parent.width-4
                height: parent.height-4
                fillMode: Image.PreserveAspectFit
                source: "images/icon_human.png"
            }
        }

        Text {
            x: parent.width * 0.82
            y: parent.height * 0.8
            width: parent.width * 0.16
            height: parent.height * 0.16
            text: qsTr("최소검출")
            color: "#0000EE"
            font.bold: true
            font.pixelSize: height * 0.2
            verticalAlignment: Text.AlignBottom
            horizontalAlignment: Text.AlignHCenter
        }

        //source: testMediaplayer
        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.03
            width: parent.width * 0.1
            height: parent.height * 0.15
            color: lineSet_moveLeft.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset
            Text {
                anchors.fill: parent
                fontSizeMode: Text.HorizontalFit
                minimumPixelSize: 2
                font.pixelSize: 72 
                text: qsTr("<-좌")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            MouseArea {
                id: lineSet_moveLeft
                anchors.fill: parent
                onClicked: {
                    humanCountFilter.changeLineLeft()
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.21
            width: parent.width * 0.1
            height: parent.height * 0.15
            color: lineSet_moveRight.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset
            Text {
                anchors.fill: parent
                fontSizeMode: Text.HorizontalFit
                minimumPixelSize: 2
                font.pixelSize: 72 
                text: qsTr("우->")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            MouseArea {
                id: lineSet_moveRight
                anchors.fill: parent
                onClicked: {
                    humanCountFilter.changeLineRight()
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.39
            width: parent.width * 0.1
            height: parent.height * 0.15
            color: lineSet_rotateRight.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset
            Text {
                anchors.fill: parent
                fontSizeMode: Text.HorizontalFit
                minimumPixelSize: 2
                font.pixelSize: 72 
                text: qsTr("회전->")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            MouseArea {
                id: lineSet_rotateRight
                anchors.fill: parent
                onClicked: {
                    humanCountFilter.changeLineRotateRight()
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.57
            width: parent.width * 0.1
            height: parent.height * 0.15
            color: lineSet_rotateLeft.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset
            Text {
                anchors.fill: parent
                fontSizeMode: Text.HorizontalFit
                minimumPixelSize: 2
                font.pixelSize: 72 
                text: qsTr("<-회전")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            MouseArea {
                id: lineSet_rotateLeft
                anchors.fill: parent
                onClicked: {
                    humanCountFilter.changeLineRotateLeft()
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.78
            width: parent.width * 0.1
            height: parent.height * 0.19
            color: ai_r1.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset && (airotate == 0)
            radius: height / 12
            Rectangle {
                x: parent.width *0.3
                y: parent.height *0.075
                width: parent.width * 0.4
                height: parent.height * 0.05
                color: "blue"
            }
            Rectangle {
                x: 0
                y: parent.height *0.15
                width: parent.width
                height: parent.height * 0.02
                color: "black"
            }
            Rectangle {
                x: 0
                y: parent.height *0.8
                width: parent.width
                height: parent.height * 0.02
                color: "black"
            }
            Rectangle {
                x: parent.width *0.45
                y: parent.height *0.85
                width: parent.height * 0.1
                height: parent.height * 0.1
                color: "blue"
                radius: width * 0.5
            }
            Text{
                x: parent.width *0.1
                y: parent.height *0.2
                width: parent.width * 0.8
                height:parent.height * 0.15
                text: "위쪽"
                minimumPixelSize: 10
                font.pixelSize: 72 
                fontSizeMode: Text.Fit
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Image{
                x: parent.width *0.2
                y: parent.height *0.35
                width: parent.width * 0.6
                height:parent.width * 0.6
                source: "images/icon_arrow_up.png"
            }
            MouseArea {
                id: ai_r1
                anchors.fill: parent
                onClicked: {
                    airotate = 90;
                    humanCountFilter.aiRotate = airotate;
                    humanCountFilter.changeLineRotate90Right();
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.78 + (parent.height * 0.19 - parent.width*0.1)/2
            width: parent.height * 0.19
            height: parent.width * 0.1
            color: ai_r2.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset && (airotate == 90)
            radius: width / 12
            Rectangle {
                x: parent.width *0.915
                y: parent.height *0.3
                width: parent.width * 0.05
                height: parent.height * 0.4
                color: "blue"
            }
            Rectangle {
                x: parent.width *0.85
                y: 0 
                width: parent.width *0.02
                height: parent.height
                color: "black"
            }
            Rectangle {
                x: parent.width *0.2
                y: 0
                width: parent.width * 0.02
                height: parent.height
                color: "black"
            }
            Rectangle {
                x: parent.width *0.05
                y: parent.height *0.45
                width: parent.width * 0.1
                height: parent.width * 0.1
                color: "blue"
                radius: width * 0.5
            }
            Text{
                x: parent.width *0.4
                y: parent.height *0.1
                width: parent.width * 0.6
                height:parent.height * 0.15
                text: "위쪽"
                minimumPixelSize: 10
                font.pixelSize: 72 
                fontSizeMode: Text.Fit
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.hintingPreference: Font.PreferVerticalHinting
                renderType: Text.NativeRendering
            }
            Image{
                x: parent.width *0.32
                y: parent.height *0.2
                width: parent.height * 0.6
                height:parent.height * 0.6
                source: "images/icon_arrow_right.png"
            }
            MouseArea {
                id: ai_r2
                anchors.fill: parent
                onClicked: {
                    airotate = 270;
                    humanCountFilter.aiRotate = airotate;
                    humanCountFilter.changeLineRotate90Right();
                    humanCountFilter.changeLineRotate90Right();
                }
            }
        }

        Rectangle {
            x: parent.width * 0.04
            y: parent.height * 0.78 + (parent.height * 0.19 - parent.width*0.1)/2
            width: parent.height * 0.19
            height: parent.width * 0.1
            color: ai_r4.pressed ? "#CFE9F0" : "#ffffff"
            visible: isLineset &&(airotate == 270)
            radius: width / 12
            Rectangle {
                x: parent.width *0.075
                y: parent.height *0.3
                width: parent.width * 0.05
                height: parent.height * 0.4
                color: "blue"
            }
            Rectangle {
                x: parent.width *0.15
                y: 0 
                width: parent.width *0.02
                height: parent.height
                color: "black"
            }
            Rectangle {
                x: parent.width *0.8
                y: 0
                width: parent.width * 0.02
                height: parent.height
                color: "black"
            }
            Rectangle {
                x: parent.width *0.85
                y: parent.height *0.45
                width: parent.width * 0.1
                height: parent.width * 0.1
                color: "blue"
                radius: width * 0.5
            }
            Text{
                x: parent.width *0.1
                y: parent.height *0.1
                width: parent.width * 0.6
                height:parent.height * 0.15
                text: "위쪽"
                minimumPixelSize: 10
                font.pixelSize: 72 
                fontSizeMode: Text.Fit
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.hintingPreference: Font.PreferVerticalHinting
                renderType: Text.NativeRendering
            }
            Image{
                x: parent.width *0.25
                y: parent.height *0.2
                width: parent.height * 0.6
                height:parent.height * 0.6
                source: "images/icon_arrow_left.png"
            }
            MouseArea {
                id: ai_r4
                anchors.fill: parent
                onClicked: {
                    airotate = 0;
                    humanCountFilter.aiRotate = airotate;
                    humanCountFilter.changeLineRotate90Right();
                }
            }
        }

        
    }

    Rectangle {
        x: 0
        y: parent.height * 0.438
        width: parent.width
        height: parent.height * 0.111472975
        color: "#F6F9FB"
        Text {
            x:0
            y:0
            width: parent.width / 4
            height: parent.height / 3
            text: qsTr("AI")
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            
        }

        Text {
            x:0
            y:parent.height / 3
            width: parent.width / 4
            height: parent.height *2 / 3
            text: autoCount_day.toString();
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Text {
            x:parent.width / 4
            y:0
            width: parent.width / 4
            height: parent.height / 3
            text: qsTr("수동")
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            
        }

        Text {
            x:parent.width / 4
            y:parent.height / 3
            width: parent.width / 4
            height: parent.height *2 / 3
            text: manualCount_day.toString();
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Text {
            x:parent.width *2 / 4
            y:0
            width: parent.width / 4
            height: parent.height / 3
            text: qsTr("합계")
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            
        }

        Text {
            x:parent.width *2 / 4
            y:parent.height / 3
            width: parent.width / 4
            height: parent.height *2 / 3
            text: (autoCount_day+manualCount_day).toString();
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Rectangle {
            x:parent.width *3/ 4
            y:parent.height * 0.2
            width: parent.width / 4
            height: parent.height *0.6
            
            border.color: "#CFE9F0"
            border.width: 1
            radius: width * 0.02
            color: initcount_ma.pressed==true ? "#CFE9F0" : "#ffffff"

            
            MouseArea {
                id: initcount_ma
                anchors.fill: parent
                onClicked: {
                    var return_value = main_window.openYesNoPopup("현 시점 숫자는 서버로 전송되고, 0으로 초기화됩니다. 초기화 하시겠습니까?",
                        function(){
                            sendCurrentVotingInfoToServer();
                            manualCount_day = 0;
                            autoCount_day = 0;
                            if(main_window.userVtype == "사전1")
                            {
                                humanCountUserInfoData.autoCountDay1 = autoCount_day;
                                humanCountUserInfoData.manualCountDay1 = manualCount_day;
                            }
                            else if(main_window.userVtype =="사전2")
                            {
                                humanCountUserInfoData.autoCountDay2 = autoCount_day;
                                humanCountUserInfoData.manualCountDay2 = manualCount_day;
                            }
                            else if(main_window.userVtype =="선거일")
                            {
                                humanCountUserInfoData.autoCountDay3 = autoCount_day;
                                humanCountUserInfoData.manualCountDay3 = manualCount_day;
                            }
                            humanCountUserInfoData.saveCountInfoDay();

                        },
                        null);

                    //if (isServerSend) 
                    //{
                    //    isServerSend = false;
                    //} 
                    //else
                    //{
                    //    isServerSend = true;
                    //    manualCount_file = 0;
                    //    autoCount_file   = 0;
                    //}
                }
            }

            Text {
                anchors.fill: parent
                text: "초기화"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                minimumPixelSize: 2
                font.pixelSize: 72 
                fontSizeMode: Text.Fit
            }
        }
    }

    /*Text {
        id: countText2
        x: parent.width * 0.066666667
        y: parent.height * 0.438
        width: parent.width * 0.866666667
        height: parent.height * 0.111472975
        text: qsTr("누적AI:") + commaWithZeroPad(autoCount_day) + qsTr(
            "   누적수동:") + commaWithZeroPad(manualCount_day) + 
            qsTr("   누적합계:") + commaWithZeroPad(autoCount_day + manualCount_day);
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }*/

    Rectangle {
        x: mainhome_page.width * 0.02
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: zoomPlus_ma.pressed == true ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_zoom_plus.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: zoomPlus_ma
            anchors.fill: parent
            onClicked: {
                if (camera.maximumOpticalZoom > cameraOpticalZoomValue) {
                    cameraOpticalZoomValue += 0.5
                    camera.opticalZoom = cameraOpticalZoomValue
                } else if (camera.maximumDigitalZoom > cameraDigitalZoomValue) {
                    cameraDigitalZoomValue += 0.5
                    camera.digitalZoom = cameraDigitalZoomValue
                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.02
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("줌인")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        x: mainhome_page.width * 0.184542857
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: zoomMinus_ma.pressed == true ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_zoom_minus.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: zoomMinus_ma
            anchors.fill: parent
            onClicked: {
                if (1.0 < cameraOpticalZoomValue) {
                    cameraOpticalZoomValue -= 0.5
                    camera.opticalZoom = cameraOpticalZoomValue
                } else if (1.0 < cameraDigitalZoomValue) {
                    cameraDigitalZoomValue -= 0.5
                    camera.digitalZoom = cameraDigitalZoomValue
                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.184542857
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("줌아웃")
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        x: mainhome_page.width * 0.349085714
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: isLineset == true ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_lineset.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: lineSet_ma
            anchors.fill: parent
            onClicked: {
                if (isLineset) {
                    humanCountFilter.setLineSetMode(false)
                    isLineset = false
                } else {
                    humanCountFilter.setLineSetMode(true)
                    humanCountFilter.setAutoCountMode(false);
                    isLineset = true
                    isAutoDetecting = false;         
                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.349085714
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("라인 셋")
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        x: mainhome_page.width * 0.513628571
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: isVideoRecording ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_rec_start.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: recStart_ma
            anchors.fill: parent
            onClicked: {
                if (isVideoRecording) {
                    isVideoRecording = false
                    updateVideoInfo(recordFileName, "finish record")
                    camera.videoRecorder.stop();
                    //manualCount_file = 0;
                    //autoCount_file   = 0;
                } else {
                    //make file name
                    var currentTime = new Date()
                    startRecord(currentTime);
                    /*var timeString = currentTime.getFullYear(
                                ).toString() + pad2(currentTime.getMonth(
                                                        ) + 1) + pad2(currentTime.getDate(
                                                                          )) + pad2(
                                currentTime.getHours()) + pad2(currentTime.getMinutes(
                                                                   )) + pad2(
                                currentTime.getSeconds())
                    var fileName = timeString + "-" + main_window.userId + ".mp4"
                    var filePath = "file://" + AppDirPath + "/" + fileName

                    camera.videoRecorder.outputLocation = filePath
                    camera.videoRecorder.record()
                    isVideoRecording = true
                    recordStartTime = currentTime
                    recordFileName = fileName
                    insertVideoInfo(fileName, filePath, timeString,humanAuditorData.nasID,
                                    "start record")*/
                    

                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.513628571
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("녹화")
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        x: mainhome_page.width * 0.678171428
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: isAutoDetecting ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_auto_count.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: autoCount_ma
            anchors.fill: parent
            onClicked: {
                if (isAutoDetecting) {
                    humanCountFilter.setAutoCountMode(false);
                    isAutoDetecting = false;
                } else {
                    humanCountFilter.setAutoCountMode(true);
                    humanCountFilter.setLineSetMode(false);
                    isAutoDetecting = true;
                    isLineset       = false;
                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.678171428
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("자동")
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Rectangle {
        x: mainhome_page.width * 0.842714285
        y: mainhome_page.height * 0.473529412 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        color: setting_ma.pressed==true ? "#CFE9F0" : "#ffffff"
        border.width: 1
        border.color: "#CFE9F0"
        radius: width * 0.5
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height: parent.width / 2
            width: parent.height / 2
            source: "images/icon_setting.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignTop
        }
        MouseArea {
            id: setting_ma
            anchors.fill: parent
            onClicked: {
                if (isAutoDetecting || isVideoRecording) {
                    main_window.openMessagePopup("녹화 및 자동 카운트 기능이 동작 중일때는 설정이 불가능합니다.");
                }
                else
                {
                    settingPopup.openMessagePopup(reconfigFromSettings);
                }
            }
        }
    }

    Text {
        x: mainhome_page.width * 0.842714285
        y: mainhome_page.height * 0.55 + (mainhome_page.height*0.09)
        width: parent.width < parent.height ? parent.width * 0.137142857 : parent.height * 0.0647
        height: parent.height * 0.026470588
        text: qsTr("설정")
        font.family: "Roboto"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }


    Rectangle {
        x: 0
        y: mainhome_page.height * 0.591764706 + (mainhome_page.height*0.09)
        width: mainhome_page.width
        //height: mainhome_page.height * 0.314705882
        height: mainhome_page.height * 0.254705882 * 0.7
        color: "#F6F9FB"

        Text {
            id: voteTypeText
            x: parent.width * 0.01
            y: parent.height * 0.02
            width: parent.width * 0.1
            height: parent.height * 0.47
            text: qsTr("사전1")
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Text {
            id: currentTimeText
            x: parent.width * 0.11
            y: parent.height * 0.02
            width: parent.width * 0.45
            height: parent.height * 0.47
            text: qsTr("2022/06/01 12:35")
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Text {
            id: recTimeText
            x: parent.width * 0.56
            y: parent.height * 0.02
            width: parent.width * 0.43
            height: parent.height * 0.47
            text: qsTr("(REC time 00:00)")
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }

        Text {
            id: posText
            x: parent.width * 0.01
            y: parent.height * 0.5
            width: parent.width * 0.98
            height: parent.height * 0.47
            text: qsTr("촬영장소: -")
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 72 
            fontSizeMode: Text.Fit
        }
    }

    Rectangle {
        x: mainhome_page.width * 0.066666667
        y: mainhome_page.height * 0.870588235
        width: mainhome_page.width * 0.4
        height: mainhome_page.height * 0.1
        color: manualCountPlus_ma.pressed ? "#CFE9F0" : "#FFFFFF"
        radius: height / 2
        border.color: "#CFE9F0"
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.583333333
            height: mainhome_page.height * 0.5
            text: qsTr("인원수 +")
            font.bold: true
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: manualCountPlus_ma
            anchors.fill: parent
            onClicked: {
                addManualCount();
            }
        }
    }

    Rectangle {
        x: mainhome_page.width * 0.533333333
        y: mainhome_page.height * 0.870588235
        width: mainhome_page.width * 0.4
        height: mainhome_page.height * 0.1
        color: manualCountMinus_ma.pressed ? "#CFE9F0" : "#FFFFFF"
        radius: height / 2
        border.color: "#CFE9F0"
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.583333333
            height: mainhome_page.height * 0.5
            text: qsTr("인원수 -")
            font.bold: true
            font.family: "Roboto"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: manualCountMinus_ma
            anchors.fill: parent
            onClicked: {
                minusManualCount();
            }
        }
    }

    function insertVideoInfo(videoName, videoPath, startTime, nas_id, status) {
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3", "1.0",
                                               "Video Recording DB in QML",
                                               100000)
            db.transaction(function (tx) {
                // Create the database if it doesn't already exist
                tx.executeSql(
                            'CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)')

                // Add (another) greeting row
                tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                            [videoName, videoPath, startTime, nas_id,status])

                // Show all added greetings
                var rs = tx.executeSql('SELECT * FROM video_history2')

                var r = ""
                for (var i = 0; i < rs.rows.length; i++) {
                    r = rs.rows.item(i).name + ", " + rs.rows.item(
                                i).path + ", " + rs.rows.item(
                                i).datetime + ", " + rs.rows.item(i).nas_id + ", " + rs.rows.item(i).status
                    console.log(r)
                }
            })
        }
        else
        {
            humanCountUserInfoData.loadUserVideoList();
            var v_name = humanCountUserInfoData.v_name;//.push(videoName);
            var v_path = humanCountUserInfoData.v_path;
            var v_datetime =  humanCountUserInfoData.v_datetime;
            var v_nas_id   = humanCountUserInfoData.v_nas_id;
            var v_status   = humanCountUserInfoData.v_status;

            v_name.push(videoName);
            v_path.push(videoPath);
            v_datetime.push(startTime);
            v_nas_id.push(nas_id);
            v_status.push(status);
            console.log("Saved!!! into DB");
            
            humanCountUserInfoData.v_name = v_name;
            humanCountUserInfoData.v_path = v_path;
            humanCountUserInfoData.v_datetime = v_datetime;
            humanCountUserInfoData.v_nas_id = v_nas_id;
            humanCountUserInfoData.v_status = v_status;

            for(var i=0; i< v_path.length; i++)
            {
                console.log(v_path[i]);
            }

            humanCountUserInfoData.saveUserVideoList();

        }
        
    }

    function updateVideoInfo(videoName, status) {
        if(RunPlatform == "ANDROID")
        {
            var db = LocalStorage.openDatabaseSync("VideoRecordingDB3", "1.0",
                                                "Video Recording DB in QML",
                                                100000)
            db.transaction(function (tx) {
                // Create the database if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)');

                // Add (another) greeting row
                tx.executeSql('UPDATE video_history2 SET status=? WHERE name=?',
                            [status, videoName])

                // Show all added greetings
                var rs = tx.executeSql('SELECT * FROM video_history2')

                var r = ""
                for (var i = 0; i < rs.rows.length; i++) {
                    r = rs.rows.item(i).name + ", " + rs.rows.item(
                                i).path + ", " + rs.rows.item(
                                i).datetime + ", " + rs.rows.item(i).nas_id + ", " + rs.rows.item(i).status
                    console.log(r)
                }
            })
        }
        else
        {
            humanCountUserInfoData.loadUserVideoList();
            console.log("called updateVideoInfo!!!!");
            console.log(videoName);
            var v_name     = humanCountUserInfoData.v_name;
            var v_status   = humanCountUserInfoData.v_status;
            for(var i=0; i< v_name.length; i++)
            {
                if(v_name[i] == videoName)
                {
                    v_status[i]=status;
                    console.log("updateDB: "+videoName);
                }
                console.log("VideoName not found!!: "+videoName);
            }

            humanCountUserInfoData.v_status = v_status;
            humanCountUserInfoData.saveUserVideoList();
        }
    }

    function loadVotingPlcaeInfo() {
        var provincename;
        var commitee;
        var placename;
        var inout;
        if(main_window.userVtype == "사전1")
        {
            provincename = humanCountUserInfoData.provincecode1;
            commitee     = humanCountUserInfoData.commitee1;
            placename    = humanCountUserInfoData.placename1;
            inout        = humanCountUserInfoData.inout1;
        }
        else if(main_window.userVtype =="사전2")
        {
            provincename = humanCountUserInfoData.provincecode2;
            commitee     = humanCountUserInfoData.commitee2;
            placename    = humanCountUserInfoData.placename2;
            inout        = humanCountUserInfoData.inout2;
        }
        else if(main_window.userVtype =="선거일")
        {
            provincename = humanCountUserInfoData.provincecode3;
            commitee     = humanCountUserInfoData.commitee3;
            placename    = humanCountUserInfoData.placename3;
            inout        = humanCountUserInfoData.inout3;
        }
        else
        {
            return;
        }

        posText.text = qsTr("촬영장소: ") + provincename + " " 
                       + commitee + " " + placename
                       
        if (inout == "1")
            posText.text = posText.text + " (관내)"
        else if(inout == "2")
            posText.text = posText.text + " (관외)"
        else if(inout == "3")
            posText.text = posText.text + " (관내외)"

    }

    function http_request_v(url,callback) {
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                if(myxhr.readyState === 4) { callback(myxhr); }
            }
        })(xhr);

        xhr.open("POST", url,true);
        //xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("");
    }

    function sendCurrentVotingInfoToServer()
    {
        var videoFileName;
        if(isVideoRecording)
        {
            videoFileName = recordFileName;
        }    
        else
        {
            videoFileName = "-NoRec";

            var currentTime = new Date();
            var timeString = currentTime.getFullYear().toString() 
                            + pad2(currentTime.getMonth() + 1) 
                            + pad2(currentTime.getDate()) 
                            + pad2(currentTime.getHours()) 
                            + pad2(currentTime.getMinutes());
            videoFileName = timeString+videoFileName;
        }
            
        
        var gpsx = (humanAuditorData.gpsx).toFixed(10);
        var gpsy = (humanAuditorData.gpsy).toFixed(10);
        var array_value = [humanAuditorData.electionCode,
                           humanAuditorData.placeCode,
                           main_window.userId,
                           videoFileName,
                           gpsx,
                           gpsy,
                           autoCount_day,
                           manualCount_day,
                           (autoCount_day+manualCount_day),
                           humanCountFilter.lineX1,
                           humanCountFilter.lineY1,
                           humanCountFilter.lineX2,
                           humanCountFilter.lineY2];
        
        var array_value2 = [humanAuditorData.electionCode,
                                   humanAuditorData.placeCode,
                                   main_window.userId,
                                   "TodayLast",
                                   gpsx,
                                   gpsy,
                                   autoCount_day,
                                   manualCount_day,
                                   (autoCount_day+manualCount_day),
                                   humanCountFilter.lineX1,
                                   humanCountFilter.lineY1,
                                   humanCountFilter.lineX2,
                                   humanCountFilter.lineY2];
        /*var url = "/vote/api/auditordata.php?electioncode="+humanAuditorData.electionCode+
                        "&placecode="+humanAuditorData.placeCode+
                        "&auditorid="+main_window.userId+"&url="+videoFileName+
                        "&gpsx="+gpsx+"&gpsy="+gpsy+"&appkey=1111&devicecd=1111"+"&aicount="+parseInt(autoCount_file)+"&mancount="+parseInt(manualCount_file)+
                        "&totalcount="+(parseInt(autoCount_file)+parseInt(manualCount_file)) +
                        "&x1=" + humanCountFilter.lineX1 +
                        "&y1=" + humanCountFilter.lineY1 +
                        "&x2=" + humanCountFilter.lineX2 +
                        "&y2=" + humanCountFilter.lineY2;*/
        humanCountUserInfoData.sendAuditorData(array_value);
        humanCountUserInfoData.sendAuditorData(array_value2);
        /*console.log(url);
        http_request_v(url,function (o) {
            if (o.status === 200)
            {
                console.log(o.responseText);
            }
            else
            {
                console.log(o.status+": sendCurrentVotingInfoToServer errors!!");
            }
        });*/

    }

    function addManualCount(){
        //manualCount_file++;
        manualCount_day++;
        if(main_window.userVtype == "사전1")
        {
            humanCountUserInfoData.manualCountDay1 = manualCount_day;
        }
        else if(main_window.userVtype =="사전2")
        {
            humanCountUserInfoData.manualCountDay2 = manualCount_day;
        }
        else if(main_window.userVtype =="선거일")
        {
            humanCountUserInfoData.manualCountDay3 = manualCount_day;
        }

        humanCountUserInfoData.saveCountInfoDay();
    }

    function addAutoCount(n){
        //autoCount_file+=n;
        autoCount_day+=n;
        if(main_window.userVtype == "사전1")
        {
            humanCountUserInfoData.autoCountDay1 = autoCount_day;
        }
        else if(main_window.userVtype =="사전2")
        {
            humanCountUserInfoData.autoCountDay2 = autoCount_day;
        }
        else if(main_window.userVtype =="선거일")
        {
            humanCountUserInfoData.autoCountDay3 = autoCount_day;
        }
        humanCountUserInfoData.saveCountInfoDay();
    }

    function minusManualCount(){
        //manualCount_file--;
        manualCount_day--;
        if(main_window.userVtype == "사전1")
        {
            humanCountUserInfoData.manualCountDay1 = manualCount_day;
        }
        else if(main_window.userVtype =="사전2")
        {
            humanCountUserInfoData.manualCountDay2 = manualCount_day;
        }
        else if(main_window.userVtype =="선거일")
        {
            humanCountUserInfoData.manualCountDay3 = manualCount_day;
        }
            
        humanCountUserInfoData.saveCountInfoDay();
    }

    function reconfigFromSettings(){
        humanCountUserInfoData.loadAiSettings();
        var ai_engine             = humanCountUserInfoData.ai_engine;
        var ai_rec_fps            = humanCountUserInfoData.ai_rec_fps;
        var ai_rec_resolutions    = humanCountUserInfoData.ai_rec_resolutions;
        var ai_data_transfer_time = humanCountUserInfoData.ai_data_transfer_time;
        var ai_human_masking      = humanCountUserInfoData.ai_human_masking;
        var ai_rec_save_pos       = humanCountUserInfoData.ai_rec_save_pos;

        ai_engine             = parseInt(ai_engine);
        ai_rec_fps            = parseInt(ai_rec_fps);
        ai_rec_resolutions    = parseInt(ai_rec_resolutions);
        ai_data_transfer_time = parseInt(ai_data_transfer_time);
        ai_human_masking      = parseInt(ai_human_masking);
        ai_rec_save_pos       = parseInt(ai_rec_save_pos);

        // ai engine
        if(ai_engine == 0)
        {
            // normal
            humanCountFilter.setHumanDetModel(0);
        }
        else if(ai_engine == 1)
        {
            // lite
            humanCountFilter.setHumanDetModel(1);
        }

        // fps settings
        if(ai_rec_fps == 0)
        {
            camera.videoRecorder.frameRate = 30;
        }
        else if(ai_rec_fps == 1)
        {
            camera.videoRecorder.frameRate = 21;
        }
        else if(ai_rec_fps == 2)
        {
            camera.videoRecorder.frameRate = 10;
        }

        // resolution settings
        if(ai_rec_resolutions == 0)
        {
            camera.videoRecorder.resolution="640x480";
        }
        else if(ai_rec_resolutions == 1)
        {
            camera.videoRecorder.resolution="320x240";
        }

        // resolution settings
        if(ai_human_masking == 0)
        {
            humanCountFilter.setMosaicOnOff(true);
        }
        else if(ai_human_masking == 1)
        {
            humanCountFilter.setMosaicOnOff(false);
        }

        // data transfer time
        if(ai_data_transfer_time == 0)
        {
            // 1분 단위
            sendDataTimer.stop();
            sendDataTimer.interval = 60000;
            sendDataTimer.start();
        }
        else if(ai_data_transfer_time == 1)
        {
            // 5분 단위
            sendDataTimer.stop();
            sendDataTimer.interval = 300000;
            sendDataTimer.start();
        }
        else if(ai_data_transfer_time == 2)
        {
            // 10분 단위
            sendDataTimer.stop();
            sendDataTimer.interval = 600000;
            sendDataTimer.start();
        }

        // 비디오 저장소
        if(ai_rec_save_pos == 0)
        {
            // 내부 저장소
            recordLocation = 0;
        }
        else if(ai_rec_save_pos == 1)
        {
            // 외부 저장소
            recordLocation = 1;

        }
    }

    Component.onCompleted: {
        loadVotingPlcaeInfo()
        humanCountUserInfoData.loadCountInfoDay();
        var day_count_manual = 0;
        var day_count_auto   = 0;

        if(main_window.userVtype == "사전1")
        {
            voteTypeText.text = "사전1";
            day_count_manual =humanCountUserInfoData.manualCountDay1;
            day_count_auto   =humanCountUserInfoData.autoCountDay1;
        }
        else if(main_window.userVtype =="사전2")
        {
            voteTypeText.text = "사전2";
            day_count_manual =humanCountUserInfoData.manualCountDay2;
            day_count_auto   =humanCountUserInfoData.autoCountDay2;
        }
        else if(main_window.userVtype =="선거일")
        {
            voteTypeText.text = "선거일";
            day_count_manual =humanCountUserInfoData.manualCountDay3;
            day_count_auto   =humanCountUserInfoData.autoCountDay3;
        }
        
        manualCount_day = day_count_manual;
        autoCount_day   = day_count_auto;

        reconfigFromSettings();

        if(RunPlatform == "ANDROID")
        {
            var path_list = JniMessenger.getWritablePaths();
            for(var i=0; i<path_list.length; i++)
            {
                console.log(path_list[i]);
            }

            if( path_list.length > 0)
                externalDir = path_list;
        }
    }

        //videoRecorder.audioBitRate: 64000
        //videoRecorder.videoBitRate: 2000000
        //videoRecorder.mediaContainer: "mp4"
        //videoRecorder.frameRate: 30
        //videoRecorder.resolution: "640x480"
        //videoRecorder.videoCodec: "avc1"
        //videoRecorder.audioCodec: "audio/x-ac3"
}
