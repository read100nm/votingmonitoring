﻿#ifdef ANDROID_
/****************************************************************************
**
** Copyright (C) 2020 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtAndroidExtras module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtandroidservice.h"

#include <QAndroidJniEnvironment>
#include <QAndroidIntent>
#include <QtDebug>
#include <QSettings>
#include <QStandardPaths>

QtAndroidService *QtAndroidService::m_instance = nullptr;

static void receivedTokenFromFCM(JNIEnv *env, jobject /*thiz*/, jstring value)
{
    emit QtAndroidService::instance()->tokenFromFCM(env->GetStringUTFChars(value, nullptr));
}

static void receivedNotificationFromFCM(JNIEnv *env, jobject /*thiz*/, jstring value1, jstring value2)
{
    emit QtAndroidService::instance()->notificationFromFCM(env->GetStringUTFChars(value1, nullptr),
                                                           env->GetStringUTFChars(value2, nullptr));

    qDebug()<<"Start Save Notification "<<env->GetStringUTFChars(value1, nullptr)<<"/"<<env->GetStringUTFChars(value2, nullptr);
    QString path = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    QString filename = ".votingmonitoring.config";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USERINFO");
    settings.setValue("FCMTitle",env->GetStringUTFChars(value1, nullptr));
    settings.setValue("FCMBody",env->GetStringUTFChars(value2, nullptr));
    settings.endGroup();
    qDebug()<<"End Save Notification ";
}

static void receivedNotificationFromFCMTest(JNIEnv *env, jobject /*thiz*/, jstring value1, jstring value2)
{
    qDebug()<<env->GetStringUTFChars(value1, nullptr);
    qDebug()<<env->GetStringUTFChars(value2, nullptr);
}

QtAndroidService::QtAndroidService(QObject *parent) : QObject(parent)
{
    m_instance = this;
    {
        JNINativeMethod methods[] {{"sendTokenToQt", "(Ljava/lang/String;)V", reinterpret_cast<void *>(receivedTokenFromFCM)}};
        QAndroidJniObject javaClass("org/peoplecount/votingmonitoring/QtAndroidService");

        QAndroidJniEnvironment env;
        jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
        env->RegisterNatives(objectClass,
                             methods,
                             sizeof(methods) / sizeof(methods[0]));
        env->DeleteLocalRef(objectClass);
    }

    {
        JNINativeMethod methods[] {{"sendNotificationToQt", "(Ljava/lang/String;Ljava/lang/String;)V",
                reinterpret_cast<void *>(receivedNotificationFromFCM)}};
        QAndroidJniObject javaClass("org/peoplecount/votingmonitoring/QtAndroidService");

        QAndroidJniEnvironment env;
        jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
        env->RegisterNatives(objectClass,
                             methods,
                             sizeof(methods) / sizeof(methods[0]));
        env->DeleteLocalRef(objectClass);
    }

    {
        JNINativeMethod methods[] {{"sendNotificationToQtTest", "(Ljava/lang/String;Ljava/lang/String;)V",
                reinterpret_cast<void *>(receivedNotificationFromFCMTest)}};
        QAndroidJniObject javaClass("org/peoplecount/votingmonitoring/QtAndroidService");

        QAndroidJniEnvironment env;
        jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
        env->RegisterNatives(objectClass,
                             methods,
                             sizeof(methods) / sizeof(methods[0]));
        env->DeleteLocalRef(objectClass);
    }
}

void QtAndroidService::requestTokenFromFCM()
{
    QAndroidJniObject::callStaticMethod<void>("org/peoplecount/votingmonitoring/QtAndroidService",
                                           "getTokenFromJava");
    qDebug()<<"called getTokenFromJava";
}

#endif
