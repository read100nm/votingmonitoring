#ifndef __HUMAN_COUNT_VIDEO_FILTER__
#define __HUMAN_COUNT_VIDEO_FILTER__

#include <QAbstractVideoFilter>
#include <QVideoFilterRunnable>

#include <opencv2/core.hpp>

#ifdef ANDROID_
    #include "HumanCountDetector.h"
#else
    #include "HumanCountDetectorCV.h"
#endif

#include "CentroidTracker.h"

class HumanCountVideoFilterRunnable;

class HumanCountVideoFilter : public QAbstractVideoFilter
{
    Q_OBJECT

    Q_PROPERTY( int videoOutputOrientation MEMBER m_VideoOutputOrientation )
    Q_PROPERTY( int videoOutputWidth MEMBER mVideoOutputWidth )
    Q_PROPERTY( int videoOutputHeight MEMBER mVideoOutputHeight )
    Q_PROPERTY( float displayZoomValue MEMBER mDisplayZoomValue)

    Q_PROPERTY( int lineX1 MEMBER lineX1 )
    Q_PROPERTY( int lineX2 MEMBER lineX2 )
    Q_PROPERTY( int lineY1 MEMBER lineY1 )
    Q_PROPERTY( int lineY2 MEMBER lineY2 )
    Q_PROPERTY( int aiRotate MEMBER aiRotate )

public:

    Q_INVOKABLE void changeLineLeft();
    Q_INVOKABLE void changeLineRight();
    Q_INVOKABLE void changeLineRotateLeft();
    Q_INVOKABLE void changeLineRotateRight();
    Q_INVOKABLE void changeLineRotate90Right();
    Q_INVOKABLE void setAutoCountMode(bool on_off);
    Q_INVOKABLE void setLineSetMode(bool on_off);

    Q_INVOKABLE void setHumanDetModel(int model_id);
    Q_INVOKABLE void setMosaicOnOff(bool mosaic_on_off);

    void invokeSigAutoCount(int n);

    HumanCountVideoFilter( QObject* parent = nullptr );
    QVideoFilterRunnable* createFilterRunnable() Q_DECL_OVERRIDE;

    int m_VideoOutputOrientation;
    float mDisplayZoomValue;
    int mVideoOutputWidth;
    int mVideoOutputHeight;

    int lineX1;
    int lineX2;
    int lineY1;
    int lineY2;

    int aiRotate;

    int mCurrentHumanDetMode;
    bool mSetMosaic;
signals:
    void sigAutoCount(int n);
protected:
    HumanCountVideoFilterRunnable* mRunnable;

};


#endif
