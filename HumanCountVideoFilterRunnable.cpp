#include "HumanCountVideoFilterRunnable.h"
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QScreen>
#include <QGuiApplication>

#include "opencv2/video/tracking.hpp"

//#include "opencv2/tracking.hpp"
void setMosaic(cv::Mat& inputMat, cv::Mat& mosaicMat,cv::Rect rect) 
{
    //if((mMosaicImageInit == false) || (inputMat.rows != mMosaicImage.rows) || (inputMat.cols != mMosaicImage.cols)) 
    //{
    //    createMosaicImage(inputMat, mMosaicImage,10);
    //    mMosaicImageInit = true;
    //}
    int x1, x2;
    int y1, y2;
    x1 = rect.x;
    x2 = rect.x + rect.width;
    y1 = rect.y;
    y2 = rect.y + rect.height;
    
    // not adjustable
    if(x1 >= inputMat.cols) return;
    if(y1 >= inputMat.rows) return;
    if(x2 < 0) return;
    if(y2 < 0) return;
    
    // adjust
    if(x1 < 0) x1 = 0;
    if(x2 >= inputMat.cols) x2 = inputMat.cols-1;
    if(y1 < 0) y1 = 0;
    if(y2 >= inputMat.rows) y2 = inputMat.rows-1;
    
    rect.x = x1;
    rect.width = x2 - x1;
    rect.y = y1;
    rect.height =y2 - y1;
        
    cv::Mat roi = inputMat(rect);
    cv::Mat tempRoi = mosaicMat(rect);
    tempRoi.copyTo(roi);
}




HumanCountVideoFilterRunnable::HumanCountVideoFilterRunnable(HumanCountVideoFilter *filter) : m_Filter(filter)
{
    //detector = nullptr;

    mCountNormalLineSize = 100;
    mInsidePolygonID = 0;
    mCountLineInit = false;
    mCentroidTracker = nullptr;
    //mGorunTracker = cv::TrackerDaSiamRPN
    //mCSRTTracker = cv::TrackerCSRT::create();
    

    mAutoCountEnabled   = false;
    mLineSetModeEnabled = false;

    mCurrentImgWidth = -1;
    mCurrentImgHeight = -1;
    mLineSetImgWidth = -1;
    mLineSetImgHeight = -1;

    mTrackingInited = false;

    mLoadedHumanDetMode = -1;
}


HumanCountVideoFilterRunnable::~HumanCountVideoFilterRunnable()
{
    if (mCentroidTracker != nullptr)
    {
        delete mCentroidTracker;
    }
}

void HumanCountVideoFilterRunnable::loadHumanDetModel()
{
    if(mLoadedHumanDetMode == m_Filter->mCurrentHumanDetMode)
        return;

    QString model_file;
    if(m_Filter->mCurrentHumanDetMode == 0)
    {
        model_file = "human_det_s.onnx";
    }
    else if(m_Filter->mCurrentHumanDetMode == 1)
    {
        model_file = "human_det_n.onnx";
    }

#ifdef ANDROID_

    try
    {
        QString app_path = QString(QDir::currentPath());
        qDebug() << app_path;

        QFile assest_onnx("assets:/weights/"+model_file);
        QFile local_onnx(app_path + "/"+model_file);
        if (local_onnx.exists() == false && assest_onnx.exists())
        {
            assest_onnx.copy(app_path + "/"+model_file);
        }
        QString onnx_path_qstr = app_path + "/"+model_file;
        detector = HumanCountDetector(onnx_path_qstr.toStdString(), false, cv::Size(640, 640));

        //QFile assest_onnx_deepsort("assets:/weights/deepsort.onnx");
        //QFile local_onnx_deepsort(app_path + "/deepsort.onnx");
        //if (local_onnx_deepsort.exists() == false && assest_onnx_deepsort.exists())
        //{
        //    assest_onnx_deepsort.copy(app_path + "/deepsort.onnx");
        //}
        //QString deepsort_onnx_path_qstr = app_path + "/deepsort.onnx";
        //DS = new DeepSort(deepsort_onnx_path_qstr.toStdString(), 128, 256, 0);
    }
    catch (Ort::Exception &e)
    {
        qDebug() << e.what() << e.GetOrtErrorCode();
    }

#else
    QString app_path = QString(QDir::currentPath());
    QString yolov5_onnx_path = app_path + "/" + model_file;
    detector = HumanCountDetectorCV(yolov5_onnx_path.toStdString(), false, cv::Size(640, 640));
    //DS = new DeepSort("/home/john/project/VotingMonitoring/source/android_package/assets/weights/deepsort.onnx", 128, 256, 0);
#endif
    mLoadedHumanDetMode = m_Filter->mCurrentHumanDetMode;
    qDebug() << "Model was initailized with "<<model_file;
}

QVideoFrame HumanCountVideoFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    int64 t0,t1,t2,t3,t4;
    t0 = cv::getTickCount();

    // 1. input check
    Q_UNUSED(flags)
    if (!input)
    {
        return QVideoFrame();
    }
    int videoOutputOrientation = m_Filter->m_VideoOutputOrientation;
    //qDebug()<<"surfaceFormat rotation:" << videoOutputOrientation;
    //qDebug()<<"surfaceFormat pixelAspectRatio:" << surfaceFormat.pixelAspectRatio();
    //qDebug()<<"surfaceFormat viewport:" << m_Filter->mVideoOutputWidth<<"x"<<m_Filter->mVideoOutputHeight;
    //qDebug()<<"QScreen Orientation:"<<QGuiApplication::screens()[0]->orientation();
    // 2. QVideoFrame to QImage
    cv::Mat input_mat_;
    bool   isInputBGR = false;
    if(QVideoFrameToQImage(input,input_mat_,isInputBGR)==false)
    {
        return QVideoFrame();
    }
    //return QVideoFrame();

    // 3. QImage to cv::Mat
    //cv::Mat input_mat_(input_qimg.height(), input_qimg.width(), CV_8UC3, (cv::Scalar *)input_qimg.scanLine(0), input_qimg.bytesPerLine());
    //cv::Mat input_mat_2;
    
    if (isInputBGR)
    {
        cv::cvtColor(input_mat_, input_mat_, cv::COLOR_BGR2RGB);
    }
#ifdef ANDROID_
    cv::flip(input_mat_, input_mat_, 0);
#endif
    cv::Mat input_mat;
    if(m_Filter->aiRotate == 0)
        cv::rotate(input_mat_,input_mat,cv::ROTATE_90_CLOCKWISE);
    else if(m_Filter->aiRotate == 90)
        cv::rotate(input_mat_,input_mat,cv::ROTATE_180);
    else if(m_Filter->aiRotate == 180)
        cv::rotate(input_mat_,input_mat,cv::ROTATE_90_COUNTERCLOCKWISE);
    else
        input_mat = input_mat_;


    mCurrentImgWidth  = input_mat.cols;
    mCurrentImgHeight = input_mat.rows;

    // 4. check current mode
    if(!mAutoCountEnabled && !mLineSetModeEnabled)
        return QVideoFrame();

    // load model
    loadHumanDetModel();
    
    // get mosasic set
    bool mosaic_on_off = m_Filter->mSetMosaic;

    t1 = cv::getTickCount();

    //////////////////////////////////////////////////////////////////////////
    // Count Line init
    //////////////////////////////////////////////////////////////////////////
    if (mCountLineInit == false)
    {
        cv::Point pt1,pt2;
        pt1.x = input_mat.cols/2;
        pt1.y = input_mat.rows/2;
        pt2.x = pt1.x;
        pt2.y = pt1.y - 100;

        initCountLine(pt1,pt2,input_mat.cols,input_mat.rows);

    }
    else
    {
        if( (mLineSetImgHeight != mCurrentImgHeight) || (mLineSetImgWidth != mCurrentImgWidth))
        {
            // 1. 기존 라인셋의 중앙 -> p1,p2 간 x,y 거리 구하기 
            // 2. 새로운 이미지와 기존 이미지 간의 x,y 비율 구하기 
            // 3. 새로운 이미지의 중앙과 p1,p2 거리에 비율 곱하여 새로운 p1,p2 계산

            cv::Point2f previousCenter;
            cv::Point2f currentCenter;
            previousCenter.x = mLineSetImgWidth / 2.0f;
            previousCenter.y = mLineSetImgHeight / 2.0f;
            currentCenter.x = mCurrentImgWidth / 2.0f;
            currentCenter.y = mCurrentImgHeight / 2.0f;
            
            float dist_p1_x = mCountLinePt1.x - previousCenter.x;
            float dist_p1_y = mCountLinePt1.y - previousCenter.y;
            float dist_p2_x = mCountLinePt2.x - previousCenter.x;
            float dist_p2_y = mCountLinePt2.y - previousCenter.y;

            float image_ratio_x = (float)mCurrentImgWidth / (float)mLineSetImgWidth;
            float image_ratio_y = (float)mCurrentImgHeight / (float)mLineSetImgHeight;

            cv::Point pt1,pt2;
            pt1.x = int(currentCenter.x + (dist_p1_x * image_ratio_x) + 0.5);
            pt1.y = int(currentCenter.y + (dist_p1_y * image_ratio_y) + 0.5);
            pt2.x = int(currentCenter.x + (dist_p2_x * image_ratio_x) + 0.5);
            pt2.y = int(currentCenter.y + (dist_p2_y * image_ratio_y) + 0.5);

            initCountLine(pt1,pt2,input_mat.cols,input_mat.rows);
        }
    }



    //////////////////////////////////////////////////////////////////////////
    // Human Detect and Count
    //////////////////////////////////////////////////////////////////////////
    if(mAutoCountEnabled && (mLineSetModeEnabled == false))
    {
        // detection
        std::vector<Detection> result;
        //std::vector<DetectBox> human_tracking;
        std::vector<Detection> human_result;
        try
        {
            result = detector.detect(input_mat, 0.3, 0.4);
            qDebug() << "Detection Result:" << result.size();
            for (const Detection &detection : result)
            {
                if(detection.classId == 0)
                {
                    //cv::rectangle(crop_mat, detection.box, cv::Scalar(0, 0, 255), 2);
                    human_result.push_back(detection);

                    if(mTrackingInited)
                    {

                    }
                    //DetectBox dd(detection.box.x,detection.box.y, detection.box.x + detection.box.width
                    //    ,detection.box.y + detection.box.height,detection.conf,detection.classId);

                    //human_tracking.push_back(dd);
                }
            }
            t2 = cv::getTickCount();

            std::unordered_map<int, cv::Point2i> objects;
            mCentroidTracker->updateObjects(human_result,objects);
            int people_count = mCentroidTracker->calculateEnterCount();
            

            cv::Mat mosaicImage;
            
            if(mosaic_on_off)
            {
                mosaicImage = input_mat.clone();
                cv::resize(mosaicImage,mosaicImage,cv::Size(input_mat.cols/20,input_mat.rows/20),0,0,cv::INTER_NEAREST);
                cv::resize(mosaicImage,mosaicImage,cv::Size(input_mat.cols,input_mat.rows),0,0,cv::INTER_NEAREST);
            } 
            
            //createMosaicImage(crop_mat, mMosaicImage,10);
            for(auto box: human_result)
            {
                int current_area = 0; // 라인 바깥쪽
                double check_pt = cv::pointPolygonTest(mCountPolygon[mInsidePolygonID],cv::Point(box.box.x+box.box.width/2,box.box.y+box.box.height/2),false);
                if(check_pt > 0)
                    current_area = 1;  // 라인 안쪽

                if(current_area == 0)
                {
                    cv::Point lt(box.box.x, box.box.y);
                    //cv::Point br(box.x2, box.y2);
                    if(mosaic_on_off) setMosaic(input_mat,mosaicImage, box.box);

                    cv::rectangle(input_mat, box.box, cv::Scalar(255, 0, 0), 3);
                    //std::string lbl = cv::format("ID:%d", (int)box.classId);
                    //cv::putText(input_mat, lbl, lt, cv::FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(0,255,0));
                }
                else
                {
                    cv::Point lt(box.box.x, box.box.y);
                    //cv::Point br(box.x2, box.y2);
                    if(mosaic_on_off) setMosaic(input_mat,mosaicImage, box.box);
                    
                    cv::rectangle(input_mat, box.box, cv::Scalar(0, 255, 0), 3);
                    //std::string lbl = cv::format("ID:%d", (int)box.classId);
                    //cv::putText(input_mat, lbl, lt, cv::FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(0,255,0));
                }
            }

            m_Filter->invokeSigAutoCount(people_count);
            t3 = cv::getTickCount();

            cv::line(input_mat,mCountLineClipPt1,mCountLineClipPt2,cv::Scalar(0,0,255),3);
            cv::line(input_mat,mCountNormalLinePt1,mCountNormalLinePt2,cv::Scalar(0,255,0),3);
            cv::Mat layer = cv::Mat::zeros(input_mat.size(), CV_8UC3);
            cv::rectangle(input_mat,cv::Rect(0,0,input_mat.cols,input_mat.rows),cv::Scalar(255,255,0));
            cv::fillPoly(layer,mCountPolygon[mInsidePolygonID],cv::Scalar(255,0,0));
            cv::addWeighted(input_mat, 0.8, layer, 1-0.8, 0, input_mat);

            double secs1 = (t1 - t0) / cv::getTickFrequency();
            double secs2 = (t2 - t1) / cv::getTickFrequency();
            double secs3 = (t3 - t2) / cv::getTickFrequency();
            qDebug() << "time: " << (secs1 + secs2+secs3) << " secs (" << secs1 << "+" << secs2 << "+"<<secs3<<")";
            cv::Mat output_mat;

            if(m_Filter->aiRotate == 0)
                cv::rotate(input_mat,output_mat,cv::ROTATE_90_COUNTERCLOCKWISE);
            else if(m_Filter->aiRotate == 90)
                cv::rotate(input_mat,output_mat,cv::ROTATE_180);
            else if(m_Filter->aiRotate == 180)
                cv::rotate(input_mat,output_mat,cv::ROTATE_90_CLOCKWISE);
            else if(m_Filter->aiRotate == 270)
                output_mat = input_mat;

            //else if(m_Filter->aiRotate == 180)
            //    cv::rotate(input_mat_,input_mat,cv::ROTATE_90_COUNTERCLOCKWISE);
            //else if(m_Filter->aiRotate == 270)
            //    cv::rotate(input_mat_,input_mat,cv::ROTATE_90_CLOCKWISE);
            //else if(m_Filter->aiRotate == 90)
            //    cv::rotate(input_mat_,input_mat,cv::ROTATE_90_CLOCKWISE);


            
            QImage input_qimg((uchar*) output_mat.data, output_mat.cols, output_mat.rows, output_mat.step, QImage::Format_RGB888);
            return QVideoFrame(input_qimg.convertToFormat(QImage::Format_ARGB32));
        }
        catch (const std::exception &e)
        {
            qDebug() << e.what();
            return QVideoFrame();
        }

    }
    if(mLineSetModeEnabled)
    {
        try
        {
            cv::line(input_mat,mCountLineClipPt1,mCountLineClipPt2,cv::Scalar(0,0,255),3);
            cv::line(input_mat,mCountNormalLinePt1,mCountNormalLinePt2,cv::Scalar(0,255,0),3);
            cv::Mat layer = cv::Mat::zeros(input_mat.size(), CV_8UC3);
            cv::rectangle(input_mat,cv::Rect(0,0,input_mat.cols,input_mat.rows),cv::Scalar(255,255,0));
            cv::fillPoly(layer,mCountPolygon[mInsidePolygonID],cv::Scalar(255,0,0));
            cv::addWeighted(input_mat, 0.8, layer, 1-0.8, 0, input_mat);

            double secs1 = (t1 - t0) / cv::getTickFrequency();
            double secs2 = (t2 - t1) / cv::getTickFrequency();
            double secs3 = (t3 - t2) / cv::getTickFrequency();
            qDebug() << "time: " << (secs1 + secs2+secs3) << " secs (" << secs1 << "+" << secs2 << "+"<<secs3<<")";
            cv::Mat output_mat;
            //cv::rotate(input_mat,output_mat,cv::ROTATE_90_COUNTERCLOCKWISE);
            if(m_Filter->aiRotate == 0)
                cv::rotate(input_mat,output_mat,cv::ROTATE_90_COUNTERCLOCKWISE);
            else if(m_Filter->aiRotate == 90)
                cv::rotate(input_mat,output_mat,cv::ROTATE_180);
            else if(m_Filter->aiRotate == 180)
                cv::rotate(input_mat,output_mat,cv::ROTATE_90_CLOCKWISE);
            else if(m_Filter->aiRotate == 270)
                output_mat = input_mat;

            QImage input_qimg((uchar*) output_mat.data, output_mat.cols, output_mat.rows, output_mat.step, QImage::Format_RGB888);
            return QVideoFrame(input_qimg.convertToFormat(QImage::Format_ARGB32));
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            return QVideoFrame();
        }
        
    }
    return QVideoFrame();
}

void HumanCountVideoFilterRunnable::initCountLine(cv::Point &line_pt1, cv::Point &line_pt2, int img_width, int img_height)
{
    mLineSetMutex.lock();

    mCountLinePt1 = line_pt1;
    mCountLinePt2 = line_pt2;

    // calculate line angle and bias
    float count_line_m;
    float count_line_b;
    float x1, x2, y1, y2;

    if(mCountLinePt2.x == mCountLinePt1.x)
    {
        x1 = mCountLinePt1.x;
        x2 = mCountLinePt1.x;
        y1 = -1;
        y2 = img_height+1;
    }
    else
    {
        count_line_m = (float)(mCountLinePt2.y - mCountLinePt1.y) / (float)(mCountLinePt2.x - mCountLinePt1.x);
        count_line_b = (float)(mCountLinePt1.y) - (float)count_line_m * (float)mCountLinePt1.x;

        // cliping line in the overall image
        y1 = count_line_b;
        y2 = (img_width - 1) * count_line_m + count_line_b;
        x1 = -count_line_b / count_line_m;
        x2 = ((img_height - 1) - count_line_b) / count_line_m;
    }

    if ((y1 >= 0.f) && (y1 <= (float)(img_height - 1)) && (y2 >= 0.f) && (y2 <= (float)(img_height - 1)))
    {
        mCountLineClipPt1.x = 0;
        mCountLineClipPt1.y = int(y1 + 0.5f);

        mCountLineClipPt2.x = img_width - 1;
        mCountLineClipPt2.y = int(y2 + 0.5);

        // make inside and outside poligons
        mCountPolygon[0].clear();
        mCountPolygon[0].push_back(cv::Point(0, 0));
        mCountPolygon[0].push_back(cv::Point(mCountLineClipPt2.x, 0));
        mCountPolygon[0].push_back(cv::Point(mCountLineClipPt2.x, mCountLineClipPt2.y));
        mCountPolygon[0].push_back(cv::Point(0, mCountLineClipPt1.y));

        mCountPolygon[1].clear();
        mCountPolygon[1].push_back(cv::Point(0, mCountLineClipPt1.y));
        mCountPolygon[1].push_back(cv::Point(mCountLineClipPt2.x, mCountLineClipPt2.y));
        mCountPolygon[1].push_back(cv::Point(img_width - 1, img_height - 1));
        mCountPolygon[1].push_back(cv::Point(0, img_height - 1));
    }
    else
    {
        mCountLineClipPt1.x = int(x1 + 0.5f);
        mCountLineClipPt1.y = 0;

        mCountLineClipPt2.x = int(x2 + 0.5f);
        mCountLineClipPt2.y = img_height - 1;

        // make inside and outside poligons
        mCountPolygon[0].clear();
        mCountPolygon[0].push_back(cv::Point(0, 0));
        mCountPolygon[0].push_back(cv::Point(mCountLineClipPt1.x, 0));
        mCountPolygon[0].push_back(cv::Point(mCountLineClipPt2.x, mCountLineClipPt2.y));
        mCountPolygon[0].push_back(cv::Point(0, mCountLineClipPt2.y));

        mCountPolygon[1].clear();
        mCountPolygon[1].push_back(cv::Point(mCountLineClipPt1.x, 0));
        mCountPolygon[1].push_back(cv::Point(img_width - 1, 0));
        mCountPolygon[1].push_back(cv::Point(img_width - 1, img_height - 1));
        mCountPolygon[1].push_back(cv::Point(mCountLineClipPt2.x, mCountLineClipPt2.y));
    }

    // calculate normal vector
    float vk1 = (float)(mCountLinePt2.x-mCountLinePt1.x);
    float vk2 = (float)(mCountLinePt2.y-mCountLinePt1.y);

    float center_x = float(mCountLineClipPt1.x + mCountLineClipPt2.x) /2.0f;
    float center_y = float(mCountLineClipPt1.y + mCountLineClipPt2.y) /2.0f;

    float vk_mag = std::sqrt(vk1*vk1 +vk2*vk2);
    vk1 = vk1 * mCountNormalLineSize /vk_mag;
    vk2 = vk2 * mCountNormalLineSize /vk_mag;

    float vn1 = -vk2;
    float vn2 =  vk1;

    mCountNormalLinePt1.x = int(center_x+0.5);
    mCountNormalLinePt1.y = int(center_y+0.5);

    mCountNormalLinePt2.x = mCountNormalLinePt1.x + vn1;
    mCountNormalLinePt2.y = mCountNormalLinePt1.y + vn2;

    double check1 = cv::pointPolygonTest(mCountPolygon[0],mCountNormalLinePt2,false);
    if(check1 > 0)
        mInsidePolygonID = 0;
    else
        mInsidePolygonID = 1;

    if(mCentroidTracker != nullptr)
       delete mCentroidTracker;

//#if ANDROID_
//    QString app_path = QString(QDir::currentPath());
//    app_path = app_path+"/deepsort.onnx";
//    DS = new DeepSort(app_path.toStdString(),128,256,0);
//#else
    mCentroidTracker = new
            CentroidTracker(mCountPolygon[mInsidePolygonID]);
                //m("/home/john/project/VotingMonitoring/source/android_package/assets/weights/deepsort.onnx", 128, 256, 0);
//#endif
    mCountLineInit    = true;
    mLineSetImgWidth  = img_width;
    mLineSetImgHeight = img_height;
    mLineSetMutex.unlock();

    m_Filter->lineX1 = mCountLineClipPt1.x;
    m_Filter->lineY1 = mCountLineClipPt1.y;

    m_Filter->lineX2 = mCountLineClipPt2.x;
    m_Filter->lineY2 = mCountLineClipPt2.y;
}

void HumanCountVideoFilterRunnable::changeLineLeft()
{
    if( (mCurrentImgWidth == -1) || (mCurrentImgHeight == -1))
        return;

    // calculate normal vector
    float vk1 = (float)(mCountLinePt2.x-mCountLinePt1.x);
    float vk2 = (float)(mCountLinePt2.y-mCountLinePt1.y);

    float center_x = float(mCountLineClipPt1.x + mCountLineClipPt2.x) /2.0f;
    float center_y = float(mCountLineClipPt1.y + mCountLineClipPt2.y) /2.0f;

    float vk_mag = std::sqrt(vk1*vk1 +vk2*vk2);
    vk1 = vk1 /vk_mag;
    vk2 = vk2 /vk_mag;

    float vn1 = -vk2;
    float vn2 =  vk1;

    cv::Point pt1,pt2;
    pt1 = mCountLinePt1;
    pt2 = mCountLinePt2;

    pt1.x = pt1.x - vn1*6;
    pt1.y = pt1.y - vn2*6;

    pt2.x = pt2.x - vn1*6;
    pt2.y = pt2.y - vn2*6;

    initCountLine(pt1,pt2,mCurrentImgWidth,mCurrentImgHeight);
}

void HumanCountVideoFilterRunnable::changeLineRight()
{
    if( (mCurrentImgWidth == -1) || (mCurrentImgHeight == -1))
        return;

    // calculate normal vector
    float vk1 = (float)(mCountLinePt2.x-mCountLinePt1.x);
    float vk2 = (float)(mCountLinePt2.y-mCountLinePt1.y);

    float center_x = float(mCountLineClipPt1.x + mCountLineClipPt2.x) /2.0f;
    float center_y = float(mCountLineClipPt1.y + mCountLineClipPt2.y) /2.0f;

    float vk_mag = std::sqrt(vk1*vk1 +vk2*vk2);
    vk1 = vk1 /vk_mag;
    vk2 = vk2 /vk_mag;

    float vn1 = -vk2;
    float vn2 =  vk1;

    cv::Point pt1,pt2;
    pt1 = mCountLinePt1;
    pt2 = mCountLinePt2;

    pt1.x = pt1.x + vn1*6;
    pt1.y = pt1.y + vn2*6;

    pt2.x = pt2.x + vn1*6;
    pt2.y = pt2.y + vn2*6;

    initCountLine(pt1,pt2,mCurrentImgWidth,mCurrentImgHeight);
}

void HumanCountVideoFilterRunnable::changeLineRotateLeft()
{
    if( (mCurrentImgWidth == -1) || (mCurrentImgHeight == -1))
        return;

    cv::Point pt1,pt2;
    pt1 = mCountLinePt1;
    pt2 = mCountLinePt2;

    float v1 = pt2.x - pt1.x;
    float v2 = pt2.y - pt1.y;

    float angle=0.1;
    float vr1 = v1 * cos(angle) - v2*sin(angle);
    float vr2 = v1 * sin(angle) + v2*cos(angle);

    pt2.x = pt1.x + int(vr1+0.5);
    pt2.y = pt1.y + int(vr2+0.5);

    initCountLine(pt1,pt2,mCurrentImgWidth,mCurrentImgHeight);
}

void HumanCountVideoFilterRunnable::changeLineRotateRight()
{
    if( (mCurrentImgWidth == -1) || (mCurrentImgHeight == -1))
        return;

    cv::Point pt1,pt2;
    pt1 = mCountLinePt1;
    pt2 = mCountLinePt2;

    float v1 = pt2.x - pt1.x;
    float v2 = pt2.y - pt1.y;

    float angle=-0.1;
    float vr1 = v1 * cos(angle) - v2*sin(angle);
    float vr2 = v1 * sin(angle) + v2*cos(angle);

    pt2.x = pt1.x + int(vr1+0.5);
    pt2.y = pt1.y + int(vr2+0.5);

    initCountLine(pt1,pt2,mCurrentImgWidth,mCurrentImgHeight);
}

void HumanCountVideoFilterRunnable::changeLineRotate90Right()
{
    if( (mCurrentImgWidth == -1) || (mCurrentImgHeight == -1))
        return;

    cv::Point pt1,pt2;
    pt1 = mCountLinePt1;
    pt2 = mCountLinePt2;

    float v1 = pt2.x - pt1.x;
    float v2 = pt2.y - pt1.y;

    float angle=1.5708;
    float vr1 = v1 * cos(angle) - v2*sin(angle);
    float vr2 = v1 * sin(angle) + v2*cos(angle);

    pt2.x = pt1.x + int(vr1+0.5);
    pt2.y = pt1.y + int(vr2+0.5);
    initCountLine(pt1,pt2,mCurrentImgWidth,mCurrentImgHeight);
}

void HumanCountVideoFilterRunnable::setAutoCountMode(bool on_off)
{
    mAutoCountEnabled = on_off;
}

void HumanCountVideoFilterRunnable::setLineSetMode(bool on_off)
{

    mLineSetModeEnabled = on_off;
}

bool HumanCountVideoFilterRunnable::QVideoFrameToQImage(QVideoFrame* input,cv::Mat& output,bool &isBGR)
{
    QImage image;
    isBGR = false;
    if(input->map(QAbstractVideoBuffer::ReadOnly) == true)
    {
        if (input->pixelFormat() == QVideoFrame::Format_ABGR32)
        {
            //image = QImage(input->width(),input->height(),QImage::Format_RGB888);
            output = cv::Mat::zeros(input->height(),input->width(),CV_8UC3);

            uchar* frame_buffer = input->bits();
            uchar* image_buffer = output.data;
            
            for(int i=0; i< input->height(); i++)
            {
                uchar* line_v_buffer = frame_buffer + (input->bytesPerLine() * i);
                uchar* line_i_buffer = image_buffer + (output.step * i);

                for(int j=0; j<input->width(); j++)
                {
                    uchar r = *line_v_buffer;
                    line_v_buffer++;
                    
                    uchar g = *line_v_buffer;
                    line_v_buffer++;
                    
                    uchar b = *line_v_buffer;
                    line_v_buffer++;
                    
                    uchar a = *line_v_buffer;
                    line_v_buffer++;
                        
                    *line_i_buffer=r;
                    line_i_buffer++;
                    *line_i_buffer=g;
                    line_i_buffer++;
                    *line_i_buffer=b;
                    line_i_buffer++;
                }
            }

            return true;
        }
        else
        {
            image = input->image();
        }
        input->unmap();
    }

    if (image.isNull())
    {
        return false;
    }

    if (image.format() != QImage::Format_RGB888)
    {
        image.convertTo(QImage::Format_BGR888);
    }

    output = cv::Mat::zeros(image.height(),image.width(),CV_8UC3);
    uchar* cv_buffer = output.data;
    uchar* image_buffer = image.bits();

    for(int i=0; i< image.height(); i++)
    {
        uchar* line_v_buffer = cv_buffer + (output.step * i);
        uchar* line_i_buffer = image_buffer + (image.bytesPerLine() * i);
        for(int j=0; j< image.width(); j++)
        {
            *line_v_buffer = *line_i_buffer;
            line_v_buffer++;
            line_i_buffer++;

            *line_v_buffer = *line_i_buffer;
            line_v_buffer++;
            line_i_buffer++;

            *line_v_buffer = *line_i_buffer;
            line_v_buffer++;
            line_i_buffer++;
        }
    }

    return true;
}
