import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
//import Qt.labs.platform
import QtMultimedia 5.12
import QtWebView 1.15
import HumanCountLib 1.0

Item {
    id: messageYesNoPopup
    x:0
    y:0
    width: parent.width
    height: parent.height
    property bool isYes:false
    property var yesFunction: null
    property var noFunction: null
    // Popup object
    Popup {
        id:messagePopup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        modal: true
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closePopup();
            }
        }

        Rectangle {
            id: add_cargo_info_ui
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*1/2
            radius: 5
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            Text
            {
                id:messageText
                x:add_cargo_info_ui.width * 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                y:add_cargo_info_ui.height* 0.5        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                width:add_cargo_info_ui.width*0.8
                height:add_cargo_info_ui.height*0.3
                color:"black"
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                wrapMode: Text.WordWrap
            }

            Rectangle{
                id: yes_btn
                x: add_cargo_info_ui.width * 0.05
                y: add_cargo_info_ui.height * 0.86
                width: add_cargo_info_ui.width * 0.4
                height: add_cargo_info_ui.height * 0.1
                color: yes_btn_ma.pressed? "#FFFFFF":"#CFE9F0"
                radius: height*0.458333333
                border.color: "#CFE9F0"
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter 
                    width: parent.width* 0.487179487
                    height: parent.height * 0.4375
                    text: qsTr("예")
                    font.family: "Roboto"
                    font.pixelSize: height* 0.761904762
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: yes_btn_ma
                    anchors.fill: parent
                    onClicked: {
                        isYes=true;
                        closePopup();
                    }
                }
            }

            Rectangle{
                id: no_btn
                x: add_cargo_info_ui.width * 0.5
                y: add_cargo_info_ui.height * 0.86
                width: add_cargo_info_ui.width * 0.4
                height: add_cargo_info_ui.height * 0.1
                color: no_btn_ma.pressed? "#FFFFFF":"#CFE9F0"
                radius: height*0.458333333
                border.color: "#CFE9F0"
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter 
                    width: parent.width* 0.487179487
                    height: parent.height * 0.4375
                    text: qsTr("아니오")
                    font.family: "Roboto"
                    font.pixelSize: height* 0.761904762
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: no_btn_ma
                    anchors.fill: parent
                    onClicked: {
                        isYes=false;
                        closePopup();
                    }
                }
            }
        }
    }

    function openPopup(msg,yes_callback,no_callback){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수
        messageText.text = msg;
        yesFunction = yes_callback
        noFunction = no_callback
        messagePopup.open();

        
        console.log("open Popup finished!!")
        return isYes;
    }

    function closePopup()
    {
        console.log("close yesno popup")
        if(isYes && (yesFunction != null))
        {
            yesFunction();
        }
        if((isYes==false) && (noFunction != null))
        {
            noFunction();
        }
        messagePopup.close();
    }
}
