#ifndef HUMAN_COUNT_USER_INFO_DATA_H_
#define HUMAN_COUNT_USER_INFO_DATA_H_

#include <QObject>
#include <qvariant.h>
#include <qmap.h>
#include "HumanCountFtpUploader.h"

class HumanCountUserInfoData : public QObject
{
    Q_OBJECT

    Q_PROPERTY( QString electioncode1 MEMBER electioncode1 )
    Q_PROPERTY( QString provincecode1 MEMBER provincecode1 )
    Q_PROPERTY( QString provincename1 MEMBER provincename1)
    Q_PROPERTY( QString commitee1 MEMBER commitee1)
    Q_PROPERTY( QString placename1 MEMBER placename1 )
    Q_PROPERTY( QString vtype1 MEMBER vtype1)
    Q_PROPERTY( QString username1 MEMBER username1)
    Q_PROPERTY( QString userphone1 MEMBER userphone1)
    Q_PROPERTY( QString inout1 MEMBER inout1 )
    Q_PROPERTY( int     ampm1 MEMBER ampm1)
    Q_PROPERTY( QString userid1 MEMBER userid1)

    Q_PROPERTY( QString electioncode2 MEMBER electioncode2 )
    Q_PROPERTY( QString provincecode2 MEMBER provincecode2 )
    Q_PROPERTY( QString provincename2 MEMBER provincename2)
    Q_PROPERTY( QString commitee2 MEMBER commitee2)
    Q_PROPERTY( QString placename2 MEMBER placename2 )
    Q_PROPERTY( QString vtype2 MEMBER vtype2)
    Q_PROPERTY( QString username2 MEMBER username2)
    Q_PROPERTY( QString userphone2 MEMBER userphone2)
    Q_PROPERTY( QString inout2 MEMBER inout2 )
    Q_PROPERTY( int     ampm2 MEMBER ampm2)
    Q_PROPERTY( QString userid2 MEMBER userid2)

    Q_PROPERTY( QString electioncode3 MEMBER electioncode3 )
    Q_PROPERTY( QString provincecode3 MEMBER provincecode3 )
    Q_PROPERTY( QString provincename3 MEMBER provincename3)
    Q_PROPERTY( QString commitee3 MEMBER commitee3)
    Q_PROPERTY( QString placename3 MEMBER placename3 )
    Q_PROPERTY( QString vtype3 MEMBER vtype3)
    Q_PROPERTY( QString username3 MEMBER username3)
    Q_PROPERTY( QString userphone3 MEMBER userphone3)
    Q_PROPERTY( QString inout3 MEMBER inout3 )
    Q_PROPERTY( int     ampm3 MEMBER ampm3)
    Q_PROPERTY( QString userid3 MEMBER userid3)

    Q_PROPERTY( QString lastInsertedVtype MEMBER lastInsertedVtype)

    Q_PROPERTY( QString manualCountDay1 MEMBER manualCountDay1)
    Q_PROPERTY( QString autoCountDay1   MEMBER autoCountDay1)
    Q_PROPERTY( QString manualCountDay2 MEMBER manualCountDay2)
    Q_PROPERTY( QString autoCountDay2   MEMBER autoCountDay2)
    Q_PROPERTY( QString manualCountDay3 MEMBER manualCountDay3)
    Q_PROPERTY( QString autoCountDay3   MEMBER autoCountDay3)

    Q_PROPERTY( QVariantList v_name MEMBER v_name)
    Q_PROPERTY( QVariantList v_path MEMBER v_path)
    Q_PROPERTY( QVariantList v_datetime MEMBER v_datetime)
    Q_PROPERTY( QVariantList v_nas_id MEMBER v_nas_id)
    Q_PROPERTY( QVariantList v_status MEMBER v_status)

    Q_PROPERTY( QString ai_engine             MEMBER ai_engine)
    Q_PROPERTY( QString ai_rec_fps            MEMBER ai_rec_fps)
    Q_PROPERTY( QString ai_rec_resolutions    MEMBER ai_rec_resolutions)
    Q_PROPERTY( QString ai_data_transfer_time MEMBER ai_data_transfer_time)
    Q_PROPERTY( QString ai_human_masking      MEMBER ai_human_masking)
    Q_PROPERTY( QString ai_rec_save_pos       MEMBER ai_rec_save_pos)

public:
    Q_INVOKABLE void loadUserInfoV1();
    Q_INVOKABLE void loadUserInfoV2();
    Q_INVOKABLE void loadUserInfoV3();

    Q_INVOKABLE void saveUserInfoV1();
    Q_INVOKABLE void saveUserInfoV2();
    Q_INVOKABLE void saveUserInfoV3();

    Q_INVOKABLE void saveCountInfoDay();
    Q_INVOKABLE void loadCountInfoDay();

    Q_INVOKABLE void loadUserVideoList();
    Q_INVOKABLE void saveUserVideoList();

    Q_INVOKABLE void sendAuditorData(QVariantList infos);

    Q_INVOKABLE void deleteFileByURL(QString path_url);
    HumanCountUserInfoData(QObject *parent = nullptr);

    static HumanCountUserInfoData *instance() { return m_instance; }

    Q_INVOKABLE QVariantList getFileListFromAppRoot();

    Q_INVOKABLE void loadAiSettings();
    Q_INVOKABLE void saveAiSettings();
private:
    static HumanCountUserInfoData *m_instance;

    QString electioncode1;
    QString provincecode1;
    QString provincename1;
    QString commitee1;
    QString placename1;
    QString vtype1;
    QString username1;
    QString userphone1;
    QString inout1;
    int ampm1;
    QString userid1;

    QString electioncode2;
    QString provincecode2;
    QString provincename2;
    QString commitee2;
    QString placename2;
    QString vtype2;
    QString username2;
    QString userphone2;
    QString inout2;
    int ampm2;
    QString userid2;

    QString electioncode3;
    QString provincecode3;
    QString provincename3;
    QString commitee3;
    QString placename3;
    QString vtype3;
    QString username3;
    QString userphone3;
    QString inout3;
    int ampm3;
    QString userid3;

    QString lastInsertedVtype;

    QString manualCountDay1;
    QString autoCountDay1;
    QString manualCountDay2;
    QString autoCountDay2;
    QString manualCountDay3;
    QString autoCountDay3;

    QString manualCountCurrent;
    QString autoCountCurrent;

    QVariantList v_name;
    QVariantList v_path;
    QVariantList v_datetime;
    QVariantList v_nas_id;
    QVariantList v_status;

    // Home(AI분석 페이지) Setting info
    QString ai_engine;
    QString ai_rec_fps;
    QString ai_rec_resolutions;
    QString ai_data_transfer_time;
    QString ai_human_masking;
    QString ai_rec_save_pos;
};


#endif
