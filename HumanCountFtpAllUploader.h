#ifndef HUMAN_COUNT_FTP_ALL_UPLOADER_H_00
#define HUMAN_COUNT_FTP_ALL_UPLOADER_H_00

#include <qthread.h>
#include <qvariant.h>
#include <qmetatype.h>
#include <qlist.h>

class HumanCountFtpAllUploader : public QThread
{
	Q_OBJECT

	// 쓰레드 루틴
	void run();

public:
	explicit HumanCountFtpAllUploader(void);
	virtual ~HumanCountFtpAllUploader(void);

	Q_INVOKABLE void addUploadList(int idx, QString filePath, QString fileName, QString hostName){
		mIdxList.append(idx);
		mFilePathList.append(filePath);
		mFileNameList.append(fileName);
		mHostNameList.append("59.15.239."+hostName);
	}

	Q_INVOKABLE void clearDownloadList(){
		mIdxList.clear();
		mFilePathList.clear();
		mFileNameList.clear();
		mHostNameList.clear();
	}

	Q_INVOKABLE void stop() {
		mRun = false;
	}

    // 필수 입력 정보
    QList<int>  mIdxList;
    QStringList mFilePathList;
    QStringList mFileNameList;
    QStringList mHostNameList;

signals:
	void sigCurrentFileProgress(QVariantList result);
	void sigCurrentFileProgressText(QString result);
	void sigCurrentFinishUpload(QVariantList result);
	void sigCurrentErrorUpload(QVariantList result);

	void sigTotalProgress(QVariantList result);
	void sigTotalProgressText(QString result);
	void sigTotalFinishUpload(QVariantList result);
	void sigTotalErrorUpload(QVariantList result);
    
protected:
	bool mRun;

};


#endif
