#include "HumanCountDetectorCV.h"

HumanCountDetectorCV::HumanCountDetectorCV(const std::string& modelPath,
                           const bool& isGPU = true,
                           const cv::Size& inputSize = cv::Size(640, 640))
{
    model_net = cv::dnn::readNetFromONNX(modelPath.c_str());
    this->inputImageShape = cv::Size2f(inputSize);
}

void HumanCountDetectorCV::getBestClassInfo(float* it, const int& numClasses,
                                    float& bestConf, int& bestClassId)
{
    // first 5 element are box and obj confidence
    bestClassId = 5;
    bestConf = 0;

    for (int i = 5; i < numClasses + 5; i++)
    {
        if (it[i] > bestConf)
        {
            bestConf = it[i];
            bestClassId = i - 5;
        }
    }

}

void HumanCountDetectorCV::preprocessing(cv::Mat &image, cv::Mat& blob)
{
    cv::Mat resizedImage, floatImage;
    cv::cvtColor(image, resizedImage, cv::COLOR_BGR2RGB);
    utils::letterbox(resizedImage, resizedImage, this->inputImageShape,
                     cv::Scalar(114, 114, 114), this->isDynamicInputShape,
                     false, true, 32);

    cv::dnn::blobFromImage(image,blob,1./255.,cv::Size(inputImageShape.width,inputImageShape.height),cv::Scalar(), true, false);
}

std::vector<Detection> HumanCountDetectorCV::postprocessing(const cv::Size& resizedImageShape,
                                                    const cv::Size& originalImageShape,
                                                    std::vector<cv::Mat>& outputTensors,
                                                    const float& confThreshold, const float& iouThreshold)
{
    std::vector<cv::Rect> boxes;
    std::vector<float> confs;
    std::vector<int> classIds;

    float *data = (float *)outputTensors[0].data;
    auto         output_tensor_size = outputTensors[0].size;
    //std::vector<float> output(rawOutput, rawOutput + output_tensor_size.width);

    int numClasses = 80;
    int rows = output_tensor_size[1];

    for(int i=0; i< rows; i++)
    {
        float clsConf = data[4];
        if (clsConf > confThreshold)
        {
            int centerX = (int) (data[0]);
            int centerY = (int) (data[1]);
            int width = (int) (data[2]);
            int height = (int) (data[3]);
            int left = centerX - width / 2;
            int top = centerY - height / 2;

            float objConf;
            int classId;
            this->getBestClassInfo(data, numClasses, objConf, classId);

            float confidence = clsConf * objConf;

            boxes.emplace_back(left, top, width, height);
            confs.emplace_back(confidence);
            classIds.emplace_back(classId);
        }

        data += 85;
    }

    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confs, confThreshold, iouThreshold, indices);
    // std::cout << "amount of NMS indices: " << indices.size() << std::endl;

    std::vector<Detection> detections;

    for (int idx : indices)
    {
        Detection det;
        det.box = cv::Rect(boxes[idx]);
        utils::scaleCoords(resizedImageShape, det.box, originalImageShape);

        det.conf = confs[idx];
        det.classId = classIds[idx];
        detections.emplace_back(det);
    }

    return detections;
}

std::vector<Detection> HumanCountDetectorCV::detect(cv::Mat &image, const float& confThreshold = 0.4,
                                            const float& iouThreshold = 0.45)
{
    cv::Mat blob;
    cv::dnn::blobFromImage(image,blob,1./255.,cv::Size(inputImageShape.width,inputImageShape.height),cv::Scalar(), true, false);

    model_net.setInput(blob);
    std::vector<cv::Mat> outputs;
    model_net.forward(outputs,model_net.getUnconnectedOutLayersNames());
    
    cv::Size resizedShape = cv::Size((int)inputImageShape.width,(int)inputImageShape.height);
    std::vector<Detection> result = this->postprocessing(resizedShape,
                                                         image.size(),
                                                         outputs,
                                                         confThreshold, iouThreshold);

    return result;
}
