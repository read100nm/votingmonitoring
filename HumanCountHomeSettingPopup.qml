import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import HumanCountLib 1.0

Item {
    x:0
    y:0
    width: parent.width
    height: parent.height
    property bool isOK:false
    property var applySettingFunction: null
    // Popup object
    Popup {
        id:messagePopup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        modal: true
        focus: true
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closeFindAddressPopup();
                //sigClosed();
            }
        }

        Rectangle
        {
            id:popup_rect
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*3/5
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.085
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "AI 엔진"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: aiModelComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.085
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "기본", "Lite"]
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.2
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "영상저장 FPS"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: fpsComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.2
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "100%", "70%","30%"]
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.315
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "영상 저장 해상도"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: resolutionComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.315
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "640x480", "320x240"]
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.43
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "분석 Data 전송 간격"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: dataTransIntervalComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.43
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "1분단위", "5분단위","10분단위"]
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.545
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "사람 마스킹 기능"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: humanMaskingComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.545
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "Y(사용)", "N(미사용)"]
            }

            Text {
                x: parent.width * 0.0667
                y: parent.height * 0.66
                width: parent.width * 0.38
                height: parent.height * 0.1
                color: "#000000"
                font.pixelSize: height * 0.3
                text: "영상데이터 저장소"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            HumanCountComboBox {
                id: saveVideoComboBox
                x: parent.width * 0.483333333
                y: parent.height * 0.66
                width: parent.width * 0.45
                height: parent.height * 0.1
                model: [ "어플 내부", "어플 외부"]
            }

            /*Text
            {
                id:messageText
                x:popup_rect.width * 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                y:popup_rect.height* 0.5        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                width:popup_rect.width*0.8
                height:popup_rect.height*0.3
                color:"black"
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                wrapMode: Text.WordWrap
            }*/

            
            Button
            {
                id:ok_btn
                x: popup_rect.width * 0.05
                y: popup_rect.height* 0.8
                width: popup_rect.width*0.425
                height: popup_rect.height*0.15
                contentItem: Text {
                    fontSizeMode: Text.Fit
                    text: qsTr("확인")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onClicked: {
                    var ai_engine             = aiModelComboBox.currentIndex;
                    var ai_rec_fps            = fpsComboBox.currentIndex
                    var ai_rec_resolutions    = resolutionComboBox.currentIndex;
                    var ai_data_transfer_time = dataTransIntervalComboBox.currentIndex;
                    var ai_human_masking      = humanMaskingComboBox.currentIndex;
                    var ai_rec_save_pos       = saveVideoComboBox.currentIndex;
                    ai_engine = ai_engine.toString();
                    ai_rec_fps = ai_rec_fps.toString();
                    ai_rec_resolutions = ai_rec_resolutions.toString();
                    ai_data_transfer_time = ai_data_transfer_time.toString();
                    ai_human_masking = ai_human_masking.toString();
                    ai_rec_save_pos = ai_rec_save_pos.toString();
                    humanCountUserInfoData.ai_engine = ai_engine;
                    humanCountUserInfoData.ai_rec_fps = ai_rec_fps;
                    humanCountUserInfoData.ai_rec_resolutions = ai_rec_resolutions;
                    humanCountUserInfoData.ai_data_transfer_time = ai_data_transfer_time;
                    humanCountUserInfoData.ai_human_masking = ai_human_masking;
                    humanCountUserInfoData.ai_rec_save_pos = ai_rec_save_pos;
                    humanCountUserInfoData.saveAiSettings();

                    isOK = true;
                    closeFindAddressPopup();
                }
            }

            Button
            {
                id:cancel_btn
                x: popup_rect.width * 0.5
                y: popup_rect.height* 0.8
                width: popup_rect.width*0.425
                height: popup_rect.height*0.15
                contentItem: Text {
                    fontSizeMode: Text.Fit
                    text: qsTr("취소")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onClicked: {
                    isOK = false;
                    closeFindAddressPopup();
                }
            }
        }
    }



    function openMessagePopup(applyCallback){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수

        // fill current settings
        humanCountUserInfoData.loadAiSettings();
        var ai_engine             = humanCountUserInfoData.ai_engine;
        var ai_rec_fps            = humanCountUserInfoData.ai_rec_fps;
        var ai_rec_resolutions    = humanCountUserInfoData.ai_rec_resolutions;
        var ai_data_transfer_time = humanCountUserInfoData.ai_data_transfer_time;
        var ai_human_masking      = humanCountUserInfoData.ai_human_masking;
        var ai_rec_save_pos       = humanCountUserInfoData.ai_rec_save_pos;

        aiModelComboBox.currentIndex           = parseInt(ai_engine);
        fpsComboBox.currentIndex               = parseInt(ai_rec_fps);
        resolutionComboBox.currentIndex        = parseInt(ai_rec_resolutions);
        dataTransIntervalComboBox.currentIndex = parseInt(ai_data_transfer_time);
        humanMaskingComboBox.currentIndex      = parseInt(ai_human_masking);
        saveVideoComboBox.currentIndex         = parseInt(ai_rec_save_pos);
        console.log("open Ai Setting Popup")

        applySettingFunction = applyCallback;
        // open popup
        messagePopup.open();

    }
    function closeFindAddressPopup()
    {
        console.log("close Ai Setting Popup")
        if(isOK && (applySettingFunction!=null))
        {
            applySettingFunction();
        }
        messagePopup.close();
    }
}
