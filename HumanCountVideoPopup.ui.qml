import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import Qt.labs.platform 1.1
import QtMultimedia 5.15

Item {
    id: lookupCargoInfoItem
    x:0
    y:0
    width: parent.width
    height: parent.height

    // Popup object
    Popup {
        id:cargoInfoPopup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closeVideoPlayPopup();
            }
        }



        
        Rectangle {
            id: add_cargo_info_ui
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*4/5
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            MediaPlayer {
                id: mediaplayer
                autoPlay: true
                //source: ""
            }

            Rectangle {
                x: add_cargo_info_ui.width * 0.1
                y: add_cargo_info_ui.height * 0.1
                width: add_cargo_info_ui.width * 0.8
                height: add_cargo_info_ui.height * 0.6
                color: "black"
            }
            VideoOutput {
                x: add_cargo_info_ui.width * 0.1
                y: add_cargo_info_ui.height * 0.1
                width: add_cargo_info_ui.width * 0.8
                height: add_cargo_info_ui.height * 0.6
                source: mediaplayer
                //orientation: RunPlatform=="ANDROID"?0:270
                orientation: 270
            }

            Button {
                id: finish_add_cargo_btn
                x: add_cargo_info_ui.width * 0.1
                y: add_cargo_info_ui.height * 0.8
                width: add_cargo_info_ui.width * 0.3
                height: add_cargo_info_ui.height * 0.1
                
                contentItem: Text {
                    fontSizeMode: Text.Fit
                    text: qsTr("확인")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onClicked:{
                    closeVideoPlayPopup();
                }
            }
        }
    }


    function openVideoPlayPopup(path){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수
        console.log("opencargoInfoPopup");
        console.log(path);
        cargoInfoPopup.open();

        if(path.includes("file://") == false)
            path = "file://"+path;
        
        console.log(path);
        console.log("hihi");
        
        mediaplayer.source=path;
        mediaplayer.seek(0);
        mediaplayer.play();
    }

    function closeVideoPlayPopup()
    {
        console.log("closecargoInfoPopup")
        mediaplayer.stop();
        cargoInfoPopup.close();
    }

}
