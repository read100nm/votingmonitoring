#ifndef HUMAN_COUNT_DETECTOR_CV_H_
#define HUMAN_COUNT_DETECTOR_CV_H_

#include <utility>
#include "utils.h"

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/all_layers.hpp>

class HumanCountDetectorCV
{
public:
    explicit HumanCountDetectorCV(std::nullptr_t) {};
    HumanCountDetectorCV(const std::string& modelPath,
                 const bool& isGPU,
                 const cv::Size& inputSize);

    std::vector<Detection> detect(cv::Mat &image, const float& confThreshold, const float& iouThreshold);

private:
    
    void preprocessing(cv::Mat &image, cv::Mat& blob);
    std::vector<Detection> postprocessing(const cv::Size& resizedImageShape,
                                          const cv::Size& originalImageShape,
                                          std::vector<cv::Mat>& outputTensors,
                                          const float& confThreshold, const float& iouThreshold);

    static void getBestClassInfo(float* it, const int& numClasses,
                                 float& bestConf, int& bestClassId);

    //std::vector<const char*> inputNames;
    //std::vector<const char*> outputNames;
    bool isDynamicInputShape{};
    cv::Size2f inputImageShape;

    cv::dnn::Net model_net;

};

#endif