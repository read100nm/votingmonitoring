#ifndef HUMAN_COUNT_FTP_UPLOADER_H_00
#define HUMAN_COUNT_FTP_UPLOADER_H_00

#include <qthread.h>
#include <qvariant.h>
#include <qmetatype.h>

class HumanCountFtpUploader : public QThread
{
	Q_OBJECT

	// 쓰레드 루틴
	void run();

public:
	HumanCountFtpUploader(void);
	~HumanCountFtpUploader(void);

	void stop() {
		mRun = false;
	}

    // 필수 입력 정보
    int mListIndex;
    QString mFilePath;
    QString mFileName;
    QString mHostName;
signals:
	void sigFinishUpload(QVariantList result);
    void sigErrorUpload(QVariantList result);
    void sigProgress(QVariantList result);
    
protected:
	bool mRun;

};


#endif
