#include "HumanCountAuditorData.h"
#include "HumanCountFtpUploader.h"
#include "HumanCountFtpAllUploader.h"
#include <qdebug.h>
#include <qstandardpaths.h>
#include <qsettings.h>

HumanCountAuditorData *HumanCountAuditorData::m_instance = nullptr;

HumanCountAuditorData::HumanCountAuditorData(QObject *parent) : QObject(parent)
{
    m_instance     = this;
    mMaxUpload     = 4;
    mCurrentUpload = 0;
    mCurrentNasID  = 1;

    isAllVideoUploading = false;
}

QVariantList HumanCountAuditorData::uploadVideoFile(int listIdx, QString fileName, QString filePath, QString nas_id)
{
    // 1. max working check
    // 2. make thread
    // 3. make qt connections
    // 4. set filename
    // 5. set filepath
    // 6. set list index
    // 7. set host (based nas id)
    // 8. add map
    // 9. run thread


    // 1. max working check
    QVariantList retrun_value_list;
    if(mCurrentUpload >= mMaxUpload)
    {
        retrun_value_list.append(false);
        retrun_value_list.append("최대 업로드 숫자 초과");
        return retrun_value_list;
    }

    // 2. make thread
    HumanCountFtpUploader* thread = new HumanCountFtpUploader();

    // 3. make qt connections
    connect(thread, &HumanCountFtpUploader::finished, thread, &QObject::deleteLater);
	connect(thread, &HumanCountFtpUploader::sigFinishUpload, this, &HumanCountAuditorData::sigFinishUpload);
    connect(thread, &HumanCountFtpUploader::sigErrorUpload, this, &HumanCountAuditorData::sigErrorUpload);
    connect(thread, &HumanCountFtpUploader::sigProgress, this, &HumanCountAuditorData::sigProgress);
    connect(thread, &HumanCountFtpUploader::sigFinishUpload, this, &HumanCountAuditorData::slotFinishUpload);
    connect(thread, &HumanCountFtpUploader::sigErrorUpload, this, &HumanCountAuditorData::slotErrorUpload);

    // 4. set filename
    thread->mFileName = fileName;

    // 5. set filepath
    if(filePath.contains("file://"))
        filePath = filePath.replace("file://","");

    qDebug()<<filePath;
    thread->mFilePath = filePath;

    // 6. set list index
    thread->mListIndex = listIdx;

    // 7. set host (based nas id)
    thread->mHostName = "59.15.239." + nas_id;
    qDebug()<<thread->mHostName;
    
    // 8. add map
    mListKeyThreadMap.insert(listIdx,thread);

    // 9. run thread
    thread->start();

    retrun_value_list.append(true);
    return retrun_value_list;
}

void HumanCountAuditorData::slotFinishUpload(QVariantList value)
{
    if(mListKeyThreadMap.find(value[0].toInt()) != mListKeyThreadMap.end())
    {
        mListKeyThreadMap.remove(value[0].toInt());
    }
}

void HumanCountAuditorData::slotErrorUpload(QVariantList value)
{
    if(mListKeyThreadMap.find(value[0].toInt()) != mListKeyThreadMap.end())
    {
        mListKeyThreadMap.remove(value[0].toInt());
    }
}

QVariantList HumanCountAuditorData::stopUploading(int listIdx)
{
    QVariantList retrun_value_list;
    if(mListKeyThreadMap.find(listIdx) == mListKeyThreadMap.end())
    {
        retrun_value_list.append(false);
        retrun_value_list.append("이미 업로드가 완료되었습니다.");
        return retrun_value_list;
    }
    else
    {
        mListKeyThreadMap[listIdx]->stop();
        retrun_value_list.append(true);
    }

    return retrun_value_list;
}

bool HumanCountAuditorData::isUploading()
{
    if(mListKeyThreadMap.size()==0)
        return false;

    return true;
}


QString HumanCountAuditorData::getSavedID()
{
    qDebug()<<"Start get Notification ";

    QString path = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    QString filename = ".votingmonitoring.config";
    QSettings settings(path + "/"+ filename,QSettings::IniFormat);
    qDebug()<<path + "/"+ filename;

    settings.beginGroup("USERINFO");
    QString title = settings.value("FCMTitle","").toString();
    QString body  = settings.value("FCMBody","").toString();
    settings.endGroup();

    QString saved_id="";
    qDebug()<<"End get Notification "<<title<<"/"<<body;
    body=body.replace("아이디 : ","");
    body=body.replace("패스워드 : ","");
    QStringList resultList = body.split(" ");
    if(resultList.size() >= 1)
        saved_id = resultList[0];

    return saved_id;
}
