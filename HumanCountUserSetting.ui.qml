import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.12
import QtMultimedia 5.12
import QtWebView 1.15
import HumanCountLib 1.0
import QtPositioning 5.15
import QtQuick.LocalStorage 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Page {

    id: recposition_page
    width: stackView.width
    height: stackView.height

    //property string selected_province:""
    //property string selected_section:""
    //property string selected_office:""
    property var selected_office_lat:""
    property var selected_office_lng:""
    property string election_code:""
    property string election_name:""
    property string election_date:""
    property string election_vtype:"사전1"
    property bool   earlyVoteCB1_checked: true
    property bool   earlyVoteCB2_checked: false
    property bool   electionDayVoteCB_checked: false
    property bool   votingAMCB_checked: true
    property bool   votingPMCB_checked: true
    property bool   votingInCB_checked: true
    property bool   votingOutCB_checked: false
    property string saveBtnColor:"#CFE9F0"
    property bool   waitingApproval: false
    //property boolean isMapLoaded: False

    title: qsTr("촬영위치설정")

    HumanCountMapPopup{
        id: mapInfoPopup
    }

    HumanCountQmlUtils{
        id: votingMonitoringQmlUtils
    }

    PositionSource {
        id: my_pos_src
        updateInterval: 1000
        active: true

        onPositionChanged:{
            var coord = my_pos_src.position.coordinate;
            //console.log("Coordinate:", coord.longitude, coord.latitude);
        }
    }

    function clickEarlyVoteCB1(){
        if(waitingApproval == true)
        {
            main_window.openMessagePopup("ID승인을 기다리고 있습니다. 승인 완료후 새로운 신청을 해주세요");
            return;
        }
        if(earlyVoteCB1_checked == false)
        {
            earlyVoteCB1_checked=true;
            earlyVoteCB2_checked=false;
            electionDayVoteCB_checked=false;
            election_vtype="사전1";
            provinceComboBox_listmodel.clear();
            provinceTownComboBox_listmodel.clear();
            votingPlaceComboBox_listmodel.clear();

            votingInCBText.enabled=true;
            votingInCB.enabled=true;
            votingOutCBText.enabled=true;
            votingOutCB.enabled=true;
            votingInCB_checked = true;
            votingOutCB_checked = true;
            loadProvinceList(function(){
            loadVotingPlcaeInfo("사전1")});
        }
    }

    function clickEarlyVoteCB2(){
        if(waitingApproval == true)
        {
            main_window.openMessagePopup("ID승인을 기다리고 있습니다. 승인 완료후 새로운 신청을 해주세요");
            return;
        }

        if(earlyVoteCB2_checked == false)
        {
            earlyVoteCB1_checked=false;
            earlyVoteCB2_checked=true;
            electionDayVoteCB_checked=false;
            election_vtype="사전2";
            provinceComboBox_listmodel.clear();
            provinceTownComboBox_listmodel.clear();
            votingPlaceComboBox_listmodel.clear();

            votingInCBText.enabled=true;
            votingInCB.enabled=true;
            votingOutCBText.enabled=true;
            votingOutCB.enabled=true;
            votingInCB_checked = true;
            votingOutCB_checked = true;
            loadProvinceList(function(){
            loadVotingPlcaeInfo("사전2")});
        }
    }

    function clickElectionDayCB(){
        if(waitingApproval == true)
        {
            main_window.openMessagePopup("ID승인을 기다리고 있습니다. 승인 완료후 새로운 신청을 해주세요");
            return;
        }

        if(electionDayVoteCB_checked == false)
        {
            earlyVoteCB1_checked=false;
            earlyVoteCB2_checked=false;
            electionDayVoteCB_checked=true;
            election_vtype="선거일";
            provinceComboBox_listmodel.clear();
            provinceTownComboBox_listmodel.clear();
            votingPlaceComboBox_listmodel.clear();

            votingInCBText.enabled=false;
            votingInCB.enabled=false;
            votingOutCBText.enabled=false;
            votingOutCB.enabled=false;
            votingInCB_checked = true;
            votingOutCB_checked = false;
            loadProvinceList(function(){
            loadVotingPlcaeInfo("선거일")});
        }
    }

    /////////////////////////////////////////////////
    /////////////////////// Tab components///////////
    /////////////////////////////////////////////////
     // 밑줄
    Rectangle{
        x: 0
        y: recposition_page.height* (0.07) 
        width: recposition_page.width 
        height: 1
        color: "#E5E5E5"
    }
  
    // 컬러박스1
    Rectangle{
        x: 0
        y: 5 
        width: recposition_page.width  / 3.0
        height: recposition_page.height* (0.07)-5
        border.width: 0
        color: "#E5E5E5"
        visible: earlyVoteCB1_checked
    }

    // 컬러박스2
    Rectangle{
        x: recposition_page.width * 1.0/3.0
        y: 5 
        width: recposition_page.width  / 3.0
        height: recposition_page.height* (0.07)-5
        border.width: 0
        color: "#E5E5E5"
        visible: earlyVoteCB2_checked
    }

    // 컬러박스3
    Rectangle{
        x: recposition_page.width * 2.0/3.0
        y: 5 
        width: recposition_page.width  / 3.0
        height: recposition_page.height* (0.07) -5
        border.width: 0
        color: "#E5E5E5"
        visible: electionDayVoteCB_checked
    }

    // 텍스트1
    Text {
        id: earlyVoteCB
        x: 0
        y: 0
        width: recposition_page.width / 3.0
        height: recposition_page.height * 0.07
        text: qsTr("사전 1일차")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        MouseArea {
            anchors.fill: parent
            onClicked:{
                clickEarlyVoteCB1();
            }
        }
    }

    // 텍스트2
    Text {
        id: earlyVoteCB2
        x: recposition_page.width * 1.0/3.0
        y: 0
        width: recposition_page.width * 1.0/3.0
        height: recposition_page.height * 0.07
        text: qsTr("사전 2일차")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        MouseArea {
            anchors.fill: parent
            onClicked:{
                clickEarlyVoteCB2();
            }
        }
    }
   
    // 텍스트3
    Text {
        id: electionDayVoteCB
        x: recposition_page.width * 2.0/3.0
        y: 0
        width: recposition_page.width * 1.0/3.0
        height: recposition_page.height * 0.07
        text: qsTr("선거일")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        MouseArea {
            anchors.fill: parent
            onClicked:{
                clickElectionDayCB();
            }
        }
    }
    /////////////////////////////////////////////////
    /////////////////////// Tab components///////////
    /////////////////////////////////////////////////

    Text {
        id: electionText
        x: recposition_page.width * 0.0667
        y: earlyVoteCB.y + recposition_page.height * 0.085
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        font.pixelSize: height * 0.761904762
        text: "선거 구분"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    HumanCountComboBox {
        id: electionComboBox
        x: recposition_page.width * 0.483333333
        y: earlyVoteCB.y + recposition_page.height * 0.085
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        textRole: "electionname"
        model: ListModel {
            id: electionComboBox_listmodel
        }
        delegate: ItemDelegate{
            width: votingPlaceComboBox.width
            contentItem: Item {
                width: parent.width
                Text{
                    id: vplace_name_item
                    x:0
                    width: parent.width * 0.6
                    text: model.electionname
                    fontSizeMode: Text.VeriticalFit
                    minimumPixelSize: 2
                    font.pixelSize: 12
                    color:"#000000"
                }

                Text{
                    id: vplace_amc_item
                    x:parent.width * 0.65
                    width: parent.width * 0.35
                    text: model.electionymd
                    fontSizeMode: Text.VeriticalFit
                    minimumPixelSize: 2
                    font.pixelSize: 12
                    color:"#0000FF"
                }
            }
            highlighted: votingPlaceComboBox.highlightedIndex == index
        }
        onActivated:{
            election_code = electionComboBox_listmodel.get(index).electioncode;
            election_name = electionComboBox_listmodel.get(index).electionname;
            election_date = electionComboBox_listmodel.get(index).electionymd;

            votingPlaceComboBox_listmodel.clear();
            var selected_code = provinceComboBox_listmodel.get(provinceComboBox.currentIndex).provincecode;
            console.log(selected_code);
            loadProvinceTownList("electioncode="+election_code+
                                "&provincecode="+selected_code+"&vtype="+election_vtype);
            //votingPlaceComboBox_listmodel.clear();
            //var selected_code = provinceComboBox_listmodel.get(index).provincecode;
            //console.log(selected_code);
            //loadProvinceTownList("electioncode="+election_code+
            //                    "&provincecode="+selected_code+"&vtype="+election_vtype);
        }
        enabled: userIDTextInput.text==""?true:false
    }

   
    Text {
        id: provinceText
        x: recposition_page.width * 0.0667
        y: electionText.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        font.pixelSize: height * 0.761904762
        text: "광역 구분 (도/시)"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        
    }

    HumanCountComboBox {
        id: provinceComboBox
        x: recposition_page.width * 0.483333333
        y: electionComboBox.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        textRole: "provincename"
        model: ListModel {
            id: provinceComboBox_listmodel
        }
        onActivated:{
            votingPlaceComboBox_listmodel.clear();
            var selected_code = provinceComboBox_listmodel.get(index).provincecode;
            console.log(selected_code);
            loadProvinceTownList("electioncode="+election_code+
                                "&provincecode="+selected_code+"&vtype="+election_vtype);
        }
        enabled: userIDTextInput.text==""?true:false
    }


    Text {
        id: commiteeText
        x: recposition_page.width * 0.0667
        y: provinceText.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        font.pixelSize: height * 0.761904762
        text: "상세 구분(시/군/구)"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    HumanCountComboBox {
        id: commiteeComboBox
        x: recposition_page.width * 0.483333333
        y: provinceComboBox.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        textRole: "commitee"
        model: ListModel {
            id: provinceTownComboBox_listmodel
        }
        onActivated:{
            var p_code = provinceTownComboBox_listmodel.get(index).provincecode;
            var commitee_name = provinceTownComboBox_listmodel.get(index).commitee;
            loadVotingPlaceList("electioncode="+election_code+
                                "&provincecode="+p_code+
                                "&commitee="+commitee_name+
                                "&vtype="+election_vtype);

            
        }
        enabled: userIDTextInput.text==""?true:false
    }

    Text {
        id: votingPlaceText
        x: recposition_page.width * 0.0667
        y: commiteeText.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        font.pixelSize: height * 0.761904762
        text: "투표소"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    HumanCountComboBox {
        id: votingPlaceComboBox
        x: recposition_page.width * 0.483333333
        y: commiteeComboBox.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        textRole: "placename"
        model: ListModel {
            id: votingPlaceComboBox_listmodel
        }
        delegate: ItemDelegate{
            width: votingPlaceComboBox.width
            contentItem: Item {
                width: parent.width
                Text{
                    id: vplace_name_item
                    x:0
                    width: parent.width * 0.6
                    text: model.placename
                    fontSizeMode: Text.HorizontalFit
                    minimumPixelSize: 2
                    font.pixelSize: 10 
                    color:model.amcnt==model.amcapa?"#FF0000":model.pmcnt==model.pmcapa?"#FF0000":"#0000ff"
                }

                Text{
                    id: vplace_amc_item
                    x:parent.width * 0.65
                    width: parent.width * 0.15
                    text: model.amcnt==model.amcapa?"마감":model.amcnt+"/"+model.amcapa
                    fontSizeMode: Text.HorizontalFit
                    minimumPixelSize: 2
                    font.pixelSize: 10 
                    color:model.amcnt==model.amcapa?"#FF0000":"#0000ff"
                }

                Text{
                    id: vplace_pmc_item
                    x:parent.width * 0.85
                    width: parent.width * 0.15
                    text: model.pmcnt==model.pmcapa?"마감":model.pmcnt+"/"+model.pmcapa
                    fontSizeMode: Text.HorizontalFit
                    minimumPixelSize: 2
                    font.pixelSize: 10 
                    color:model.pmcnt==model.pmcapa?"#FF0000":"#0000ff"
                }
            }
            highlighted: votingPlaceComboBox.highlightedIndex == index
        }
        onActivated:{
            selected_office_lat = votingPlaceComboBox_listmodel.get(index).latitude;
            selected_office_lng = votingPlaceComboBox_listmodel.get(index).longitude;
            console.log(selected_office_lat);
            console.log(selected_office_lng);
            votingAMCB.enabled=true;
            votingPMCB.enabled=true;
            //votingAMCBText.text="오전 ("+votingPlaceComboBox_listmodel.get(index).amcnt+")";
            //votingPMCBText.text="오후 ("+votingPlaceComboBox_listmodel.get(index).pmcnt+")";
            votingAMCBText.text="오전 ("+votingPlaceComboBox_listmodel.get(index).amcnt+"/"+ 
                                        votingPlaceComboBox_listmodel.get(index).amcapa+ ")";
            votingPMCBText.text="오후 ("+votingPlaceComboBox_listmodel.get(index).pmcnt+"/"+
                                        votingPlaceComboBox_listmodel.get(index).pmcapa+ ")";
        }
        enabled: userIDTextInput.text==""?true:false
    }

    Rectangle {
        id: votingInCB
        x: recposition_page.width * 0.0667
        y: votingPlaceText.y + recposition_page.height * 0.07
        width: recposition_page.height * 0.030882353
        height: recposition_page.height * 0.030882353
        border.color: "#CFE9F0"
        radius: 3
        Text {
            width: parent.width
            height: parent.height
            x: parent.width*0.2
            y: 0
            text: "✔"
            //color: control.down ? "#17a81a" : "#21be2b"
            visible: votingInCB_checked
        }
        MouseArea {
            anchors.fill: parent
            onClicked:{
                if(votingInCB_checked)
                {
                    votingInCB_checked = false;
                }
                else
                {
                    votingInCB_checked = true;
                }
            }
        }
        visible: electionDayVoteCB_checked?false:true
    }

    Rectangle {
        id: votingOutCB
        x: recposition_page.width * 0.483333333
        y: votingPlaceText.y + recposition_page.height * 0.07
        width: recposition_page.height * 0.030882353
        height: recposition_page.height * 0.030882353
        border.color: "#CFE9F0"
        radius: 3
        Text {
            width: parent.width
            height: parent.height
            x: parent.width*0.2
            y: 0
            text: "✔"
            //color: control.down ? "#17a81a" : "#21be2b"
            visible: votingOutCB_checked
        }
        MouseArea {
            anchors.fill: parent
            onClicked:{
                if(votingOutCB_checked)
                {
                    votingOutCB_checked = false;
                }
                else
                {
                    votingOutCB_checked = true;
                }
            }
        }
        visible: electionDayVoteCB_checked?false:true
    }

    Text {
        id: votingInCBText
        anchors.left: votingInCB.right
        leftPadding: recposition_page.width * 0.03
        y: votingPlaceText.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.3
        height: recposition_page.height * 0.030882353
        text: qsTr("관내")
        visible: electionDayVoteCB_checked?false:true
    }

    Text {
        id: votingOutCBText
        anchors.left: votingOutCB.right
        leftPadding: recposition_page.width * 0.03
        y: votingPlaceText.y + recposition_page.height * 0.07
        width: recposition_page.width * 0.3
        height: recposition_page.height * 0.030882353
        text: qsTr("관외")
        visible: electionDayVoteCB_checked?false:true
    }
    
    Rectangle {
        id: votingAMCB
        x: recposition_page.width * 0.0667
        y: votingInCB.y + recposition_page.height * 0.06
        width: recposition_page.height * 0.030882353
        height: recposition_page.height * 0.030882353
        border.color: "#CFE9F0"
        radius: 3
        Text {
            width: parent.width
            height: parent.height
            x: parent.width*0.2
            y: 0
            text: "✔"
            //color: control.down ? "#17a81a" : "#21be2b"
            visible: votingAMCB_checked
        }
        MouseArea {
            anchors.fill: parent
            onClicked:{
                if(votingAMCB_checked)
                {
                    votingAMCB_checked = false;
                }
                else
                {
                    votingAMCB_checked = true;
                }
            }
        }
        //enabled: false
    }
    Text {
        id: votingAMCBText
        anchors.left: votingAMCB.right
        leftPadding: recposition_page.width * 0.03
        y: votingInCB.y + recposition_page.height * 0.06
        width: recposition_page.width * 0.3
        height: recposition_page.height * 0.030882353
        text: qsTr("오전")
    }

    Rectangle {
        id: votingPMCB
        x: recposition_page.width * 0.483333333
        y: votingInCB.y + recposition_page.height * 0.06
        width: recposition_page.height * 0.030882353
        height: recposition_page.height * 0.030882353
        border.color: "#CFE9F0"
        radius: 3
        Text {
            width: parent.width
            height: parent.height
            x: parent.width*0.2
            y: 0
            text: "✔"
            //font.pointSize: 18
            //color: control.down ? "#17a81a" : "#21be2b"
            visible: votingPMCB_checked
        }
        MouseArea {
            anchors.fill: parent
            onClicked:{
                if(votingPMCB_checked)
                {
                    votingPMCB_checked = false;
                }
                else
                {
                    votingPMCB_checked = true;
                }
            }
            enabled: userIDTextInput.text==""?true:false
        }
        
        
        //checked: false
        //text: qsTr("오후")
        //enabled: false
    }
    Text {
        id: votingPMCBText
        anchors.left: votingPMCB.right
        leftPadding: recposition_page.width * 0.03
        y: votingInCB.y + recposition_page.height * 0.06
        width: recposition_page.width * 0.3
        height: recposition_page.height * 0.030882353
        text: qsTr("오후")
    }

    Rectangle{
        id: checkPositionBtn
        x: parent.width* 0.066666667
        y: votingPMCB.y + recposition_page.height * 0.055
        width: parent.width*0.666666667
        height: parent.height * 0.060588235
        color: checkPosInMap_ma.pressed? "#FFFFFF":"#CFE9F0"
        radius: height*0.458333333
        border.color: "#CFE9F0"
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter 
            width: parent.width* 0.487179487
            height: parent.height * 0.4375
            text: qsTr("지도에서 위치 확인")
            font.family: "Roboto"
            font.pixelSize: height* 0.761904762
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: checkPosInMap_ma
            anchors.fill: parent
            onClicked: {
                var coord = my_pos_src.position.coordinate;
                loadKakaoMap(selected_office_lat,selected_office_lng,coord.latitude,coord.longitude);
            }
            //enabled: userIDTextInput.text==""?true:false
        }
    }

    Rectangle {
        x: parent.width* 0.75
        y: votingPMCB.y + recposition_page.height * 0.055
        width: parent.width * 0.18
        height: parent.height * 0.060588235
        color: "transparent"
        border.color: "#CFE9F0"
        border.width: 2
        Image{
                x:2
                y:2
                width: parent.width-4
                height: parent.height * 0.5
                fillMode: Image.PreserveAspectFit
                source: "images/manual_icon.jpg"
        }
        Text {
            x: 2
            y: 2 + parent.height * 0.5
            width: parent.width -4
            height: parent.height * 0.5 -4
            text: qsTr("어플 사용법")
            font.family: "Roboto"
            font.pixelSize: height* 0.761904762
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                Qt.openUrlExternally("http://175.118.126.139/vote/data/user_guide.pdf")
                //var coord = my_pos_src.position.coordinate;
                //loadKakaoMap(selected_office_lat,selected_office_lng,coord.latitude,coord.longitude);
            }
            //enabled: userIDTextInput.text==""?true:false
        }
    }


    Text {
        id: nameLabelText
        x: recposition_page.width * 0.0667
        y: checkPositionBtn.y + recposition_page.height * 0.0924
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        
        font.pixelSize: height * 0.761904762
        text: "촬영자 이름"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Rectangle{
        id: nameTextRect
        x: recposition_page.width * 0.483333333
        y: checkPositionBtn.y + recposition_page.height * 0.0792
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#FFFFFF"
        radius: parent.height * 0.55
        border.width: 1
        border.color: "#CFE9F0"
    }

    TextInput {
        id: nameTextInput
        x: recposition_page.width * 0.55
        y: checkPositionBtn.y + recposition_page.height * 0.0924
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#000000"
        enabled: userIDTextInput.text==""?true:false
    }

    Text {
        id: phoneNumberText
        x: recposition_page.width * 0.0667
        y: nameLabelText.y + recposition_page.height * 0.0824
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        
        font.pixelSize: height * 0.761904762
        text: "전화번호"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Rectangle{
        id: phoneNumberTextRect
        x: recposition_page.width * 0.483333333
        y: nameLabelText.y + recposition_page.height * 0.0692
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#FFFFFF"
        radius: parent.height * 0.55
        border.width: 1
        border.color: "#CFE9F0"
    }

    TextInput {
        id: phoneNumberTextInput
        x: recposition_page.width * 0.55
        y: nameLabelText.y + recposition_page.height * 0.0824
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#000000"
        validator: RegExpValidator { 
            regExp: /^\d{3}\d{4}\d{4}$/;
        }
        inputMethodHints: Qt.ImhDigitsOnly
        enabled: userIDTextInput.text==""?true:false
    }

    Rectangle{
        id: saveBtn
        x: parent.width* 0.066666667
        y: phoneNumberText.y + recposition_page.height * 0.0592
        width: parent.width*0.866666667
        height: parent.height * 0.060588235
        color: savePos_ma.pressed? "#FFFFFF":userIDTextInput.text!=""?"#AAAAAA":saveBtnColor
        radius: height*0.458333333
        border.color: saveBtnColor
        enabled: userIDTextInput.text==""?true:false
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter 
            width: parent.width* 0.487179487
            height: parent.height * 0.4375
            text: qsTr("저장")
            font.family: "Roboto"
            font.pixelSize: height* 0.761904762
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: savePos_ma
            anchors.fill: parent
            onClicked: {
                if(userIDTextInput.text != ""){
                    changeInOutInfo();
                }
                else{
                    var pi = provinceComboBox.currentIndex;
                    var ci = commiteeComboBox.currentIndex;
                    var vi = votingPlaceComboBox.currentIndex;
                    //nameTextInput
                    //phoneNumberTextInput
                    if(pi <0){

                        main_window.openMessagePopup("광역 구분(도/시)을 선택해 주세요.");
                        return;
                    } 
                    if(ci <0){
                        main_window.openMessagePopup("상세 구분(시/군/구)을 선택해 주세요.");
                        return;
                    } 
                    if(vi <0){
                        main_window.openMessagePopup("투표소를 선택해 주세요.");
                        return;
                    } 
                    if(nameTextInput.text == "")
                    {
                        main_window.openMessagePopup("촬영자 이름을 입력해 주세요.");
                        return;
                    }
                    if(phoneNumberTextInput.acceptableInput == false)
                    {
                        main_window.openMessagePopup("전화번호를 올바르게 입력해 주세요.");
                        return;
                    }
                    if((votingAMCB_checked == false)&& 
                        (votingPMCB_checked == false))
                    {
                        main_window.openMessagePopup("오전/오후 중 최소한 하나를 선택해주세요.");
                        return;
                    }
                    if((votingInCB_checked == false) && 
                        (votingOutCB_checked == false))
                    {
                        main_window.openMessagePopup("사전선거에서는 관내/관외 중 최소한 하나를 선택해주세요.");
                        return;
                    }
                    
                    var proCode    = provinceComboBox_listmodel.get(pi).provincecode;
                    var proName    = provinceComboBox_listmodel.get(pi).provincename;
                    var commText   = provinceTownComboBox_listmodel.get(ci).commitee;
                    var vtype      = election_vtype;
                    var votingText = votingPlaceComboBox_listmodel.get(vi).placename;
                    var placecode = votingPlaceComboBox_listmodel.get(vi).placecode;
                    var userName   = nameTextInput.text;
                    var userPhone  = phoneNumberTextInput.text;
                    //var inOut;//      = inoutComboBox.currentText;
                    //if(votingInCB_checked && votingOutCB_checked)
                    //    inOut = "둘다";
                    //else if(votingInCB_checked)
                    //    inOut = "관내";
                    //else if(votingOutCB_checked)
                    //    inOut = "관외";
                                                    
                    var ampmValue  = 0;
                    if(votingAMCB_checked)
                        ampmValue +=1;
                    if(votingPMCB_checked)
                        ampmValue +=2;

                    //console.log(inOut);
                    //console.log(ampmValue);

                    // election_code 선거번호
                    // placecode     투표소코드
                    // userName      성명(닉네임)
                    // userPhone     휴대폰 번호 숫자만
                    // hptoken       토큰은 모냐?
                    // ampmValue
                    // inOut
                    var inOut;
                    if(votingInCB_checked && votingOutCB_checked)
                        inOut = 3;
                    else if(votingInCB_checked)
                        inOut = 1;
                    else if(votingOutCB_checked)
                        inOut = 2;
                    var auditor_params = "electioncode="+election_code+
                                        "&placecode="+placecode+
                                        "&nickname="+userName+
                                        "&hpno="+userPhone+
                                        "&hptoken="+ main_window.userToken +
                                        "&datetype="+ampmValue+
                                        "&locatetype="+(inOut);

                    console.log(auditor_params);
                    insertVotingPlcaeInfo(proCode,proName,commText,votingText,vtype,
                            userName,userPhone,inOut,ampmValue);

                    regUserInfoToServer(auditor_params);
                    //if(RunPlatform != "ANDROID")
                    //idCheckTimer.start();
                    //main_window.openMessagePopup("성공적으로 등록했습니다");
                }
                
            }
        }
    }

    Text {
        id: userIDText
        x: recposition_page.width * 0.0667
        y: saveBtn.y + recposition_page.height * 0.0874
        width: recposition_page.width * 0.38
        height: recposition_page.height * 0.030882353
        color: "#000000"
        
        font.pixelSize: height * 0.761904762
        text: "사용자ID"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Rectangle{
        id: userIDTextRect
        x: recposition_page.width * 0.483333333
        y: saveBtn.y + recposition_page.height * 0.0742
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#FFFFFF"
        radius: parent.height * 0.55
        border.width: 1
        border.color: "#CFE9F0"
    }

    TextInput {
        id: userIDTextInput
        x: recposition_page.width * 0.55
        y: saveBtn.y + recposition_page.height * 0.0874
        width: recposition_page.width * 0.45
        height: recposition_page.height * 0.058823529
        color: "#000000"
        readOnly: true
        
        MouseArea{
            anchors.fill: parent
            pressAndHoldInterval:3000
            onPressAndHold:{
                /*userIDTextInput.text="";
                if(earlyVoteCB1_checked)
                {
                    humanCountUserInfoData.userid1 = "";
                    humanCountUserInfoData.saveUserInfoV1();
                }
                else if(earlyVoteCB2_checked)
                {
                    humanCountUserInfoData.userid2 = "";
                    humanCountUserInfoData.saveUserInfoV2();
                }
                else if(electionDayVoteCB_checked)
                {
                    humanCountUserInfoData.userid3 = "";
                    humanCountUserInfoData.saveUserInfoV3();
                }*/
            }
        }
    }

    Rectangle{
        id: checkID
        x: parent.width* 0.066666667
        y: userIDTextRect.y + recposition_page.height * 0.0742
        width: parent.width*0.866666667
        height: parent.height * 0.060588235
        color: checkID_ma.pressed? "#FFFFFF":"#CFE9F0"
        radius: height*0.458333333
        border.color: "#CFE9F0"
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter 
            width: parent.width* 0.487179487
            height: parent.height * 0.4375
            text: qsTr("확인")
            font.family: "Roboto"
            font.pixelSize: height* 0.761904762
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: checkID_ma
            anchors.fill: parent
            onClicked: {
                //string str;
                //votingMonitoringQmlUtils.getLastNotification();
                main_window.clickedHomeTab();
                //console.log(str);
            }
        }
    }

    Rectangle{
        x: parent.width* 0.066666667
        y: checkID.y + recposition_page.height * 0.0742
        width: parent.width*0.866666667
        height: parent.height * 0.060588235
        color: deleteID_ma.pressed? "#FFFFFF":userIDTextInput.text==""?"#AAAAAA":saveBtnColor
        enabled: userIDTextInput.text==""?false:true
        radius: height*0.458333333
        border.color: "#CFE9F0"
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter 
            width: parent.width* 0.487179487
            height: parent.height * 0.4375
            text: qsTr("등록 취소")
            font.family: "Roboto"
            font.pixelSize: height* 0.761904762
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: deleteID_ma
            anchors.fill: parent
            onClicked: {
                var vi = votingPlaceComboBox.currentIndex;
                var placeCode = votingPlaceComboBox_listmodel.get(vi).placecode;
                var userPhone  = phoneNumberTextInput.text;

                deleteUser(election_code,placeCode,userPhone);
            }
        }
    }

    Component.onCompleted:{
        loadVoteInfo(function(){
            loadProvinceList(function(){
                loadVotingPlcaeInfo()});
        });
    }

    function deleteUser(electionCode, placeCode,hpno)
    {
        var params = "electioncode="+electionCode+"&placecode="+placeCode+"&hpno="+hpno;
        http_request("http://175.118.126.139/vote/api/auditorcancel.php",params, function (o) {
            if (o.status === 200)
            {
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");
                var jsn = JSON.parse(jsonString);

                // 성공
                if(jsn.result_code == 1000)
                {
                    userIDTextInput.text="";
                    if(earlyVoteCB1_checked)
                    {
                        humanCountUserInfoData.userid1 = "";
                        humanCountUserInfoData.saveUserInfoV1();

                        humanCountUserInfoData.manualCountDay1 = 0;
                        humanCountUserInfoData.autoCountDay1 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }
                    else if(earlyVoteCB2_checked)
                    {
                        humanCountUserInfoData.userid2 = "";
                        humanCountUserInfoData.saveUserInfoV2();

                        humanCountUserInfoData.manualCountDay2 = 0;
                        humanCountUserInfoData.autoCountDay2 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }
                    else if(electionDayVoteCB_checked)
                    {
                        humanCountUserInfoData.userid3 = "";
                        humanCountUserInfoData.saveUserInfoV3();

                        humanCountUserInfoData.manualCountDay3 = 0;
                        humanCountUserInfoData.autoCountDay3 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }

                    main_window.openMessagePopup("성공적으로 사용자를 삭제 했습니다");
                }
                else // 실패
                {
                    main_window.openMessagePopup("사용자 삭제를 실패했습니다. 사유:"+jsn.result_msg+"\n 앱 내부 저장된 아이디는 삭제됩니다.");
                    userIDTextInput.text="";
                    if(earlyVoteCB1_checked)
                    {
                        humanCountUserInfoData.userid1 = "";
                        humanCountUserInfoData.saveUserInfoV1();

                        humanCountUserInfoData.manualCountDay1 = 0;
                        humanCountUserInfoData.autoCountDay1 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }
                    else if(earlyVoteCB2_checked)
                    {
                        humanCountUserInfoData.userid2 = "";
                        humanCountUserInfoData.saveUserInfoV2();

                        humanCountUserInfoData.manualCountDay2 = 0;
                        humanCountUserInfoData.autoCountDay2 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }
                    else if(electionDayVoteCB_checked)
                    {
                        humanCountUserInfoData.userid3 = "";
                        humanCountUserInfoData.saveUserInfoV3();

                        humanCountUserInfoData.manualCountDay3 = 0;
                        humanCountUserInfoData.autoCountDay3 = 0;
                        humanCountUserInfoData.saveCountInfoDay();
                    }
                }
            }
            else
            {
                main_window.openMessagePopup("사용자 정보 삭제를 실패했습니다. 사유:"+o.status);
                 
            }
        });
    }

    function setCurrentVoteInfoToAuditorData()
    {
        // 1. copy current place code
        // 2. copy current election code

        var p_code = votingPlaceComboBox_listmodel.get(votingPlaceComboBox.currentIndex).placecode;
        var nas_id = votingPlaceComboBox_listmodel.get(votingPlaceComboBox.currentIndex).nas_id;
        humanAuditorData.electionCode = election_code;
        if(p_code != null)
            humanAuditorData.placeCode = p_code;
        else
            humanAuditorData.placeCode = "pcode error!!";
            
        var coord = my_pos_src.position.coordinate;
        humanAuditorData.gpsx = coord.latitude;
        humanAuditorData.gpsy = coord.longitude;
        humanAuditorData.nasID = nas_id;

        if(earlyVoteCB1_checked)
        {
            humanCountUserInfoData.lastInsertedVtype = "사전1";
            humanCountUserInfoData.saveUserInfoV1();
        }
        else if(earlyVoteCB2_checked)
        {
            humanCountUserInfoData.lastInsertedVtype = "사전2";
            humanCountUserInfoData.saveUserInfoV2();
        }
        else if(electionDayVoteCB_checked)
        {
            humanCountUserInfoData.lastInsertedVtype = "선거일";
            humanCountUserInfoData.saveUserInfoV3();
        }

    }

    function loadVotingPlaceList(param,next_call_back)
    {
        console.log(param)
        http_request("http://175.118.126.139/vote/api/votingplace.php",param, function (o) {
            if (o.status === 200)
            {
                console.log(o.responseText);
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");

                var jsn = JSON.parse(jsonString);
                votingPlaceComboBox_listmodel.clear();
                for(var i in jsn)
                {
                    //console.log(jsn[i].latitude);
                    //console.log(jsn[i].longitude);
                    //console.log(jsn[i].placename);
                    votingPlaceComboBox_listmodel.append({
                                       latitude: jsn[i].latitude, 
                                       longitude: jsn[i].longitude,
                                       placename: jsn[i].placename,
                                       placecode: jsn[i].placecode, 
                                       amcapa: jsn[i].amcapa,
                                       pmcapa: jsn[i].pmcapa,
                                       amcnt: jsn[i].amcnt,
                                       pmcnt: jsn[i].pmcnt,
                                       nas_id: jsn[i].nas_id});
                    
                    //console.log("NAS ID: "+jsn[i].nas_id);
                }

                if(next_call_back != null)
                    next_call_back();
            }
            else
            {
                console.log("Some error has occurred");
            }
        });
    }

    function loadProvinceTownList(param,next_call_back)
    {
        //console.log(param)
        http_request("http://175.118.126.139/vote/api/provincetown.php",param, function (o) {
            if (o.status === 200)
            {
                //console.log(o.responseText);
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");

                var jsn = JSON.parse(jsonString);
                provinceTownComboBox_listmodel.clear();
                votingPlaceComboBox_listmodel.clear();
                for(var i in jsn)
                {
                    //console.log(jsn[i].provincecode);
                    //console.log(jsn[i].commitee);
                    
                    
                    provinceTownComboBox_listmodel.append({
                                       provincecode: jsn[i].provincecode, 
                                       vtype: jsn[i].vtype,
                                       commitee: jsn[i].commitee});
                }

                if(next_call_back != null)
                    next_call_back();
            }
            else
            {
                console.log("Some error has occurred");
            }
        });
    }
    function loadVoteInfo(next_call_back){
        http_request("http://175.118.126.139/vote/api/election.php","", function (o) {
            if (o.status === 200)
            {
                //console.log(o.responseText);
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");

                var jsn = JSON.parse(jsonString);
                for(var i in jsn)
                {
                    //election_code = jsn[i].electioncode.toString();
                    //election_name = jsn[i].electionname.toString();
                    //election_date = jsn[i].electionymd.toString();
                    electionComboBox_listmodel.append({
                                       electioncode: jsn[i].electioncode, 
                                       electionname: jsn[i].electionname,
                                       electionymd: jsn[i].electionymd});

                    console.log(election_code);
                    console.log(election_name);
                    console.log(election_date);
                }

                if(next_call_back != null)
                    next_call_back();
            }
            else
            {
                console.log("Some error has occurred");
            }
        });
    }

    function loadProvinceList(next_call_back){
        http_request("http://175.118.126.139/vote/api/province.php","", function (o) {
            if (o.status === 200)
            {
                //console.log(o.responseText);

                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");

                var jsn = JSON.parse(jsonString);
                provinceComboBox_listmodel.clear();
                provinceTownComboBox_listmodel.clear();
                votingPlaceComboBox_listmodel.clear();
                //province_code_list=[];
                
                for(var i in jsn)
                {
                    //console.log(i + ": " + jsn[i].provincename);
                    provinceComboBox_listmodel.append({
                                       provincename: jsn[i].provincename.toString(), 
                                       provincecode: jsn[i].provincecode});
                }

                if(next_call_back != null)
                    next_call_back();
            }
            else
            {
                //console.log("Some error has occurred");
            }
        });

        
        //for(var i in jsn)
        //{
        //    console.log(i + ": " + jsn[i]);
        //}
    }

    function regUserInfoToServer(param){
        console.log(param)
        http_request("http://175.118.126.139/vote/api/auditor.php",param, function (o) {
            if (o.status === 200)
            {
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");
                var jsn = JSON.parse(jsonString);

                // 성공
                if(jsn.result_code == 1000)
                {
                    //console.log(jsn.result_msg);
                    main_window.openMessagePopup("성공적으로 등록했습니다");
                    saveBtnColor="#BFD9E0";
                    idCheckTimer.start();
                    waitingApproval = true;
                }
                else // 실패
                {
                    //console.log(jsn.result_msg);
                    if( nameTextInput.text == "구글테스트" && phoneNumberTextInput.text == "01099999999")
                    {
                        idCheckTimer.start();
                        waitingApproval = true;
                    }
                    else
                    {
                        main_window.openMessagePopup("등록에 실패했습니다. 사유:"+jsn.result_msg);
                    }
                    
                }
            }
            else
            {
                //console.log("Some error has occurred");
                main_window.openMessagePopup("등록에 실패했습니다. 인터넷 연결이 좋지 않습니다.");
            }
        });
    }

    function http_request(url,param, callback) {
        var xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                if(myxhr.readyState === 4) { callback(myxhr); }
            }
        })(xhr);

        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send(param);
    }

    function loadKakaoMap(office_lat, office_lng,my_lat,my_lng){
        //console.log(office_lat);
        //console.log(office_lng);
        
        mapInfoPopup.openMapPopup(office_lat, office_lng,my_lat,my_lng);
        //html_data = html_data.toString();
        //html_data = html_data.replaceAll("##officeLat",office_lat);
        //html_data = html_data.replaceAll("##officeLng",office_lng);
        //html_data = html_data.replaceAll("##myLat",office_lat);
        //html_data = html_data.replaceAll("##myLng",office_lng);
        //console.log(html_data);
        //mapview.loadHtml(html_data);
    }
   
    function updateUserIDinLastVotingPlaceInfo(userId){
        humanCountUserInfoData.loadUserInfoV1();
        humanCountUserInfoData.loadUserInfoV2();
        humanCountUserInfoData.loadUserInfoV3();

        if(humanCountUserInfoData.vtype1 ==humanCountUserInfoData.lastInsertedVtype)
        {
            humanCountUserInfoData.userid1 = userId;
            humanCountUserInfoData.saveUserInfoV1();
        }
        else if(humanCountUserInfoData.vtype2 ==humanCountUserInfoData.lastInsertedVtype)
        {
            humanCountUserInfoData.userid2 = userId;
            humanCountUserInfoData.saveUserInfoV2();
        }
        else if(humanCountUserInfoData.vtype3 ==humanCountUserInfoData.lastInsertedVtype)
        {
            humanCountUserInfoData.userid3 = userId;
            humanCountUserInfoData.saveUserInfoV3();
        }
    }

    function updateUserIDWithVoteType(userId,vtype){
        humanCountUserInfoData.loadUserInfoV1();
        humanCountUserInfoData.loadUserInfoV2();
        humanCountUserInfoData.loadUserInfoV3();

        if(humanCountUserInfoData.vtype1 ==vtype)
        {
            humanCountUserInfoData.userid1 = userId;
            humanCountUserInfoData.saveUserInfoV1();
        }
        else if(humanCountUserInfoData.vtype2 ==vtype)
        {
            humanCountUserInfoData.userid2 = userId;
            humanCountUserInfoData.saveUserInfoV2();
        }
        else if(humanCountUserInfoData.vtype3 ==vtype)
        {
            humanCountUserInfoData.userid3 = userId;
            humanCountUserInfoData.saveUserInfoV3();
        }
    }
    
    Timer{
        id: idCheckTimer
        running: false
        repeat: true
        interval: 1000
        onTriggered: {
            var userPhone  = phoneNumberTextInput.text;
            var param = "electioncode=" + election_code +
                        "&vtype=" + election_vtype +
                        "&hpno=" + userPhone;
            console.log(param);
            http_request("http://175.118.126.139/vote/api/auditorapproval.php",param, function (o) {
                if (o.status === 200)
                {
                    var jsonString = o.responseText.toString();
                    jsonString = jsonString.replace(/^\uFEFF/gm, "");
                    var jsn = JSON.parse(jsonString);

                    console.log(jsn[0].confirmyn);

                    // 성공
                    if(jsn[0].confirmyn == 'Y')
                    {
                        console.log(jsn[0].auditorid);
                        console.log(jsn[0].passwd);
                        updateUserIDWithVoteType(jsn[0].auditorid,election_vtype);
                        setUserID(jsn[0].auditorid);
                        main_window.openMessagePopup("사용자 요청 승인이 완료되었습니다");
                        //if(RunPlatform != "ANDROID")
                        idCheckTimer.stop();
                        waitingApproval = false;
                    }
                }
                else
                {
                    console.log("bad request!!!");
                }
            });

        }
    }
    /*
    function insertVotingPlcaeInfo(proCode,proName,commText,votingText,vtype,
                        userName,userPhone,inOut,ampmValue){
        var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
        db.transaction(
            function(tx) {
                // Create the database if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS setting_info2(\
                                        provincecode TEXT, \
                                        provincename TEXT, \
                                        commitee TEXT, \
                                        placename TEXT, \
                                        vtype TEXT, \
                                        username TEXT, \
                                        userphone TEXT, \
                                        inout TEXT, \
                                        ampm INTEGER,\
                                        userid TEXT)');

                // Add (another) greeting row
                tx.executeSql('INSERT INTO setting_info2 VALUES(?,?,?,?,?,?,?,?,?,?)', 
                [ proCode,proName,commText,votingText,vtype,userName,userPhone,inOut,ampmValue,'']);

                // Show all added greetings
                var rs = tx.executeSql('SELECT * FROM setting_info2');

                var r = ""
                for (var i = 0; i < rs.rows.length; i++) {
                    r = rs.rows.item(i).provincecode + ", " + 
                        rs.rows.item(i).provincename + ", " + 
                        rs.rows.item(i).commitee + ", " + 
                        rs.rows.item(i).placename + ", " + 
                        rs.rows.item(i).vtype+ ", " + 
                        rs.rows.item(i).username+ ", " + 
                        rs.rows.item(i).userphone+ ", " + 
                        rs.rows.item(i).inout+ ", " + 
                        rs.rows.item(i).ampm+ ", " + 
                        rs.rows.item(i).userid;
                    console.log(r);
                }
            }
        )
    }*/

    function insertVotingPlcaeInfo(proCode,proName,commText,votingText,vtype,
                        userName,userPhone,inOut,ampmValue){
        console.log("vtype:"+vtype);
        if(vtype == "사전1")
        {
            humanCountUserInfoData.electioncode1 = election_code;
            humanCountUserInfoData.provincecode1 = proCode;
            humanCountUserInfoData.provincename1 = proName;
            humanCountUserInfoData.commitee1     = commText;
            humanCountUserInfoData.placename1    = votingText;
            humanCountUserInfoData.vtype1        = vtype;
            humanCountUserInfoData.username1     = userName;
            humanCountUserInfoData.userphone1    = userPhone;
            humanCountUserInfoData.inout1        = inOut;
            humanCountUserInfoData.ampm1         = ampmValue;
            humanCountUserInfoData.lastInsertedVtype = vtype;
            humanCountUserInfoData.saveUserInfoV1();
            console.log("saveUserInfoV1();");
        }
        else if(vtype =="사전2")
        {
            humanCountUserInfoData.electioncode2 = election_code;
            humanCountUserInfoData.provincecode2 = proCode;
            humanCountUserInfoData.provincename2 = proName;
            humanCountUserInfoData.commitee2     = commText;
            humanCountUserInfoData.placename2    = votingText;
            humanCountUserInfoData.vtype2        = vtype;
            humanCountUserInfoData.username2     = userName;
            humanCountUserInfoData.userphone2    = userPhone;
            humanCountUserInfoData.inout2        = inOut;
            humanCountUserInfoData.ampm2         = ampmValue
            humanCountUserInfoData.lastInsertedVtype = vtype;
            humanCountUserInfoData.saveUserInfoV2();
            console.log("saveUserInfoV2();");
        }
        else if(vtype =="선거일")
        {
            humanCountUserInfoData.electioncode3 = election_code;
            humanCountUserInfoData.provincecode3 = proCode;
            humanCountUserInfoData.provincename3 = proName;
            humanCountUserInfoData.commitee3     = commText;
            humanCountUserInfoData.placename3    = votingText;
            humanCountUserInfoData.vtype3        = vtype;
            humanCountUserInfoData.username3     = userName;
            humanCountUserInfoData.userphone3    = userPhone;
            humanCountUserInfoData.inout3        = inOut;
            humanCountUserInfoData.ampm3         = ampmValue
            humanCountUserInfoData.lastInsertedVtype = vtype;
            humanCountUserInfoData.saveUserInfoV3();
            console.log("saveUserInfoV3();");
        }
        else
        {
            return;
        }
    }
/*
    function loadVotingPlcaeInfo(find_vtype){
        var db = LocalStorage.openDatabaseSync("VideoRecordingDB3","1.0","Video Recording DB in QML",100000);
        db.transaction(
            function(tx) {
                // Create the database if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS setting_info2(\
                                        provincecode TEXT, \
                                        provincename TEXT, \
                                        commitee TEXT, \
                                        placename TEXT, \
                                        vtype TEXT, \
                                        username TEXT, \
                                        userphone TEXT, \
                                        inout TEXT, \
                                        ampm INTEGER,\
                                        userid TEXT)');

                // Show all added greetings
                var rs;
                if(find_vtype != null){
                    rs = tx.executeSql('SELECT * FROM setting_info2 WHERE vtype = ? ORDER BY rowid DESC LIMIT 1'
                                        ,[find_vtype]);
                }
                else
                {
                    rs = tx.executeSql('SELECT * FROM setting_info2 ORDER BY rowid DESC LIMIT 1');
                    //,
                    //                    '');
                }

                if(rs.rows.length == 0)
                {
                    //nameTextInput.text="";
                    //phoneNumberTextInput.text="";
                    userIDTextInput.text="";
                }
                var r = ""
                for (var i = 0; i < rs.rows.length; i++) {
                    r = rs.rows.item(i).provincecode + ", " + 
                        rs.rows.item(i).provincename + ", " + 
                        rs.rows.item(i).commitee + ", " + 
                        rs.rows.item(i).placename + ", " + 
                        rs.rows.item(i).vtype+ ", " + 
                        rs.rows.item(i).username+ ", " + 
                        rs.rows.item(i).userphone+ ", " + 
                        rs.rows.item(i).inout+ ", " + 
                        rs.rows.item(i).ampm+ ", " + 
                        rs.rows.item(i).userid;
                    console.log(r);

                    // 1. check vtype
                    if(rs.rows.item(i).vtype == "선거일")
                    {
                        earlyVoteCB1_checked = false;
                        earlyVoteCB2_checked = false;
                        electionDayVoteCB_checked=true;
                        election_vtype = "선거일";
                    }
                    else if(rs.rows.item(i).vtype == "사전1")
                    {
                        earlyVoteCB1_checked = true;
                        earlyVoteCB2_checked = false;
                        electionDayVoteCB_checked=false;
                        election_vtype = "사전1";
                    }
                    else if(rs.rows.item(i).vtype == "사전2")
                    {
                        earlyVoteCB1_checked = false;
                        earlyVoteCB2_checked = true;
                        electionDayVoteCB_checked=false;
                        election_vtype = "사전2";
                    }

                    // 2. find province code
                    for(var j=0; j< provinceComboBox.count; j++)
                    {
                        if(provinceComboBox_listmodel.get(j).provincecode 
                            == rs.rows.item(i).provincecode)
                        {
                            provinceComboBox.currentIndex=j;
                            console.log("province found!!");

                            var p_code = provinceComboBox_listmodel.get(j).provincecode;
                            
                            loadProvinceTownList("electioncode="+
                            election_code+"&provincecode="+p_code
                            +"&vtype="+election_vtype,
                            function(){
                                for(var k=0; k< commiteeComboBox.count; k++)
                                {
                                    if(provinceTownComboBox_listmodel.get(k).commitee 
                                        == rs.rows.item(i).commitee)
                                    {
                                        commiteeComboBox.currentIndex=k;
                                        console.log("commitee found!!");

                                        var p_code = provinceTownComboBox_listmodel.get(k).provincecode;
                                        var commitee_name = provinceTownComboBox_listmodel.get(k).commitee;
                                        loadVotingPlaceList("electioncode="+election_code+
                                                            "&provincecode="+p_code+
                                                            "&commitee="+commitee_name+
                                                            "&vtype="+election_vtype,
                                                            function(){
                                                                for(var t=0; t< votingPlaceComboBox.count; t++)
                                                                {
                                                                    if(votingPlaceComboBox_listmodel.get(t).placename
                                                                    == rs.rows.item(i).placename)
                                                                    {
                                                                        votingPlaceComboBox.currentIndex=t;
                                                                        console.log("votingplace found!!");
                                                                        selected_office_lat = votingPlaceComboBox_listmodel.get(t).latitude;
                                                                        selected_office_lng = votingPlaceComboBox_listmodel.get(t).longitude;
                                                                        
                                                                        if(rs.rows.item(i).inout == "1")
                                                                        {
                                                                            votingInCB_checked = true;
                                                                            votingOutCB_checked = false;
                                                                        } //   inoutComboBox.currentIndex = 0;
                                                                        else if(rs.rows.item(i).inout == "2")
                                                                        {
                                                                            votingInCB_checked = false;
                                                                            votingOutCB_checked = true;
                                                                        }
                                                                        else if(rs.rows.item(i).inout == "3")
                                                                        {
                                                                            votingInCB_checked = true;
                                                                            votingOutCB_checked = true;
                                                                        }
                                                                        //inoutComboBox.currentIndex = 1;

                                                                        votingAMCB.enabled=true;
                                                                        votingPMCB.enabled=true;
                                                                        votingAMCBText.text="오전 ("+votingPlaceComboBox_listmodel.get(t).amcnt+"/"+ 
                                                                           votingPlaceComboBox_listmodel.get(t).amcapa+ ")";
                                                                        votingPMCBText.text="오후 ("+votingPlaceComboBox_listmodel.get(t).pmcnt+"/"+
                                                                           votingPlaceComboBox_listmodel.get(t).pmcapa+ ")";
                                                                        if(rs.rows.item(i).ampm == 1)
                                                                        {
                                                                            votingAMCB_checked=true;
                                                                            votingPMCB_checked=false;
                                                                        }
                                                                        else if(rs.rows.item(i).ampm == 2)
                                                                        {
                                                                            votingAMCB_checked=false;
                                                                            votingPMCB_checked=true;
                                                                        }
                                                                        else if(rs.rows.item(i).ampm == 3)
                                                                        {
                                                                            votingAMCB_checked=true;
                                                                            votingPMCB_checked=true;
                                                                        }

                                                                        phoneNumberTextInput.text = rs.rows.item(i).userphone;
                                                                        nameTextInput.text        = rs.rows.item(i).username;
                                                                        if(rs.rows.item(i).userid == null)
                                                                        {
                                                                            checkUserApproval();
                                                                        }
                                                                        else
                                                                        {
                                                                            setUserID(rs.rows.item(i).userid);
                                                                        }
                                                                        
                                                                        
                                                                        break;
                                                                    }

                                                                }

                                                            });

                                    }
                                }

                            });
                            
                            break;
                        }
                    }

                    if(provinceComboBox.currentIndex < 0)
                        break;

                    

                    break;
                }
            }
        )
    }*/

    function loadVotingPlcaeInfo(find_vtype){
        setUserID("");

        var electionCode;
        var proCode;
        var proName;
        var commText;
        var votingText;
        var vtype;
        var userName;
        var userPhone;
        var inOut;
        var ampmValue;
        var userId;

        humanCountUserInfoData.loadUserInfoV1();
        humanCountUserInfoData.loadUserInfoV2();
        humanCountUserInfoData.loadUserInfoV3();

        console.log(humanCountUserInfoData.electioncode1)
        console.log(humanCountUserInfoData.provincecode1)
        console.log(humanCountUserInfoData.provincename1)
        console.log(humanCountUserInfoData.commitee1)
        console.log(humanCountUserInfoData.placename1)
        console.log(humanCountUserInfoData.vtype1)
        console.log(humanCountUserInfoData.username1)
        console.log(humanCountUserInfoData.userphone1)
        console.log(humanCountUserInfoData.inout1)
        console.log(humanCountUserInfoData.ampm1)
        console.log(humanCountUserInfoData.userid1)
        console.log(humanCountUserInfoData.lastInsertedVtype)
        if((find_vtype =="사전1") || 
        ((find_vtype==null)&&(humanCountUserInfoData.vtype1==humanCountUserInfoData.lastInsertedVtype)))
        {
            electionCode = humanCountUserInfoData.electioncode1;
            proCode    = humanCountUserInfoData.provincecode1;
            proName    = humanCountUserInfoData.provincename1;
            commText   = humanCountUserInfoData.commitee1;
            votingText = humanCountUserInfoData.placename1;
            vtype      = humanCountUserInfoData.vtype1;
            userName   = humanCountUserInfoData.username1;
            userPhone  = humanCountUserInfoData.userphone1;
            inOut      = humanCountUserInfoData.inout1;
            ampmValue  = humanCountUserInfoData.ampm1;
            userId     = humanCountUserInfoData.userid1;
            
        }
        else if((find_vtype =="사전2") || 
        ((find_vtype==null)&&(humanCountUserInfoData.vtype2==humanCountUserInfoData.lastInsertedVtype)))
        {
            electionCode = humanCountUserInfoData.electioncode2;
            proCode    = humanCountUserInfoData.provincecode2;
            proName    = humanCountUserInfoData.provincename2;
            commText   = humanCountUserInfoData.commitee2;
            votingText = humanCountUserInfoData.placename2;
            vtype      = humanCountUserInfoData.vtype2;
            userName   = humanCountUserInfoData.username2;
            userPhone  = humanCountUserInfoData.userphone2;
            inOut      = humanCountUserInfoData.inout2;
            ampmValue  = humanCountUserInfoData.ampm2;
            userId     = humanCountUserInfoData.userid2;
        }
        else if((find_vtype =="선거일") || 
        ((find_vtype==null)&&(humanCountUserInfoData.vtype3==humanCountUserInfoData.lastInsertedVtype)))
        {
            electionCode = humanCountUserInfoData.electioncode3;
            proCode    = humanCountUserInfoData.provincecode3;
            proName    = humanCountUserInfoData.provincename3;
            commText   = humanCountUserInfoData.commitee3;
            votingText = humanCountUserInfoData.placename3;
            vtype      = humanCountUserInfoData.vtype3;
            userName   = humanCountUserInfoData.username3;
            userPhone  = humanCountUserInfoData.userphone3;
            inOut      = humanCountUserInfoData.inout3;
            ampmValue  = humanCountUserInfoData.ampm3;
            userId     = humanCountUserInfoData.userid3;
        }
        else
        {
            return;
        }

        // 1. 선거 종류 선택
        var foundIdx = -1;
        for(var m=0; m< electionComboBox.count; m++)
        {
            if(electionComboBox_listmodel.get(m).electioncode == electionCode)
            {
                electionComboBox.currentIndex = m;
                election_code = electionComboBox_listmodel.get(m).electioncode;
                election_name = electionComboBox_listmodel.get(m).electionname;
                election_date = electionComboBox_listmodel.get(m).electionymd;
                foundIdx = m;
                break;
            }
        }

        if(foundIdx == -1)
        {
            electionComboBox.currentIndex = 0;
            console.log("Election Code not Found!!!");
            return;
        }



        // 2. 선거일 투표 타입 선택
        if(vtype == "선거일")
        {
            earlyVoteCB1_checked = false;
            earlyVoteCB2_checked = false;
            electionDayVoteCB_checked=true;
            election_vtype = "선거일";
        }
        else if(vtype == "사전1")
        {
            earlyVoteCB1_checked = true;
            earlyVoteCB2_checked = false;
            electionDayVoteCB_checked=false;
            election_vtype = "사전1";
        }
        else if(vtype == "사전2")
        {
            earlyVoteCB1_checked = false;
            earlyVoteCB2_checked = true;
            electionDayVoteCB_checked=false;
            election_vtype = "사전2";
        }

        for(var j=0; j< provinceComboBox.count; j++)
        {
            if(provinceComboBox_listmodel.get(j).provincecode 
                == proCode)
            {
                provinceComboBox.currentIndex=j;
                console.log("province found!!");

                var p_code = provinceComboBox_listmodel.get(j).provincecode;
                console.log("electioncode="+election_code+"&provincecode="+p_code+"&vtype="+election_vtype);       
                loadProvinceTownList("electioncode="+
                    election_code+"&provincecode="+p_code
                    +"&vtype="+election_vtype,
                    function(){
                    for(var k=0; k< commiteeComboBox.count; k++)
                    {
                        if(provinceTownComboBox_listmodel.get(k).commitee 
                                == commText)
                        {
                            commiteeComboBox.currentIndex=k;
                            console.log("commitee found!!");
                            var p_code = provinceTownComboBox_listmodel.get(k).provincecode;
                            var commitee_name = provinceTownComboBox_listmodel.get(k).commitee;
                            loadVotingPlaceList("electioncode="+election_code+
                                "&provincecode="+p_code+
                                "&commitee="+commitee_name+
                                "&vtype="+election_vtype,
                                function(){
                        
                                for(var t=0; t< votingPlaceComboBox.count; t++)
                                {
                                    if(votingPlaceComboBox_listmodel.get(t).placename
                                        == votingText)
                                    {
                                        votingPlaceComboBox.currentIndex=t;
                                        console.log("votingplace found!!");
                                        selected_office_lat = votingPlaceComboBox_listmodel.get(t).latitude;
                                        selected_office_lng = votingPlaceComboBox_listmodel.get(t).longitude;
                                                                        
                                        if(inOut == "1")
                                        {
                                            votingInCB_checked = true;
                                            votingOutCB_checked = false;
                                        } //   inoutComboBox.currentIndex = 0;
                                        else if(inOut == "2")
                                        {
                                            votingInCB_checked = false;
                                            votingOutCB_checked = true;
                                        }
                                        else if(inOut == "3")
                                        {
                                            votingInCB_checked = true;
                                            votingOutCB_checked = true;
                                        }
                                                //inoutComboBox.currentIndex = 1;

                                        votingAMCB.enabled=true;
                                        votingPMCB.enabled=true;
                                        votingAMCBText.text="오전 ("+votingPlaceComboBox_listmodel.get(t).amcnt+"/"+ 
                                        votingPlaceComboBox_listmodel.get(t).amcapa+ ")";
                                        votingPMCBText.text="오후 ("+votingPlaceComboBox_listmodel.get(t).pmcnt+"/"+
                                        votingPlaceComboBox_listmodel.get(t).pmcapa+ ")";
                                        if(ampmValue == 1)
                                        {
                                            votingAMCB_checked=true;
                                            votingPMCB_checked=false;
                                        }
                                        else if(ampmValue == 2)
                                        {
                                            votingAMCB_checked=false;
                                            votingPMCB_checked=true;
                                        }
                                        else if(ampmValue == 3)
                                        {
                                            votingAMCB_checked=true;
                                            votingPMCB_checked=true;
                                        }

                                        phoneNumberTextInput.text = userPhone;
                                        nameTextInput.text        = userName;
                                        if(userId == "")
                                        {
                                            checkUserApproval();
                                        }
                                        else
                                        {
                                            setUserID(userId);
                                        }
                                                                            
                                                                        
                                        break;
                                    }
                                }
                            });

                        }
                    }

                });
                            
                break;
            }
        }
    }

    function setUserID(id){
        userIDTextInput.text=id;
    }

    function getUserID(){
        return userIDTextInput.text;
    }

    function getCurrentVtype(){
        if(earlyVoteCB1_checked)
        {
            return "사전1";
        }    
        else if(earlyVoteCB2_checked)
        {
            return "사전2";
        }
        
        return "선거일";
    }

    function insertTestInfo() {
        var db = LocalStorage.openDatabaseSync("VideoRecordingDB3", "1.0",
                                               "Video Recording DB in QML",
                                               100000)
        db.transaction(function (tx) {
            // Create the database if it doesn't already exist
            tx.executeSql(
                        'CREATE TABLE IF NOT EXISTS video_history2(name TEXT, path TEXT, datetime TEXT, nas_id TEXT, status TEXT)')

            // Add (another) greeting row
            var videoName = "video9.mp4"
            var videoPath = "/home/john/video9.mp4"
            var startTime = "20220522003500"
            var nas_id = "1"
            var status = "finish record"
            tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                          [videoName, videoPath, startTime, nas_id,status])

            var videoName = "video10.mp4"
            var videoPath = "/home/john/video10.mp4"
            var startTime = "20220522003500"
            var nas_id = "2"
            var status = "finish record"
            tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                          [videoName, videoPath, startTime, nas_id,status])

            var videoName = "video11.mp4"
            var videoPath = "/home/john/video11.mp4"
            var startTime = "20220522003500"
            var nas_id = "3"
            var status = "finish record"
            tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                          [videoName, videoPath, startTime, nas_id,status])

            var videoName = "video12.mp4"
            var videoPath = "/home/john/video12.mp4"
            var startTime = "20220522003500"
            var nas_id = "4"
            var status = "finish record"
            tx.executeSql('INSERT INTO video_history2 VALUES(?, ?,?,?,?)',
                          [videoName, videoPath, startTime, nas_id,status])

            // Show all added greetings
            var rs = tx.executeSql('SELECT * FROM video_history2')

            var r = ""
            for (var i = 0; i < rs.rows.length; i++) {
                r = rs.rows.item(i).name + ", " + rs.rows.item(
                            i).path + ", " + rs.rows.item(
                            i).datetime + ", " + rs.rows.item(i).nas_id + ", " + rs.rows.item(i).status
                console.log(r)
            }
        })

    }

    function changeInOutInfo()
    {
        var locateType;
        if(votingInCB_checked && votingOutCB_checked)
            locateType="3";
        else if(votingOutCB_checked && !votingInCB_checked)
            locateType="2";
        else
            locateType="1";
        

        var param = "auditorid="+userIDTextInput.text + "&locatetype=" +locateType;
        http_request("http://175.118.126.139/vote/api/auditormod.php",param, function (o) {
            if (o.status === 200)
            {
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");
                var jsn = JSON.parse(jsonString);

                // 성공
                if(jsn.result_code == 1000)
                {
                    //console.log(jsn.result_msg);
                    main_window.openMessagePopup("성공적으로 수정 했습니다");
                    saveBtnColor="#BFD9E0";
                }
                else // 실패
                {
                    //console.log(jsn.result_msg);
                    main_window.openMessagePopup("등록에 실패했습니다. 사유:"+jsn.result_msg);
                }
            }
            else
            {
                //console.log("Some error has occurred");
                main_window.openMessagePopup("등록에 실패했습니다. 인터넷 연결이 좋지 않습니다.");
            }
        });
    }

    function checkUserApproval(){
        var userPhone  = phoneNumberTextInput.text;
        var param = "electioncode=" + election_code +
                    "&vtype=" + election_vtype +
                    "&hpno=" + userPhone;
            console.log(param);
            http_request("http://175.118.126.139/vote/api/auditorapproval.php",param, function (o) {
            if (o.status === 200)
            {
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");
                var jsn = JSON.parse(jsonString);

                console.log(jsn[0].confirmyn);

                // 성공
                if(jsn[0].confirmyn == 'Y')
                {
                    console.log(jsn[0].auditorid);
                    console.log(jsn[0].passwd);
                    //updateUserIDinLastVotingPlaceInfo(jsn[0].auditorid);
                    updateUserIDWithVoteType(jsn[0].auditorid,election_vtype);
                    setUserID(jsn[0].auditorid);
                    idCheckTimer.stop();
                    waitingApproval = false;
                }
            }
            else
            {
                console.log("bad request!!!");
            }
        });

    }


    
}
