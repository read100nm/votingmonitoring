#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDir>
#include <QMediaRecorder>
#include "HumanCountVideoFilter.h"
#include "HumanCountQmlUtils.h"
#include "HumanCountAuditorData.h"
#include "HumanCountUserInfoData.h"
#include "HumanCountFtpAllUploader.h"
#include <QStandardPaths>
#ifndef ANDROID_
#include <QtWebView>
#endif

//#include "firebase/firebaseapp.h"
//#include "firebase/firebasemessaging.h"
//#include "firebase/firebasedatabase.h"

//#include <firebase/app.h>
//#include <firebase/future.h>
//#include <firebase/messaging.h>
//#include <firebase/util.h>
#ifdef ANDROID_
    #include "jnimessenger.h"
    #include "qtandroidservice.h"
#endif


int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("fiftybridge");
    QCoreApplication::setOrganizationDomain("fiftybridge.com");
    QCoreApplication::setApplicationName("촬영특공대");

    //qRegisterMetaType<QVariantList();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

#ifndef ANDROID_
    QtWebView::initialize();
#endif

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("AppDirPath", QString(QDir::currentPath()));
    engine.rootContext()->setContextProperty("MoiveLocationPath", 
    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    engine.rootContext()->setContextProperty("MoiveLocationPath2", 
    QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));

    QObject::connect(&engine, &QQmlApplicationEngine::quit, &QGuiApplication::quit);
    
#ifdef ANDROID_

    engine.rootContext()->setContextProperty("RunPlatform","ANDROID");
#else
    engine.rootContext()->setContextProperty("RunPlatform","OHTERS");
#endif

#ifdef ANDROID_
    JniMessenger *jniMessenger = new JniMessenger(&app);
    engine.rootContext()->setContextProperty(QLatin1String("JniMessenger"), jniMessenger);

    QtAndroidService *qtAndroidService = new QtAndroidService(&app);
    engine.rootContext()->setContextProperty(QLatin1String("qtAndroidService"), qtAndroidService);
#endif

    HumanCountAuditorData* humanAuditorData = new HumanCountAuditorData(&app);
    engine.rootContext()->setContextProperty(QLatin1String("humanAuditorData"), humanAuditorData);

    HumanCountUserInfoData* humanCountUserInfoData = new HumanCountUserInfoData(&app);
        engine.rootContext()->setContextProperty(QLatin1String("humanCountUserInfoData"), humanCountUserInfoData);

    HumanCountFtpAllUploader* humanCountFtpAllUploader = new HumanCountFtpAllUploader();
    engine.rootContext()->setContextProperty(QLatin1String("humanCountFtpAllUploader"), humanCountFtpAllUploader);

    qmlRegisterType< HumanCountVideoFilter >   ("HumanCountLib", 1, 0, "HumanCountVideoFilter" );
    qmlRegisterType< HumanCountQmlUtils >      ("HumanCountLib", 1, 0, "HumanCountQmlUtils" );
    //qmlRegisterType< HumanCountFtpAllUploader >("HumanCountLib", 1, 0, "HumanCountFtpAllUploader" );

    const QUrl url(QStringLiteral("qrc:/HumanCountMain.ui.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
