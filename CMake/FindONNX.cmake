# - Find ONNX
# Find the ONNX includes and library
# This module defines
#  ONNX_INCLUDE_DIRS, where to find OCILIB.h
#  ONNX_LIBRARIES, the libraries needed to use OCILIB.
#  ONNX_FOUND, If false, do not try to use OCILIB.
#
# Hints:
# Set ``ONNX_ROOT_DIR`` to the root directory of an installation.
#

include(FindPackageHandleStandardArgs)

find_path(ONNX_INCLUDE_DIR onnxruntime_cxx_api.h
    HINTS
	  ${ONNX_ROOT_DIR}/include
    PATHS
      /usr/include
      /usr/local/include
      $ENV{ProgramFiles}/ONNX/*/include
      $ENV{SystemDrive}/ONNX/*/include
      ${PROGRAM_FILES_32}/ONNX/*/include
      ${PROGRAM_FILES_64}/ONNX/*/include
)

if(WIN32 AND MSVC)
  find_library(ONNX_LIBRARY NAMES onnxruntime
	HINTS
	  ${ONNX_ROOT_DIR}/lib
    PATHS
      $ENV{ProgramFiles}/OCILIB/*/lib/opt
      $ENV{SystemDrive}/OCILIB/*/lib/opt
      $ENV{SystemDrive}/OCILIB/*/lib
      ${PROGRAM_FILES_32}/OCILIB/*/lib
      ${PROGRAM_FILES_64}/OCILIB/*/lib
      )
else(WIN32 AND MSVC)
  find_library(ONNX_LIBRARY NAMES onnxruntime
      PATHS
      ${ONNX_ROOT_DIR}/lib
      /usr/lib
      /usr/local/lib
      )
endif(WIN32 AND MSVC)

find_package_handle_standard_args(ONNX
  FOUND_VAR ONNX_FOUND
  REQUIRED_VARS
	ONNX_INCLUDE_DIR
	ONNX_LIBRARY
)

if(ONNX_FOUND)
	set(ONNX_LIBRARIES ${ONNX_LIBRARY})
	set(ONNX_INCLUDE_DIRS ${ONNX_INCLUDE_DIR})
endif()

mark_as_advanced(
  ONNX_LIBRARY
  ONNX_INCLUDE_DIR
)
