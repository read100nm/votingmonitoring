#include "HumanCountQmlUtils.h"

#include <qfile.h>
#include <qobject.h>
#include <qdir.h>
#include <qstandardpaths.h>
#include <qsettings.h>

HumanCountQmlUtils::HumanCountQmlUtils(QObject *parent):QObject(parent)
{
}
HumanCountQmlUtils::~HumanCountQmlUtils()
{

}

QString HumanCountQmlUtils::readKakaoMapHtml(QString office_lat, QString office_lng, QString my_lat, QString my_lng)
{

    // read template file
    QString data;
    QString fileName(":/a.htm");

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug()<<"filenot opened";
    }
    else
    {
        qDebug()<<"file opened";
        data = file.readAll();
    }

    file.close();

    data.replace("##officeLat",office_lat);
    data.replace("##officeLng",office_lng);
    data.replace("##myLat",my_lat);
    data.replace("##myLng",my_lng);
    qDebug()<<data;

    // write html file
    QString html_path = QString(QDir::currentPath());
    html_path = html_path+"/userpos.html";
    QFile html_file(html_path);
    if(!html_file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        qDebug()<<"the html file opened "<<html_path;
    }
    else
    {
        qDebug()<<"file opened";
        html_file.write(data.toUtf8());
        qDebug()<<"the html file opened "<<html_path;
    }
    return data;
}

