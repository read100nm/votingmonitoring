import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.12
import QtMultimedia 5.12


Page {
    id: reguser_page
    width: stackView.width
    height: stackView.height

    title: qsTr("촬영자정보등록")

    TextField {
        id: user_id
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.05
        width: reguser_page.width * 0.5
        height: reguser_page.height * 0.07
        placeholderText: qsTr("아이디")
    }

    Button {
        id: find_duplicateBtn
        x: reguser_page.width * 0.65
        y: reguser_page.height * 0.05
        width: reguser_page.width * 0.25
        height: reguser_page.height * 0.07
        contentItem: Text {
            fontSizeMode: Text.Fit
            text: qsTr("중복확인")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        onClicked:{
            
        }
    }

    TextField {
        id: user_pw
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.14
        width: reguser_page.width * 0.8
        height: reguser_page.height * 0.07
        placeholderText: qsTr("비밀번호")
    }

    TextField {
        id: user_pw_confirm
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.23
        width: reguser_page.width * 0.8
        height: reguser_page.height * 0.07
        placeholderText: qsTr("비밀번호 확인")
    }
    
    TextField {
        id: user_name
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.31
        width: reguser_page.width * 0.8
        height: reguser_page.height * 0.07
        placeholderText: qsTr("이름")
    }

    TextField {
        id: user_phone_number
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.40
        width: reguser_page.width * 0.8
        height: reguser_page.height * 0.07
        placeholderText: qsTr("휴대폰번호")
    }
    
    Button {
        id: request_idpwBtn
        x: reguser_page.width * 0.60
        y: reguser_page.height * 0.49
        width: reguser_page.width * 0.3
        height: reguser_page.height * 0.07
        contentItem: Text {
            fontSizeMode: Text.Fit
            text: qsTr("ID/PW 요청")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        onClicked:{
            
        }
    }

    ToolButton {
        x: reguser_page.width * 0.1
        y: reguser_page.height * 0.58
        width: reguser_page.width * 0.8
        height: reguser_page.height * 0.07
        id: manualMinusBtn
        font.pointSize: 20
        background: Rectangle {
            color: "#5555ff"
        }
        palette.buttonText: "white"
        text: qsTr("확인")
        onClicked: {

        }
    }
}
