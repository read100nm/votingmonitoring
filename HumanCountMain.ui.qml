import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.2

ApplicationWindow {
    id: main_window
    width: 360
    height: 736
    visible: true
    title: qsTr("VotingMonitoring")
    property string userId: "--"
    property string userToken: "--"
    property string userVtype:"--"
    property int    currentTabIndex:1
    property bool   clickedBackButton: false
    HumanCountPopup{
        id: infoMsgPopup
    }

    HumanCountPopup{
        id: versionCheckMsgPopup
        onSigClosed:{
            Qt.callLater(Qt.quit);
        }
    }

    HumanCountYesNoPopup{
        id: yesNoPopup
    }

    Timer {
        id: backButtonCheckTimer
        running: false
        repeat: false
        interval: 1000

        onTriggered:{
            clickedBackButton =false;
        }
    }

    function clickedHomeTab(){
        // 자기 탭 체크
        if(currentTabIndex == 0)
            return;

        if(currentTabIndex == 1)
        {
            userId = stackView.currentItem.getUserID();
            userVtype = stackView.currentItem.getCurrentVtype();
        }

        // 자격 체크 
        if((userId == "--") || (userId ==""))
        {
            // message box !!!!
            openMessagePopup("서버에서 아이디를 수신하기전에 영상분석처리를 실행할 수 없습니다.");
            return;
        }

        // 투표소 정보 저장
        if(currentTabIndex == 1)
        {
            stackView.currentItem.setCurrentVoteInfoToAuditorData();

        }    

        // 화면 구성
        if (stackView.depth > 1) {
            stackView.pop()
        }
        stackView.push("HumanCountHome.ui.qml")

        // 탭 구성
        homeBtnIndicator.visible = true;
        recordingPosBtnIndicator.visible = false;
        videoDataBtnIndicator.visible = false;

        homeBtn.font.bold = true;
        recordingPosBtn.font.bold = false;
        videoDataBtn.font.bold = false;
        currentTabIndex=0;
    }

    function clickedRegPosTab(){
        // 자기 탭 체크
        if(currentTabIndex == 1)
            return;

        // Home 화면일 경우, 녹화, 등등 중지된다고 알리고 Yes/No 선택
        // Yes인 경우 이동 
        if(currentTabIndex == 0)
        {
            if(stackView.currentItem.isVideoRecording)
            {
                openMessagePopup("녹화를 중지하고 탭이동을 해주세요");
                return;
            }

            if(stackView.currentItem.isAutoDetecting)
            {
                openMessagePopup("AI 자동 카운팅을 종료하고 탭이동을 해주세요");
                return;
            }
        }

        // Video Data 화면일 업로드가 중지된다고 알리고 Yes/No 선택
        // Yes인 경우 이동
        if(currentTabIndex == 2)
        {
            // message box !!!!
            if(humanAuditorData.isUploading())
            {
                var return_value = openYesNoPopup("진행중인 업로드가 있습니다. 다른탭으로 넘어갈 경우 중단됩니다. 넘어가시겠습니까?",function(){
                    // 화면 구성
                    if (stackView.depth > 1) {
                        stackView.pop()
                    }
                    stackView.push("HumanCountUserSetting.ui.qml")

                    homeBtnIndicator.visible = false;
                    recordingPosBtnIndicator.visible = true;
                    videoDataBtnIndicator.visible = false;

                    homeBtn.font.bold = false;
                    recordingPosBtn.font.bold = true;
                    videoDataBtn.font.bold = false;
                    currentTabIndex = 1;
                }, function(){

                });

                //if(return_value == true)
                //{
                //
                //}
                //else
                //{
                //    return;
                //}
            }
            else
            {
                // 화면 구성
                if (stackView.depth > 1) {
                    stackView.pop()
                }
                stackView.push("HumanCountUserSetting.ui.qml")

                homeBtnIndicator.visible = false;
                recordingPosBtnIndicator.visible = true;
                videoDataBtnIndicator.visible = false;

                homeBtn.font.bold = false;
                recordingPosBtn.font.bold = true;
                videoDataBtn.font.bold = false;
                currentTabIndex = 1;
            }
            
            return;
        }

        
        // 화면 구성
        if (stackView.depth > 1) {
            stackView.pop()
        }
        stackView.push("HumanCountUserSetting.ui.qml")

        homeBtnIndicator.visible = false;
        recordingPosBtnIndicator.visible = true;
        videoDataBtnIndicator.visible = false;

        homeBtn.font.bold = false;
        recordingPosBtn.font.bold = true;
        videoDataBtn.font.bold = false;
        currentTabIndex = 1;
    }

    function clickedVideoDataTab(){
        // 자기 탭 체크
        if(currentTabIndex == 2)
            return;

        // Home 화면일 경우, 녹화, 등등 중지된다고 알리고 Yes/No 선택
        // Yes인 경우 이동 
        if(currentTabIndex == 0)
        {
            if(stackView.currentItem.isVideoRecording)
            {
                openMessagePopup("녹화를 중지하고 탭이동을 해주세요");
                return;
            }

            if(stackView.currentItem.isAutoDetecting)
            {
                openMessagePopup("AI 자동 카운팅을 종료하고 탭이동을 해주세요");
                return;
            }
        }

        // 투표소 정보 저장
        if(currentTabIndex == 1)
            stackView.currentItem.setCurrentVoteInfoToAuditorData();

        //stackView.currentItem.insertTestInfo();
        if (stackView.depth > 1) {
            stackView.pop()
        }
        stackView.push("HumanCountVideoData.ui.qml")
        
        homeBtnIndicator.visible = false;
        recordingPosBtnIndicator.visible = false;
        videoDataBtnIndicator.visible = true;

        homeBtn.font.bold = false;
        recordingPosBtn.font.bold = false;
        videoDataBtn.font.bold = true;
        currentTabIndex = 2;
    }

    // 헤더 부분
    header: toolbar
    footer: versionText
    Item{
        id: versionText
        width: parent.width
        height: 20
        Text {
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            text: "version 50(1.0, 20221121)"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
        }
    }
    
    ToolBar {
        id: toolbar
        anchors.top: parent.top
        width: parent.width
        height: 54
        
        Rectangle{
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            color: "#ffffff"
        }

        Rectangle{
            x: 0
            anchors.bottom: parent.bottom
            width: parent.width
            height: 1
            color: "transparent"
            border.width: 1
            border.color: "#E5E5E5"
            visible: true
        }

        Text {
            id: homeBtn
            x: 0
            y: 0
            width: parent.width * 1.0/3.0
            height: parent.height
            text: qsTr("홈")
            font.bold: false
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 20 
            fontSizeMode: Text.Fit
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    clickedHomeTab();
                }
            }
        }

        Rectangle{
            id: homeBtnIndicator
            x: 0
            anchors.bottom: parent.bottom
            width: parent.width * 1.0/3.0
            height: 2
            color: "transparent"
            border.width: 1
            border.color: "#16B2E0"
            visible: false
        }

        Text {
            id: recordingPosBtn
            x: parent.width * 1.0/3.0
            y: 0
            width: parent.width * 1.0/3.0
            height: parent.height
            text: qsTr("설정")
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 20 
            fontSizeMode: Text.Fit
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    clickedRegPosTab();
                }
            }
        }

        Rectangle{
            id: recordingPosBtnIndicator
            x: parent.width * 1.0/3.0
            anchors.bottom: parent.bottom
            width: parent.width * 1.0/3.0
            height: 2
            color: "transparent"
            border.width: 1
            border.color: "#16B2E0"
            visible: true
        }

        Text {
            id: videoDataBtn
            x: parent.width * 2.0/3.0
            y: 0
            width: parent.width * 1.0/3.0
            height: parent.height
            //contentHeight: toolbar.contentHeight
            text: qsTr("영상Data")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            minimumPixelSize: 2
            font.pixelSize: 20
            fontSizeMode: Text.Fit
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    clickedVideoDataTab();
                }
            }
        }

        Rectangle{
            id: videoDataBtnIndicator
            x: parent.width * 2.0/3.0
            anchors.bottom: parent.bottom
            width: parent.width * 1.0/3.0
            height: 2
            color: "transparent"
            border.width: 1
            border.color: "#16B2E0"
            visible: false
        }
    }

    StackView {
        /*x: 0
        y: main_window.height * 0.13
        width: main_window.width
        height: main_window.height * 0.13*/
        id: stackView
        initialItem: "HumanCountUserSetting.ui.qml"
        //initialItem: "HumanCountHome.ui.qml"
        anchors.fill: parent
    }

    Component.onCompleted: {
        JniMessenger.printFromJava("hihi")
        JniMessenger.setKeyValueInPre("hihihi","kknd");
        console.log(JniMessenger.getKeyValueInPre("hihihi","nonono"));
        qtAndroidService.requestTokenFromFCM()
    }

    Connections {
        target: qtAndroidService
        function onNotificationFromFCM(title, body) {
            if(currentTabIndex == 1)
            {
                /*
                console.log("raw:"+body);
                body=body.replace("아이디 : ","");
                console.log("rmid:"+body);
                body=body.replace("패스워드 : ","");
                console.log("rmpw:"+body);
                const idpw = body.split(" ");
                var output = qsTr("QML received a notification: %1 %2").arg(title).arg(body);
                console.log(idpw[0]);
                userId = idpw[0];
                
                stackView.currentItem.updateUserIDinLastVotingPlaceInfo(idpw[0]);
                stackView.currentItem.setUserID(idpw[0]);
                openMessagePopup("사용자 요청 승인이 완료되었습니다");
                */
            }
            
        }
    }

    Connections {
        target: qtAndroidService
        function onTokenFromFCM(token) {
            var output = qsTr("QML received FCM token: %1").arg(token);
            console.log(output);
            userToken = token;
        }
    }

    onClosing: {
        if(RunPlatform == "ANDROID")
        {
            if(clickedBackButton == false)
            {
                close.accepted = false;
                clickedBackButton = true;
                backButtonCheckTimer.start();
            }
            else
            {
                var return_value = openYesNoPopup("프로그램을 종료하겠습니까?",function(){
                    
                    if(currentTabIndex == 0)
                    {
                        if(stackView.currentItem.isVideoRecording)
                        {
                            stackView.currentItem.stopRecord();
                        }
                    }

                    close.accepted = true;
                    Qt.callLater(Qt.quit);

                }, function(){
                    close.accepted = false;
                });

                close.accepted = false;
            }
            
            console.log("BACK PRESSED");
        }
        else
        {
            close.accepted = true;
        }
    }

    function openMessagePopup(msg){
        infoMsgPopup.openMessagePopup(msg);
    }

    function openQuitPopup(msg){
        versionCheckMsgPopup.openMessagePopup(msg);
    }

    function openYesNoPopup(msg,yes_callback,no_callback){
        return yesNoPopup.openPopup(msg,yes_callback,no_callback);
    }

    

}
