#include "HumanCountFtpAllUploader.h"
#include <Poco/Net/FTPClientSession.h>
#include <Poco/FileStream.h>

#include <qurl.h>
#include <qdebug.h>

HumanCountFtpAllUploader::HumanCountFtpAllUploader()
{
    mRun = true;
    //mListIndex = -1;
    //mFilePath = "";
    //mFileName = "";
    //mHostName = "59.15.239.1";
}


HumanCountFtpAllUploader::~HumanCountFtpAllUploader()
{


}

void HumanCountFtpAllUploader::run()
{
    // 1. calculate total size
    // 2. check fileSizeList and mIdxList
    // 3. upload files

    // connection info
    std::string id= "YDP";
    std::string pw= "Ab123456";

    // 1. calculate total size
    QList<int64_t> fileSizeList;
    int64_t        totalFileSize=0;
    for(int k=0; k< mIdxList.size(); k++)
    {
        try
        {
            // open the local target file
            std::string filePath = QUrl(mFilePathList[k]).path().toStdString();
            Poco::FileStream upfile_stream(filePath,std::ios::in);
            
            // calculate file size
            upfile_stream.seekg (0, upfile_stream.end);
            int64_t file_size = upfile_stream.tellg();
            upfile_stream.seekg (0, upfile_stream.beg);

            // add file size
            totalFileSize += file_size;
            fileSizeList.push_back(file_size);

            qDebug()<<mIdxList[k]<<", "<<mFileNameList[k]<<", "<<mFilePathList[k]<<", "<<mHostNameList[k];
        }
        catch(Poco::Exception& e)
        {
            fileSizeList.push_back(0);
        }
    }

    // 2. check fileSizeList and mIdxList
    if(fileSizeList.size() != mIdxList.size())
    {
        QVariantList value;
        value.append("파일 사이즈 계산오류");
        emit sigTotalErrorUpload(value);
        return;
    }

    // 3. upload files
    int successedFiles = 0;
    int faliedFiles = 0;
    totalFileSize=totalFileSize/8192 +1;
    int64_t totalFileProgress = 0;
    for(int k=0; k< mIdxList.size(); k++)
    {
        try
        {
            // emit signal current file name
            {
                emit sigCurrentFileProgressText(mFileNameList[k]);
            }

            // emit signal total progress text
            {
                QString totalProgressText;
                totalProgressText = QString::number(k+1)+"/"+QString::number(mIdxList.size());
                totalProgressText += ", "+ QString::number(successedFiles)+" successes, "+QString::number(faliedFiles) + " failures";
                emit sigTotalProgressText(totalProgressText);
            }

            // connect
            Poco::Net::FTPClientSession session(mHostNameList[k].toStdString(), 21);
            session.login(id,pw);

            // upload path
            std::string upload_path="Videos/"+mFileNameList[k].toStdString();

            // open the local target file
            std::string filePath = QUrl(mFilePathList[k]).path().toStdString();
            Poco::FileStream upfile_stream(filePath,std::ios::in);

            // open the ftp target file
            std::ostream& ftp_stream = session.beginUpload(upload_path);

            // make ftp buffer 
            int64_t buffer_size = 8192;
            std::vector<char> buffer;
            buffer.resize(buffer_size);

            // calculate file size
            int64_t length = fileSizeList[k];

            // upload start
            int64_t iteration_count = length / buffer_size + 1;
            for(int64_t i=0; i< iteration_count; i++,totalFileProgress++)
            {
                if(!mRun)
                {
                    QVariantList value;
                    value.append("사용자 중지");
                    emit sigTotalErrorUpload(value);
                    return;
                }

                if(i != iteration_count)
                {
                    upfile_stream.read(&buffer[0],buffer_size);
                    ftp_stream.write(&buffer[0],buffer_size);
                    if(i%100 == 0)
                    {
                        float current_percent = (float)i/(float)iteration_count*100.0f;
                        QVariantList current_value;
                        current_value.append((int)(current_percent+0.5));
                        emit sigCurrentFileProgress(current_value);

                        float total_percent = (float)totalFileProgress/(float)totalFileSize*100.0f;
                        QVariantList total_value;
                        total_value.append((int)(total_percent+0.5));
                        emit sigTotalProgress(total_value);
                    }    
                }
                else
                {
                    upfile_stream.read(&buffer[0],buffer_size);
                    ftp_stream.write(&buffer[0],upfile_stream.gcount());
                }
            }

            {
                QVariantList value;
                value.append(100);
                QString progress_str = QString::number(100);
                emit sigCurrentFileProgress(value);
            }
            
            QVariantList value;
            value.append(mIdxList[k]);
            emit sigCurrentFinishUpload(value);
            

            session.endUpload();
            session.close();
            successedFiles++;
        }
        catch(Poco::Exception& e)
        {
            QVariantList value;
            value.append(mIdxList[k]);
            value.append(e.message().c_str());
            sigCurrentErrorUpload(value);
            faliedFiles++;
        }

    }
    
    {
        QVariantList value;
        value.append("Finish uploading all files!!");
        sigTotalFinishUpload(value);
    }    
}
