#include "CentroidTracker.h"

 CentroidTracker::CentroidTracker(std::vector<cv::Point> &check_area_, int max_disappear)
 {
     nextObjectID = 0;
     maxDisappeared = max_disappear;
     check_area = check_area_;
 }

CentroidTracker::~CentroidTracker()
{

}

void CentroidTracker::registerObject(cv::Point2i& centroid,int current_area)
{
    track_distance[nextObjectID]       = 0;
    objects[nextObjectID]              = centroid;
    disappeared[nextObjectID]          = 0;
    object_previous_area[nextObjectID] = current_area;
    object_current_area[nextObjectID]  = current_area;
    nextObjectID++;
}

void CentroidTracker::deregisterObject(int objectID)
{
    track_distance.erase(objectID);
    objects.erase(objectID);
    disappeared.erase(objectID);
    object_previous_area.erase(objectID);
    object_current_area.erase(objectID);
}

void CentroidTracker::updateObjects(std::vector<Detection>& rects,std::unordered_map<int, cv::Point2i>& output_objects)
{
    // check to see if the list of input bounding box rectangles is empty
    if(rects.size() == 0)
    {
        std::vector<int> delete_keys;

        auto iter = disappeared.begin();
        for(;iter != disappeared.end(); iter++)
        {
            iter->second +=1;
            object_previous_area[iter->first] = object_current_area[iter->first];
            if(iter->second > maxDisappeared)
            {
                delete_keys.push_back(iter->first);
            }
        }

        for(size_t i=0; i< delete_keys.size(); i++)
        {
            deregisterObject(delete_keys[i]);
        }

        output_objects = objects;
        return;
    }

    // initialize an array of input centroids for the current frame
    cv::Mat inputCentroids = cv::Mat::zeros(rects.size(),2,CV_32S);

    // loop over the bounding box rectangles
    for (size_t i=0; i<rects.size(); i++)
    {
        int cX = int(rects[i].box.x + (rects[i].box.width / 2.0f));
		int cY = int(rects[i].box.y + (rects[i].box.height / 2.0f));
        inputCentroids.at<int>(i,0) = cX;
        inputCentroids.at<int>(i,1) = cY;
    }

    // if we are currently not tracking any objects take the input
	// centroids and register each of them
    if(objects.size() == 0)
    {
        for(int i=0; i< inputCentroids.rows; i++)
        {
            cv::Point2i centroid;
            centroid.x = inputCentroids.at<int>(i,0);
            centroid.y = inputCentroids.at<int>(i,1);

            int current_area = 0;
            double check1 = cv::pointPolygonTest(check_area,centroid,false);
            if(check1 > 0) 
                current_area = 1;
            registerObject(centroid,current_area);
        }
    }
    else
    {
        std::vector<int>         objectIDs;
        std::vector<cv::Point2i> objectCentroids; 
        objectIDs.resize(objects.size(),0);
        objectCentroids.resize(objects.size());

        int i=0,j=0;
        for(auto iter = objects.begin(); iter!= objects.end(); iter++)
        {
            objectIDs[i] = iter->first;
            objectCentroids[i] = iter->second;
            i++;
        }

        // compute the distance between each pair of object
		// centroids and input centroids, respectively -- our
		// goal will be to match an input centroid to an existing
		// object centroid
        cv::Mat D = cv::Mat::zeros(objectCentroids.size(),inputCentroids.rows,CV_32F);
        for(i=0; i< objectCentroids.size(); i++)
        {
            for(j=0; j<inputCentroids.rows; j++)
            {
                D.at<float>(i,j) = std::abs( objectCentroids[i].x - inputCentroids.at<int>(j,0) )+std::abs(objectCentroids[i].y - inputCentroids.at<int>(j,1));
            }
        }

        cv::Mat Rows_,Rows,Cols;
        cv::sort(D,Rows_,cv::SORT_EVERY_ROW + cv::SORT_ASCENDING);
        Rows_ = Rows_(cv::Rect(0,0,1,D.rows));
        cv::sortIdx(Rows_,Rows,cv::SORT_EVERY_COLUMN + cv::SORT_ASCENDING);

        Cols = cv::Mat::zeros(Rows.rows,1,CV_32S);
        for(int k=0; k<D.rows; k++)
        {
            int row_idx = Rows.at<int>(k,0);
            float min_value = 10000000000000.f;
            for(int j=0; j<D.cols; j++)
            {
                if(D.at<float>(row_idx,j) < min_value)
                {
                    Cols.at<int>(k,0) = j;
                    min_value = D.at<float>(row_idx,j);
                }
            }
        }   

        std::vector<int> usedRows;
        std::vector<int> usedCols;
        usedRows.reserve(D.rows);
        usedCols.reserve(D.cols);

        for(i=0; i<Rows.rows; i++)
        {
            int row = Rows.at<int>(i,0);
            int col = Cols.at<int>(i,0);

            auto row_find = std::find(usedRows.begin(),usedRows.end(),row);
            auto col_find = std::find(usedCols.begin(),usedCols.end(),col);
            if((row_find != usedRows.end()) || (col_find != usedCols.end()))
                continue;

            // update the center point which is tracked
            int objectID = objectIDs[row];
            int dist = abs(objects[objectID].x -inputCentroids.at<int>(col,0))
                        + abs(objects[objectID].y -  inputCentroids.at<int>(col,1));

            track_distance[objectID] = dist;
            objects[objectID].x = inputCentroids.at<int>(col,0);
            objects[objectID].y = inputCentroids.at<int>(col,1);
            disappeared[objectID] = 0;

            // update the area check value
            object_previous_area[objectID] = object_current_area[objectID];
            double check1 = cv::pointPolygonTest(check_area,objects[objectID],false);
            if(check1 > 0) 
                object_current_area[objectID] = 1;
            else
                object_current_area[objectID] = 0;

            usedRows.push_back(row);
            usedCols.push_back(col);
        }

        std::vector<int> unusedRows, unusedCols;
        for(i=0; i< D.rows; i++)
        {
            auto row_find = std::find(usedRows.begin(),usedRows.end(),i);
            if(row_find == usedRows.end())
                unusedRows.push_back(i);
        }
        for(i=0; i< D.cols; i++)
        {
            auto col_find = std::find(usedCols.begin(),usedCols.end(),i);
            if(col_find == usedCols.end())
                unusedCols.push_back(i);
        }

        if(D.rows >= D.cols)
        {
            for(i=0; i< unusedRows.size(); i++)
            {
                int objectID = objectIDs[unusedRows[i]];
                disappeared[objectID] += 1;
                object_previous_area[objectID] = object_current_area[objectID];

                if( disappeared[objectID] > maxDisappeared)
                {
                    deregisterObject(objectID);
                }
            }
        }
        else
        {
            for(i=0; i< unusedCols.size(); i++)
            {
                cv::Point2i new_pt;
                new_pt.x = inputCentroids.at<int>(unusedCols[i],0);
                new_pt.y = inputCentroids.at<int>(unusedCols[i],1);
                int current_area = 0;
                double check1 = cv::pointPolygonTest(check_area,new_pt,false);
                if(check1 > 0) 
                    current_area = 1;

                registerObject(new_pt,current_area);
            }
        }


    }

    output_objects = objects;
    return;
}

int CentroidTracker::calculateEnterCount()
{
    auto iter = objects.begin();
    int entercount= 0;
    for(;iter != objects.end(); iter++)
    {
        //if(disappeared[iter->first] == 0)
        {
            if (object_previous_area[iter->first] == 0 && object_current_area[iter->first] == 1)
            {
                if(track_distance[iter->first] < 100)
                    entercount++;
            }
        }
    }

    return entercount;
}