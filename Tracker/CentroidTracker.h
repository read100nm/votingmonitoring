#ifndef SIMPLE_TRACKER_H
#define SIMPLE_TRACKER_H

#include <unordered_map>
#include <opencv2/core.hpp>

#ifdef ANDROID_
#include "HumanCountDetector.h"
#else
#include "HumanCountDetectorCV.h"
#endif

class CentroidTracker
{
public:
    CentroidTracker(std::vector<cv::Point> &check_area_, int maxdisappear = 1);
    ~CentroidTracker();

    void registerObject(cv::Point2i& centroid,int current_area);
    void deregisterObject(int objectID);
    void updateObjects(std::vector<Detection>& rects,std::unordered_map<int, cv::Point2i>& output_objects);

    int  calculateEnterCount();
protected:

    int nextObjectID;
    int maxDisappeared;

    std::unordered_map<int, int>         track_distance;
    std::unordered_map<int, cv::Point2i> objects;
    std::unordered_map<int, int>         disappeared;
    std::unordered_map<int, int>         object_previous_area; // if a object centeroid is inside the check area, the value will be 1, otherwise 0
    std::unordered_map<int, int>         object_current_area;  // if a object centeroid is inside the check area, the value will be 1, otherwise 0

    std::vector<cv::Point> check_area;
};

#endif