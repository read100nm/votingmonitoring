#ifdef ANDROID_
/****************************************************************************
**
** Copyright (C) 2020 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtAndroidExtras module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "jnimessenger.h"

#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QDebug>

JniMessenger *JniMessenger::m_instance = nullptr;

static void callFromJava(JNIEnv *env, jobject /*thiz*/, jstring value)
{
    emit JniMessenger::instance()->messageFromJava(env->GetStringUTFChars(value, nullptr));
}

JniMessenger::JniMessenger(QObject *parent) : QObject(parent)
{
    m_instance = this;

    JNINativeMethod methods[] {{"callFromJava", "(Ljava/lang/String;)V", reinterpret_cast<void *>(callFromJava)}};
    QAndroidJniObject javaClass("org/peoplecount/votingmonitoring/JniMessenger");

    QAndroidJniEnvironment env;
    jclass objectClass = env->GetObjectClass(javaClass.object<jobject>());
    env->RegisterNatives(objectClass,
                         methods,
                         sizeof(methods) / sizeof(methods[0]));
    env->DeleteLocalRef(objectClass);
}

void JniMessenger::printFromJava(const QString &message)
{
    QAndroidJniObject javaMessage = QAndroidJniObject::fromString(message);
    QAndroidJniObject::callStaticMethod<void>("org/peoplecount/votingmonitoring/JniMessenger",
                                       "printFromJava",
                                       "(Ljava/lang/String;)V",
                                        javaMessage.object<jstring>());
    qDebug()<<"called printFromJava with "+message;
}

int JniMessenger::getAndroidVersion()
{
    jint version_value = QAndroidJniObject::callStaticMethod<jint>("org/peoplecount/votingmonitoring/JniMessenger",
                                       "getAndroidVersion");

    return version_value;
}

void JniMessenger::setKeyValueInPre(QString key, QString value)
{
    QAndroidJniObject jni_key   = QAndroidJniObject::fromString(key);
    QAndroidJniObject jni_value = QAndroidJniObject::fromString(value);

    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject return_obj = activity.callObjectMethod("setKeyValue","(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
                              jni_key.object<jstring>(),jni_value.object<jstring>());

}

QString JniMessenger::getKeyValueInPre(QString key, QString value)
{
    QAndroidJniObject jni_key   = QAndroidJniObject::fromString(key);
    QAndroidJniObject jni_value = QAndroidJniObject::fromString(value);

    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject return_obj = activity.callObjectMethod("getKeyValue","(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
                              jni_key.object<jstring>(),jni_value.object<jstring>());

    return return_obj.toString();
}

bool JniMessenger::getCurrentActivityVisible()
{
    jboolean visible = QAndroidJniObject::callStaticMethod<jboolean>("org/peoplecount/votingmonitoring/MyActivity",
                                       "isActivityVisible",
                                       "()Z");


    return visible;
}

QStringList JniMessenger::getWritablePaths()
{
    QStringList paths;
    {
        QAndroidJniObject activity = QtAndroid::androidActivity();

        // Call a java function through the abstraction
        QAndroidJniObject dirs = activity.callObjectMethod("getExternalFilesDirs", "(Ljava/lang/String;)[Ljava/io/File;", NULL);

        // Valid dirs exist
        if (dirs.isValid()){

            QAndroidJniEnvironment env;

            // Size of dirs found
            jsize size = env->GetArrayLength(dirs.object<jarray>());

            // Iterate through the dirs found and add them in the QStringList
            for (int ctr = 0; ctr<size; ctr++){
                QAndroidJniObject dir = env->GetObjectArrayElement(dirs.object<jobjectArray>(), ctr);
                paths.push_back(dir.toString());
                qDebug()<<"External directory found "<<ctr<<":"<<dir.toString();
            }
        }
    }

    QAndroidJniObject mediaDir = QAndroidJniObject::callStaticObjectMethod("android/os/Environment", "getExternalStorageDirectory", "()Ljava/io/File;");
    QAndroidJniObject mediaPath = mediaDir.callObjectMethod( "getAbsolutePath", "()Ljava/lang/String;" );
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject package = activity.callObjectMethod("getPackageName", "()Ljava/lang/String;");
    paths.push_back(mediaPath.toString());
    return paths;
}

#endif
