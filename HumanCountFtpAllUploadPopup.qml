import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import QtMultimedia 5.12
import QtWebView 1.15
import HumanCountLib 1.0

Item {
    id: ftpAllUploadPopup
    x:0
    y:0
    width: parent.width
    height: parent.height

    signal sigCanceled()

    
    // Popup object
    Popup {
        id:popup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closePopup();
            }
        }

        Rectangle {
            id: add_cargo_info_ui
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*1/2
            radius: 5
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            Text
            {
                id:progressText
                x:parent.width * 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                y:parent.height* 0.05       //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                width:parent.width*0.8
                height:parent.height*0.1
                color:"black"
                text: "분석 파일을 다운로드 받고 있습니다."
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                wrapMode: Text.WordWrap
            }

            ProgressBar{
                id: progressBar
                x: parent.width * 0.1
                y: parent.height * 0.19
                width: parent.width * 0.8
                height: parent.height * 0.15
                visible:true
            }

            Text
            {
                id:totalText
                x:parent.width * 0.1        //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                y:parent.height* 0.44       //팝업의 영역이 현재 화면의 중앙에 오도록 지정
                width:parent.width*0.8
                height:parent.height*0.1
                color:"black"
                text: "분석 파일을 다운로드 받고 있습니다."
                horizontalAlignment: Text.AlignHCenter
                fontSizeMode: Text.Fit
                wrapMode: Text.WordWrap
            }

            ProgressBar{
                id: progressBarTotal
                x: parent.width * 0.1
                y: parent.height * 0.58
                width: parent.width * 0.8
                height: parent.height * 0.15
                visible:true
            }

            Rectangle{
                id: no_btn
                x: parent.width * 0.6
                y: parent.height * 0.8
                width: parent.width * 0.3
                height: parent.height * 0.1
                color: cancel_btn_ma.pressed? "#FFFFFF":"#CFE9F0"
                radius: height*0.458333333
                border.color: "#CFE9F0"
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter 
                    width: parent.width* 0.487179487
                    height: parent.height * 0.4375
                    text: qsTr("취소")
                    font.family: "Roboto"
                    font.pixelSize: height* 0.761904762
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: cancel_btn_ma
                    anchors.fill: parent
                    onClicked: {
                        closePopup();
                        sigCanceled();
                    }
                }
            }
        }
    }

    function openPopup(msg){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수
        console.log("open Ftp Upload All popup")
        progressText.text = msg;
        popup.open();
    }

    function setProgressText(p_value){
        progressText.text = p_value;
    }

    function setTotalProgressText(p_value){
        totalText.text = p_value;
    }

    function setProgressValue(p_value){
        progressBar.value = p_value;
    }

    function setTotalProgressValue(p_value){
        progressBarTotal.value = p_value;
    }

    function closePopup()
    {
        console.log("close progress popup")
        popup.close();
    }

    function setIndeterminate(value){
        progressBar.indeterminate = value;
    }
}
