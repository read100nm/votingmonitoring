import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
//import Qt.labs.platform
import QtMultimedia 5.12
import QtWebView 1.15
import HumanCountLib 1.0

Item {
    id: humanCountMapPopup
    x:0
    y:0
    width: parent.width
    height: parent.height
    property var htmlstring: null
    //HumanCountQmlUtils{
    //    id: votingMonitoringQmlUtils
    //}

    // Popup object
    Popup {
        id:cargoInfoPopup
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        leftPadding: 0  // popup 만들시 기본적으로 설정되는 각 영역들을 모두 없앰
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0
        background:Rectangle//팝업 dimming 처리를 해주시 위해서 배경화면을 검정색 약간 투명으로 설정해줌
        {
            color:"black"
            opacity:0.5//배경색의 투명도를 설정 해줌
        }

        MouseArea//팝업을 전체 화면으로 설장한 후 파란색 영역 이외의 부분을 클릭했을때 팝업이 닫히도록 설정하는 부분
        {
            anchors.fill: parent
            onClicked:
            {
                closeMapPopup();
            }
        }



        
        Rectangle {
            id: add_cargo_info_ui
            x:(parent.width - width) / 2 //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            y:(parent.height - height) / 2  //팝업의 영역이 현재 화면의 중앙에 오도록 지정
            width:parent.width*4/5
            height:parent.height*4/5
            radius: 5
            color:"#F8F8F8"

            MouseArea//팝업의 파란색 영역을 클릭했을때 팝업 종료 이벤트가 발생되지 안도록 선언만 시켜주는 부분
            {
                anchors.fill: parent
            }

            WebView {
                id: webview
                x: 0
                y: 0
                width: parent.width
                height: parent.height * 0.6
                url: "https://imagedei.com/a.htm"
                //url:"https://daum.net"
            }

            TextField {
                id: addressTextField
                x: parent.width * 0.05
                y: parent.height * 0.68
                width: parent.width * 0.7
                height: parent.height * 0.1
                text: qsTr("")
                readOnly: true
            }

            Rectangle {
                x: parent.width * 0.77
                y: parent.height * 0.68
                width: parent.height * 0.1
                height: parent.height * 0.1
                color: copyAddr_ma.pressed == true ? "#CFE9F0" : "#ffffff"
                border.width: 1
                border.color: "#CFE9F0"
                radius: 2
                Image {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.width * 2.0 / 3.0
                    width: parent.height * 2.0 / 3.0
                    source: "images/icon_copy.png"
                    fillMode: Image.PreserveAspectFit
                    verticalAlignment: Image.AlignTop
                }
                MouseArea {
                    id: copyAddr_ma
                    anchors.fill: parent
                    onClicked: {
                        //addressTextField.readOnly = false;
                        addressTextField.selectAll();
                        addressTextField.copy();
                        addressTextField.deselect();
                        //addressTextField.readOnly = true;
                    }
                }
            }

            Rectangle{
                x: add_cargo_info_ui.width * 0.05
                y: add_cargo_info_ui.height * 0.86
                width: add_cargo_info_ui.width * 0.9
                height: add_cargo_info_ui.height * 0.1
                color: savePos_ma.pressed? "#FFFFFF":"#CFE9F0"
                radius: height*0.458333333
                border.color: "#CFE9F0"
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter 
                    width: parent.width* 0.487179487
                    height: parent.height * 0.4375
                    text: qsTr("확인")
                    font.family: "Roboto"
                    font.pixelSize: height* 0.761904762
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                MouseArea {
                    id: finish_add_cargo_btn
                    anchors.fill: parent
                    onClicked: {
                        closeMapPopup();
                    }
                }
            }
        }
    }

    function openMapPopup(lat, lng, my_lat,my_lng){//각 화면의 버튼 팝업의 텍스트를 달리 주기위해서 설정하는 함수
        console.log("open map popup");
        cargoInfoPopup.open();
        var js_script = "loadMap("+lat+","+lng+","+my_lat+","+my_lng+");";
        console.log(js_script);
        getFullAddresFromLatLng(lat,lng);
        webview.runJavaScript(js_script,function(result){console.log(result);});
    }

    function closeMapPopup()
    {
        console.log("close map popup")
        cargoInfoPopup.close();
    }

    function getFullAddresFromLatLng(lat,lng){
        var request_addr = "http://dapi.kakao.com/v2/local/geo/coord2address?x="+lng+"&y="+lat;
        http_request(request_addr,"", function (o) {
            if (o.status === 200)
            {
                console.log("goood rest api!!");
                var jsonString = o.responseText.toString();
                jsonString = jsonString.replace(/^\uFEFF/gm, "");

                var jsn = JSON.parse(jsonString);
                
                //console.log(jsn.documents[0].road_address.address_name.toString());
                //console.log(jsn.documents[0].road_address.building_name.toString());
                if(jsn.documents[0].road_address != null)
                {
                    addressTextField.text = jsn.documents[0].road_address.address_name.toString();
                    if(jsn.documents[0].road_address.building_name != null)
                        addressTextField.text = addressTextField.text + jsn.documents[0].road_address.building_name.toString();
                }
                else
                {
                    addressTextField.text = jsn.documents[0].address.address_name.toString();
                }
                
            }
            else
            {
                console.log(o.status +": Some error has occurred");
            }
        });
    }

    function http_request(url,param, callback) {
        var xhr = new XMLHttpRequest();
        
        xhr.onreadystatechange = (function(myxhr) {
            return function() {
                if(myxhr.readyState === 4) { callback(myxhr); }
            }
        })(xhr);

        xhr.open("GET", url,true);
        xhr.setRequestHeader("Authorization", "KakaoAK 0ead8c4d9a8c9f10b1645bc30ba99507");
        xhr.send(param);
    }
}
