import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

ComboBox {
    background: Rectangle{
        anchors.fill: parent
        color: "#FFFFFF"
        radius: parent.height * 0.55
        border.width: 1
        border.color: "#CFE9F0"
    }
        indicator:Canvas {
            id: canvas
            x: parent.width * 0.833333333
            y: parent.height * 0.425
            width: parent.width * 10.0/162.0
            height: parent.height * 5.0/40.0
            contextType: "2d"

            onPaint: {
                context.beginPath();
                context.lineWidth=1.5;
                context.strokeStyle = "#767676"
                context.moveTo(0, 0);
                context.lineTo(width / 2, height);
                context.lineTo(width,0);
                context.stroke();
            }
        }
    }