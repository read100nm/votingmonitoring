import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.11
import QtQuick.Window 2.11

 SwipeDelegate {
    id: video_list_delegate
    property var window_height:managevideo_page.height
    property var window_width:managevideo_page.width
    implicitHeight: window_height* 0.2
    implicitWidth: window_width

    contentItem: Item{
        id: video_item
        x:0
        y:0
        width: parent.width
        height: parent.height
        //color: "red"

        Rectangle {
            id: status_rect
            x: parent.width * 24 /360
            y: parent.height * 26 / 130
            width: parent.width * 60/360
            height: parent.height *33/130
            radius: height * 28 / 16
            color: model.color
        }

        Rectangle {
            id: delete_rect
            x: parent.width * 108 /360
            y: parent.height * 26 / 130
            width: parent.width * 100/360
            height: parent.height *33/130
            radius: height * 28 / 16
            color: "#FFFF10"
            Text{
                x: 0
                y: 0
                width: parent.width 
                height: parent.height 
                text: qsTr("파일 영구 삭제")
                color: "#FF0000"
                minimumPixelSize: 2
                font.pixelSize: height * 14 / 28
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("click 삭제:"+model.name);
                    console.log(index);
                    if(humanAuditorData.isUploading()){
                        main_window.openMessagePopup("개별 파일 업로드가 진행중입니다. 개별 파일 업로드 진행중에는 삭제 동작이 불가능합니다.");
                        return;
                    }
                    var currnetIndex=index;
                    var return_value = main_window.openYesNoPopup("정말 해당 영상 파일을 영구적으로 삭제 하시겠습니까?",function(){
                        managevideo_page.deleteVideoInfo(model.name);
                        videoList.model.remove(index);
                    },null);

                    //if(return_value == true)
                    //{
                    //    
                    //}
                }
            }
        }
        
        Text {
            id: status_text
            x: parent.width * 24 /360
            y: parent.height * 26 / 130
            width: parent.width * 60/360
            height: parent.height *33/130
            color: "#FFFFFF"
            font.pixelSize: height * 14 / 28
            text: model.status
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(model.status == "OK")
                    {
                        main_window.openMessagePopup("이미 업로드된 파일입니다");
                    }
                    else if(model.status.includes("%"))
                    {
                        var return_value = main_window.openYesNoPopup("업로드 중입니다. 취소하시겠습니까?",function(){
                            humanAuditorData.stopUploading(index);
                        },null);
                    }
                    else
                    {
                        console.log("click 업로드:"+model.name);
                        console.log(index);
                        console.log(typeof model.nas_id);
                        console.log(model.nas_id)
                        var path = "file://" +MoiveLocationPath + "/" + model.name;
                        if(RunPlatform == "ANDROID")
                            path = model.path;
                        humanAuditorData.uploadVideoFile(index,model.name,path,model.nas_id);
                    }
                }
            }
        }

        Text {
            x: parent.width * 24 /360
            y: parent.height * 78 / 130
            width: parent.width * 45/360
            height: parent.height *15/130
            color: "#868686"
            font.pixelSize: height * 15 / 15
            text: "녹화시간"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
        }

        Text {
            x: parent.width * 89 /360
            y: parent.height * 75 / 130
            width: parent.width * 250/360
            height: parent.height *21/130
            color: "#000000"
            font.pixelSize: height * 16 / 21
            text: model.date
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
        }

        Text {
            x: parent.width * 24 /360
            y: parent.height * 113 / 130
            width: parent.width * 34/360
            height: parent.height *15/130
            color: "#868686"
            font.pixelSize: height * 15 / 15
            text: "파일명"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
        }

        Text {
            x: parent.width * 89 /360
            y: parent.height * 110 / 130
            width: parent.width * 250/360
            height: parent.height *21/130
            color: "#000000"
            font.pixelSize: height * 16 / 21
            text: model.name
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
        }

    }
    
    ListView.onRemove: SequentialAnimation {
        
        PropertyAction {
            target: video_list_delegate
            property: "ListView.delayRemove"
            value: true
        }
        
        NumberAnimation {
            target: video_list_delegate
            property: "height"
            to: 0
            easing.type: Easing.InOutQuad
        }
        
        PropertyAction {
            target: video_list_delegate
            property: "ListView.delayRemove"
            value: false
        }
    }

    swipe.right: Label {
        id: deleteLabel
        text: qsTr("Delete")
        color: "white"
        verticalAlignment: Label.AlignVCenter
        padding: 12
        height: parent.height
        anchors.right: parent.right

        background: Rectangle {
        color: deleteLabel.SwipeDelegate.pressed ? Qt.darker("tomato", 1.1) : "tomato"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("delete idx: "+index);
                    console.log("delete name: "+model.name);
                    managevideo_page.deleteVideoInfo(model.name);
                    videoList.model.remove(index);
                    //managevideo_page.loadVideoInfo();
                }
            }
        }
    }
 }
