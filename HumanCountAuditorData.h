#ifndef HUMAN_COUNT_AUDITOR_DATA_H_
#define HUMAN_COUNT_AUDITOR_DATA_H_

#include <QObject>
#include <qvariant.h>
#include <qmap.h>
#include "HumanCountFtpUploader.h"

class HumanCountAuditorData : public QObject
{
    Q_OBJECT

    Q_PROPERTY( float gpsx MEMBER mCurrentGPS_x )
    Q_PROPERTY( float gpsy MEMBER mCurrentGPS_y )
    Q_PROPERTY( QString placeCode MEMBER mCurrentPlacecode )
    Q_PROPERTY( QString electionCode MEMBER mCurrentElectionCode)
    Q_PROPERTY( QString nasID MEMBER mCurrentNasID)
public:
    Q_INVOKABLE QVariantList uploadVideoFile(int listIdx, QString fileName, QString filePath, QString nas_id);
    Q_INVOKABLE QVariantList stopUploading(int listIdx);
    Q_INVOKABLE bool isUploading();
    Q_INVOKABLE QString getSavedID();

    HumanCountAuditorData(QObject *parent = nullptr);

    static HumanCountAuditorData *instance() { return m_instance; }

    QString mCurrentNasID;
public slots:
    void slotFinishUpload(QVariantList value);
    void slotErrorUpload(QVariantList value);

signals:
    void sigFinishUpload(QVariantList result);
    void sigErrorUpload(QVariantList result);
    void sigProgress(QVariantList result);


private:
    static HumanCountAuditorData *m_instance;

    float mCurrentGPS_x;
    float mCurrentGPS_y;

    QString mCurrentPlacecode;
    QString mCurrentElectionCode;
 

    //QString mVideoFileName;
    int mMaxUpload;
    int mCurrentUpload;
    QMap<int,HumanCountFtpUploader*> mListKeyThreadMap;

    bool isAllVideoUploading;
};

#endif
